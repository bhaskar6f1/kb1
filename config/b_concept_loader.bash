#!/bin/bash
#
# This script helps load Hadoop B-concept merged tuple files into mysql
# as generated by the 'DataJoin' and 'DataSort' (Java) MapReduce tasks
#
# rename files for mysql importing
#
# for f in part-r-*; do mv $f tuples_merged.${f##*-} ; done

#
# Load into mysql
#
mysqlimport --verbose --user=root --password=\$cr1%%\$ --local --replace --force --default-character-set="utf8" --fields-terminated-by="|" --lines-terminated-by="\n" implicitome tuples_merged.*
