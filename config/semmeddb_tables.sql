-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: semmeddb
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CITATIONS`
--

DROP TABLE IF EXISTS `CITATIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CITATIONS` (
  `PMID` varchar(20) NOT NULL,
  `ISSN` varchar(10) DEFAULT NULL,
  `DP` varchar(50) DEFAULT NULL,
  `EDAT` varchar(50) DEFAULT NULL,
  `PYEAR` int(5) DEFAULT NULL,
  PRIMARY KEY (`PMID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Stores semantic predications in sentences';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CONCEPT`
--

DROP TABLE IF EXISTS `CONCEPT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CONCEPT` (
  `CONCEPT_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CUI` varchar(20) NOT NULL DEFAULT '',
  `TYPE` varchar(10) NOT NULL DEFAULT 'META',
  `PREFERRED_NAME` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `GHR` varchar(250) DEFAULT NULL,
  `OMIM` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`CONCEPT_ID`),
  UNIQUE KEY `CONCEPT` (`CUI`,`TYPE`),
  KEY `PREFERRED_NAME` (`PREFERRED_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=1377847 DEFAULT CHARSET=latin1 COMMENT='Stores basic UMLS concept information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CONCEPT_CITATIONS`
--

DROP TABLE IF EXISTS `CONCEPT_CITATIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CONCEPT_CITATIONS` (
  `CONCEPT_CITATIONS_ID` int(11) NOT NULL,
  `CUI` varchar(20) NOT NULL,
  `PMID` longtext NOT NULL,
  PRIMARY KEY (`CONCEPT_CITATIONS_ID`),
  UNIQUE KEY `CUI` (`CUI`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CONCEPT_SEMTYPE`
--

DROP TABLE IF EXISTS `CONCEPT_SEMTYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CONCEPT_SEMTYPE` (
  `CONCEPT_SEMTYPE_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CONCEPT_ID` int(10) unsigned NOT NULL,
  `SEMTYPE` varchar(4) NOT NULL DEFAULT '',
  `NOVEL` char(1) NOT NULL DEFAULT 'Y',
  `UMLS` char(1) DEFAULT NULL,
  PRIMARY KEY (`CONCEPT_SEMTYPE_ID`),
  UNIQUE KEY `CONCEPT_SEMTYPE` (`CONCEPT_ID`,`SEMTYPE`),
  CONSTRAINT `CONCEPT_SEMTYPE_ibfk_1` FOREIGN KEY (`CONCEPT_ID`) REFERENCES `CONCEPT` (`CONCEPT_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1589104 DEFAULT CHARSET=latin1 COMMENT='Stores concept semtype information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `METAINFO`
--

DROP TABLE IF EXISTS `METAINFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `METAINFO` (
  `DBVERSION` varchar(10) NOT NULL,
  `SEMREPVERSION` varchar(10) DEFAULT NULL,
  `PUBMED_TODATE` varchar(10) DEFAULT NULL,
  `COMMENT` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PREDICATION`
--

DROP TABLE IF EXISTS `PREDICATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PREDICATION` (
  `PREDICATION_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PREDICATE` varchar(50) NOT NULL DEFAULT '',
  `TYPE` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`PREDICATION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15877736 DEFAULT CHARSET=latin1 COMMENT='Stores a distinct semantic predication';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PREDICATION_AGGREGATE`
--

DROP TABLE IF EXISTS `PREDICATION_AGGREGATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PREDICATION_AGGREGATE` (
  `PID` int(10) unsigned NOT NULL DEFAULT '0',
  `SID` int(10) unsigned NOT NULL DEFAULT '0',
  `PNUMBER` int(10) unsigned NOT NULL DEFAULT '0',
  `PMID` varchar(20) DEFAULT NULL,
  `predicate` varchar(50) DEFAULT NULL,
  `s_cui` varchar(255) DEFAULT NULL,
  `s_name` varchar(999) DEFAULT NULL,
  `s_type` varchar(50) DEFAULT NULL,
  `s_novel` tinyint(1) DEFAULT NULL,
  `o_cui` varchar(255) DEFAULT NULL,
  `o_name` varchar(999) DEFAULT NULL,
  `o_type` varchar(50) DEFAULT NULL,
  `o_novel` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`PID`,`SID`,`PNUMBER`),
  KEY `pmid_index_btree2_myisam` (`PMID`) USING BTREE,
  KEY `s_cui` (`s_cui`),
  KEY `o_cui` (`o_cui`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Stores aggregate info of semantic predications';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PREDICATION_ARGUMENT`
--

DROP TABLE IF EXISTS `PREDICATION_ARGUMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PREDICATION_ARGUMENT` (
  `PREDICATION_ARGUMENT_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PREDICATION_ID` int(10) unsigned NOT NULL,
  `CONCEPT_SEMTYPE_ID` int(10) unsigned NOT NULL,
  `TYPE` char(1) NOT NULL,
  PRIMARY KEY (`PREDICATION_ARGUMENT_ID`),
  UNIQUE KEY `PREDICATION_ARGUMENT` (`PREDICATION_ID`,`CONCEPT_SEMTYPE_ID`,`TYPE`),
  KEY `FK_CONCEPT_SEMTYPE` (`CONCEPT_SEMTYPE_ID`),
  CONSTRAINT `PREDICATION_ARGUMENT_ibfk_1` FOREIGN KEY (`PREDICATION_ID`) REFERENCES `PREDICATION` (`PREDICATION_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PREDICATION_ARGUMENT_ibfk_2` FOREIGN KEY (`CONCEPT_SEMTYPE_ID`) REFERENCES `CONCEPT_SEMTYPE` (`CONCEPT_SEMTYPE_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33944698 DEFAULT CHARSET=latin1 COMMENT='Stores information about predication arguments - Created bec';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SENTENCE`
--

DROP TABLE IF EXISTS `SENTENCE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SENTENCE` (
  `SENTENCE_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PMID` varchar(20) NOT NULL DEFAULT '',
  `TYPE` varchar(2) NOT NULL DEFAULT '',
  `NUMBER` int(10) unsigned NOT NULL DEFAULT '0',
  `SENTENCE` varchar(999) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`SENTENCE_ID`),
  UNIQUE KEY `SENTENCE` (`PMID`,`TYPE`,`NUMBER`),
  KEY `PMID_INDEX` (`PMID`) USING BTREE,
  KEY `PMID_HASH` (`PMID`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=158549964 DEFAULT CHARSET=latin1 COMMENT='Stores sentences from Medline';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SENTENCE_PREDICATION`
--

DROP TABLE IF EXISTS `SENTENCE_PREDICATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SENTENCE_PREDICATION` (
  `SENTENCE_PREDICATION_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SENTENCE_ID` int(10) unsigned NOT NULL,
  `PREDICATION_ID` int(10) unsigned NOT NULL,
  `PREDICATION_NUMBER` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Same predication occurring multiple times in one sentence',
  `SUBJECT_TEXT` varchar(200) DEFAULT '' COMMENT 'Should be NOT NULL eventually',
  `SUBJECT_DIST` int(10) unsigned DEFAULT '0' COMMENT 'Should be NOT NULL eventually',
  `SUBJECT_MAXDIST` int(10) unsigned DEFAULT '0' COMMENT 'Should be NOT NULL eventually',
  `SUBJECT_START_INDEX` int(10) unsigned DEFAULT '0' COMMENT 'Should be NOT NULL eventually',
  `SUBJECT_END_INDEX` int(10) unsigned DEFAULT '0' COMMENT 'Should be NOT NULL eventually',
  `SUBJECT_SCORE` int(10) unsigned DEFAULT '0' COMMENT 'Should be NOT NULL eventually',
  `INDICATOR_TYPE` varchar(10) DEFAULT '' COMMENT 'Should be NOT NULL eventually',
  `PREDICATE_START_INDEX` int(10) unsigned DEFAULT '0' COMMENT 'Should be NOT NULL eventually',
  `PREDICATE_END_INDEX` int(10) unsigned DEFAULT '0' COMMENT 'Should be NOT NULL eventually',
  `OBJECT_TEXT` varchar(200) DEFAULT '' COMMENT 'Should be NOT NULL eventually',
  `OBJECT_DIST` int(10) unsigned DEFAULT '0' COMMENT 'Should be NOT NULL eventually',
  `OBJECT_MAXDIST` int(10) unsigned DEFAULT '0' COMMENT 'Should be NOT NULL eventually',
  `OBJECT_START_INDEX` int(10) unsigned DEFAULT '0' COMMENT 'Should be NOT NULL eventually',
  `OBJECT_END_INDEX` int(10) unsigned DEFAULT '0' COMMENT 'Should be NOT NULL eventually',
  `OBJECT_SCORE` int(10) unsigned DEFAULT '0' COMMENT 'Should be NOT NULL eventually',
  `CURR_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`SENTENCE_PREDICATION_ID`),
  UNIQUE KEY `SENTENCE_PREDICATION` (`SENTENCE_ID`,`PREDICATION_ID`,`PREDICATION_NUMBER`),
  KEY `FK_PREDICATION` (`PREDICATION_ID`),
  CONSTRAINT `SENTENCE_PREDICATION_ibfk_1` FOREIGN KEY (`SENTENCE_ID`) REFERENCES `SENTENCE` (`SENTENCE_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SENTENCE_PREDICATION_ibfk_2` FOREIGN KEY (`PREDICATION_ID`) REFERENCES `PREDICATION` (`PREDICATION_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=76520394 DEFAULT CHARSET=latin1 COMMENT='Stores semantic predications in sentences';
/*!40101 SET character_set_client = @saved_cs_client */;
