#!/bin/bash
#
# Simple database loader wrapper with timestamps
#
/bin/echo "Started loading of '$1' at "
/bin/date
/bin/echo
/usr/bin/mysql --user=<dbuser> --password='<dbuser password>' semmeddb <$1 >/dev/null
/bin/echo
/bin/echo "Completed loading of '$1' at "
/bin/date