-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: implicitome
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `concept`
--

DROP TABLE IF EXISTS `concept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `concept` (
  `concept_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `definition` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`concept_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4364122 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dblink`
--

DROP TABLE IF EXISTS `dblink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dblink` (
  `dblink_id` int(11) NOT NULL AUTO_INCREMENT,
  `concept_id` int(11) NOT NULL,
  `db_id` varchar(4) NOT NULL,
  `identifier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dblink_id`),
  KEY `dbid` (`db_id`,`identifier`)
) ENGINE=MyISAM AUTO_INCREMENT=1835929 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `relation`
--

DROP TABLE IF EXISTS `relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relation` (
  `relation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `concept1_id` int(11) NOT NULL,
  `concept2_id` int(11) NOT NULL,
  `relationtype_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`relation_id`),
  UNIQUE KEY `concept1_id` (`concept1_id`,`concept2_id`),
  KEY `conceptid1` (`concept1_id`),
  KEY `conceptid2` (`concept2_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1607588 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `term`
--

DROP TABLE IF EXISTS `term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `term` (
  `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `concept_id` int(11) NOT NULL,
  `subterm_id` int(11) NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  `casesensitive` tinyint(1) DEFAULT NULL,
  `ordersensitive` tinyint(1) DEFAULT NULL,
  `normalised` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `concept_id` (`concept_id`,`subterm_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3918407 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tuples`
--

DROP TABLE IF EXISTS `tuples`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tuples` (
  `tuple_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_id` int(11) NOT NULL,
  `sub_name` varchar(100) DEFAULT NULL,
  `obj_id` int(11) NOT NULL,
  `obj_name` varchar(100) DEFAULT NULL,
  `score` decimal(20,16) NOT NULL DEFAULT '0.0000000000000000',
  `percentile` decimal(12,10) DEFAULT NULL,
  `linked_concepts` TEXT DEFAULT NULL,
  PRIMARY KEY (`tuple_id`)
) ENGINE=InnoDB AUTO_INCREMENT=204072377 DEFAULT CHARSET=latin1;

CREATE INDEX `tuples_concept_pairs` ON tuples(`sub_id`,`obj_id`);

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-25 16:07:18
