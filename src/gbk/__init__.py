# Database Router defined for multiple Knowledg.Bio databases?

import sys

class AuthRouter(object):
    """
    A router to control all database operations on models in the
    auth application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read auth models go to auth_db.
        """
        if model._meta.app_label == 'auth':
            return 'auth_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth models go to auth_db.
        """
        if model._meta.app_label == 'auth':
            return 'auth_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the auth app is involved.
        """
        if  obj1._meta.app_label == 'auth' or \
            obj2._meta.app_label == 'auth':
            return True
        return None

    def allow_migrate(self, db, model):
        """
        Make sure the auth app only appears in the 'auth_db'
        database.
        """
        if db == 'auth_db':
            return model._meta.app_label == 'auth'
        elif model._meta.app_label == 'auth':
            return False
        return None

class KBDatabaseRouter(object):
    """
    A router to control all database operations 
    on models in the Knowledge.Bio application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read auth models go 
        to the appropriate Knowledg.Bio databsae
        """
        if model._meta.app_label == 'semmedb':
            return 'semmedb'
        elif model._meta.app_label == 'implicitome':
            return 'implicitome'
        elif model._meta.app_label == 'umls':
            return 'umls'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to read auth models go 
        to the appropriate Knowledg.Bio databsae
        """
        if model._meta.app_label == 'semmedb':
            return 'semmedb'
        elif model._meta.app_label == 'implicitome':
            return 'implicitome'
        elif model._meta.app_label == 'umls':
            return 'umls'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model 
        in the same app is involved.
        """
        if obj1._meta.app_label == obj2._meta.app_label:
            return True
        return None

    def allow_migrate(self, db, model):
        """
        Make sure a given app only 
        appears in its own database.
        """
        if db == 'semmeddb':
            return model._meta.app_label == 'semmeddb'
        elif model._meta.app_label == 'semmeddb':
            return False
        elif db == 'implicitome':
            return model._meta.app_label == 'implicitome'
        elif model._meta.app_label == 'implicitome':
            return False
        elif db == 'umls':
            return model._meta.app_label == 'umls'
        elif model._meta.app_label == 'umls':
            return False
        return None

from django.test.simple import DjangoTestSuiteRunner

from umls        import loadUMLSTestData
from semmeddb    import loadSemMedDbTestData
from implicitome import loadImplicitomeTestData

class KBTestRunner(DjangoTestSuiteRunner):
    
    def setup_databases(self, **kwargs):
        
        print >>sys.stderr,"KBTestRunner.setup_databases..."
        
        old_names, mirrors = DjangoTestSuiteRunner.setup_databases(self, **kwargs)
        
        # intercepting the database setup to load test data?
        self.loadTestData()
        
        return old_names, mirrors

    def loadTestData(self):
        
        # Load test data for UMLS testing
        loadUMLSTestData()
        
        # Load test data for SemMedDb testing
        loadSemMedDbTestData()
        
        # Load test data for Implicitome testing
        loadImplicitomeTestData()
        