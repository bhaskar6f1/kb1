# coding=utf-8
"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
"""
This module encapsulates the Semantic Medline Database
query service business module for the 
Knowledge.Bio application
"""
import urllib

from HTMLParser import HTMLParser
import json

from gbk.settings import logger

DEBUG   = False # diagnostics for code flow
VERBOSE = False # verbose diagnostics for code flow
TRACE   = False # dump results from query if true

from django.db.models import Q

from query.datasource import DSID
from query.service import Query

from semmeddb.models import (
    Concept,
    ConceptSemtype,
    Predication,
    PredicationArgument,
    Sentence,
    SentencePredication,
    Citations,
    ConceptCitations
)

class SemMedDbQuery(Query):
    
    def __init__(self):
        Query.__init__(self, DSID.SEMMEDDB)
        self.htmlparser = HTMLParser() # for local conversions of HTML
        
    RELATION_SORT_COLUMN = [
        'predicationargument__concept_semtype__concept__preferred_name',
        'predicationargument__predication__predicate',
        'predicationargument__concept_semtype__concept__preferred_name'
    ]
   
    # Common implementation of querying to retrieve Predications 
    # given a query filter anchored on the Predication model.   
    # LIMITATION: subject, predict, object concept string field ordering 
    # is NOT implemented on the database query end but rather, in memory,
    # since the Django model 'orderBy' query method doesn't seem to 
    # work on attributes nested deeply in a model query (requiring
    # joins across multiple table to get associated with the output).
    #
    # Revision:
    #     3-4-2015 if queryFilter is an array, then I apply the queryFilter as an 'or'ing Q filter 
    
    def retrievePredications(
            self,
            # if zero, then is undefined, e.g. if sentence_predications are the source of relations
            queryFilter,
            current_concept_id = 0, 
            start=0, 
            length=-1,
            type_filter = "",
            text_filter = "",
            orderColumn = '0',
            orderDir = 'asc',
            tophits = []  # fastR identified CUI's of top hits
     ):
        if DEBUG:
            print >>logger,\
                u"\nsemmeddb.service.retrievePredications("+\
                u' queryFilter = "'+unicode(queryFilter),
            print >>logger,\
                '" type(queryFilter) = '+str(type(queryFilter))+\
                ', current_concept_id = '+str(current_concept_id)+\
                ', start = '+str(start)+\
                ", length = "+str(length),
            print >>logger,\
                ", text_filter = '"+unicode(text_filter),
            print >>logger,\
                "', orderColumn = "+str(orderColumn)+\
                ", orderDir = '"+str(orderDir)+\
                ", tophits = '",tophits,\
                "' )"
        
        # sanity check on the integral values of start, length and orderColumn
        try:
            current_concept_id  = int(current_concept_id)
            current_concept_id  = 0 if current_concept_id < 0 else current_concept_id
            start  = int(start)
            start  = 0 if start < 0 else start
            length = int(length) 
            if not orderColumn == None: # column should be a int if not None
                orderColumn = int(orderColumn)
                if( orderColumn<0 or orderColumn>2 ):
                    print >>logger, 'retrievePredications() ERROR: orderColumn not in range 0..2?'
                    raise ValueError()
                
        except (ValueError, TypeError):
            print >>logger, "ERROR - retrievePredications(): Invalid start["+str(start)+\
                                "], length["+str(length)+"] or orderColumn["+str(orderColumn)+"<=2]?"
            return [],0,0
        
        # First, get all the Predication retrieved by the query filter

        qFilter = None # general Q filter
        
        # Need to distinguish between a queryFilter which 
        # is a dictionary of 'and' directives versus 
        # a queryFilter which has 'or' (Q) directives
        
        if type( queryFilter ) == dict:
            # apply queryFilter directly as filter()
            if VERBOSE:
                print >>logger, "\nApplying queryFilter ",str(queryFilter)," directly."
                
            totalHits =  [ p.predication_id 
                            for p in 
                                Predication.objects.using(self.getDsid()).filter(**queryFilter).distinct()
                          ]
        
        elif type(queryFilter) == list:  
            # this part of the code is somewhat deprecated (qFilter below is generally ignored...)
            raise Exception("Deprecated kind of predicate search?")
            
            if VERBOSE:
                print >>logger, "\nApplying queryFilter ",str(queryFilter)," as Q filter OR construct."
                
            # Sanity check - not an empty query filter list!
            if len(queryFilter) == 0:
                return [],0,0
            
            # build a Q filter predicate for 'OR'ing query filter
            # queryFilter assumed to be an array of dictionaries 
            # with Django model filter specs
            
            for option in queryFilter: 
                if qFilter == None:
                    qFilter = Q( **option )
                else:
                    qFilter |= Q( **option )

            if VERBOSE:
                print >>logger, "\nPredication qFilter: ", qFilter

            totalHits =  [ p.predication_id 
                            for p in 
                                Predication.objects.using(self.getDsid()).filter(qFilter).distinct()
                          ]
            
        else: # not sure what to do with this type of queryFilter
            if VERBOSE:
                print >>logger, u"\nNot sure what to do with ",unicode(queryFilter),u"  queryFilter."
                
            return [],0,0
        
        totalHitCount = len(totalHits)
        if VERBOSE: print >>logger, "\ntotalHits: "+str(totalHitCount)
        
        textQFilter = None
        typeMatchedPredArgs = None
        
        if not type_filter == None and len(type_filter)>0:
                            
            if VERBOSE: 
                print >>logger, "filtered with type filter[value="+str(type_filter)+"]...",
                
            # If not empty, the type_filter should be a 
            # comma delimited list of semantic types
            # split into an array of semantic types    
            semantic_types = type_filter.split(',')
            
            # Filter PredicationArguments of predication hits
            # on semantic types of concepts which are
            # other than the current query concept
            typeMatchedPredArgs = \
                PredicationArgument.objects.using(self.getDsid()).\
                    filter( predication_id__in = totalHits ). \
                    exclude( concept_semtype__concept_id = current_concept_id ). \
                    filter( concept_semtype__semtype__in = semantic_types ). \
                    distinct()            
        
        if( len(text_filter)>0 ):
            if VERBOSE: 
                print >>logger, u"filtered with search[value="+unicode(text_filter)+u"]...",
                                
            textQFilter = Q( predicationargument__concept_semtype__concept__preferred_name__icontains=text_filter ) | \
                          Q( predicationargument__predication__predicate__icontains=text_filter )
        
        if not textQFilter == None:
            if not typeMatchedPredArgs == None:      
                filteredHitCount = \
                    Predication.objects.using(self.getDsid()).\
                        filter( predication_id__in=totalHits ).\
                        filter( predicationargument__in = typeMatchedPredArgs ).\
                        filter( textQFilter ).distinct().count()
            else:
                filteredHitCount = \
                    Predication.objects.using(self.getDsid()).\
                        filter( predication_id__in=totalHits ).\
                        filter( textQFilter ).distinct().count()
                
            if VERBOSE: print >>logger, "found "+str(filteredHitCount)+" filtered entries...",
            
        else: # textQFilter == None
            
            if not typeMatchedPredArgs == None:
                    
                filteredHitCount = \
                    Predication.objects.using(self.getDsid()).\
                        filter( predication_id__in=totalHits ).\
                        filter( predicationargument__in = typeMatchedPredArgs ).distinct().count()

            else: # typeMatchedPredArgs == None
                filteredHitCount = totalHitCount

        # ...Now, retrieve the requested predications (optionally filtered by type_filter and text_filter)...
        if(length<1):
            if VERBOSE: 
                print >>logger, "from start["+str(start)+"] to end of table"
                
            if not textQFilter == None:
                if not typeMatchedPredArgs == None:      
                    predications = \
                        Predication.objects.using(self.getDsid()).\
                            filter(predication_id__in=totalHits).\
                            filter( predicationargument__in = typeMatchedPredArgs ).\
                            filter( textQFilter ).distinct()[start:]
                else:
                    predications = \
                        Predication.objects.using(self.getDsid()).\
                            filter( predication_id__in=totalHits ).\
                            filter( textQFilter ).distinct()[start:]
                
            else: # textQFilter == None
                
                if not typeMatchedPredArgs == None:
                        
                    predications = \
                        Predication.objects.using(self.getDsid()).\
                            filter( predication_id__in=totalHits ).\
                            filter( predicationargument__in = typeMatchedPredArgs ).distinct()[start:]
    
                else: # typeMatchedPredArgs == None
                    predications = \
                        Predication.objects.using(self.getDsid()).filter(
                            predication_id__in=totalHits
                    )[start:]
        else:
            end = start + int(length)
            if VERBOSE: 
                print >>logger, "from start["+str(start)+"] to end["+str(end-1)+"]"
                
            if not textQFilter == None:
                if not typeMatchedPredArgs == None:      
                    predications = \
                        Predication.objects.using(self.getDsid()).\
                            filter(predication_id__in=totalHits).\
                            filter( predicationargument__in = typeMatchedPredArgs ).\
                            filter( textQFilter ).distinct()[start:end]
                else:
                    predications = \
                        Predication.objects.using(self.getDsid()).\
                            filter( predication_id__in=totalHits ).\
                            filter( textQFilter ).distinct()[start:end]
                
            else: # textQFilter == None
                
                if not typeMatchedPredArgs == None:
                        
                    predications = \
                        Predication.objects.using(self.getDsid()).\
                            filter( predication_id__in=totalHits ).\
                            filter( predicationargument__in = typeMatchedPredArgs ).distinct()[start:end]
    
                else: # typeMatchedPredArgs == None
                    predications = \
                        Predication.objects.using(self.getDsid()).filter(
                            predication_id__in=totalHits
                    )[start:end]
                
        # then construct the full list of the complete predication details
        results = {}
        
        for predication in predications:
            pid = predication.predication_id 
            if pid not in results:
                results[pid] = {} # first time seen... create...
                results[pid]['predication_id'] = pid
                results[pid]['predicate'] = predication.predicate
                results[pid]['evidence_count'] = self.countEvidenceForPredication(pid)
                results[pid]['score'] = ""  # blank for now
                results[pid]['tophit'] = 0  # default to False
            
            # SemMedDb predication_argument entries should normally be found in pairs, of subject and object concepts
            # however, for whatever reason, this cardinality is not always honored, so here, we attempt to compensate for this
            # by comparing the 'current_concept_id' against the predication_argument concept_semtypes, to try to pick the 
            # correct ones to sent back. This will work fine if there is a unique subject or object asserted for the current_concept_id
            predargs = predication.predicationargument_set.all()
            subjects = {}
            objects  = {}
            for pa in predargs:
                
                concept_id  = pa.concept_semtype.concept.concept_id
                
                # check for top ranked hits
                if concept_id in tophits: 
                    if VERBOSE: print >>logger,  "\nFound concept_id:",concept_id," in top hits: ",tophits
                    results[pid]['tophit'] = 1
                
                conceptType = pa.concept_semtype.concept.type
                
                if pa.type == 'S':
                    subjects[concept_id] = {}
                    subjects[concept_id]['concept_id'] = concept_id
                    subjects[concept_id]['type']       = conceptType
                    subjects[concept_id]['semtype']    = pa.concept_semtype.semtype
 
                    if conceptType == 'ENTREZ':
                        # assume preferred_name is a gene symbol to be used as the cui
                        subjects[concept_id]['cui']  = pa.concept_semtype.concept.preferred_name
                        # record the entrez_id for possible future reference
                        subjects[concept_id]['entrez_id']  = pa.concept_semtype.concept.cui
                    else:
                        subjects[concept_id]['cui']  = pa.concept_semtype.concept.cui

                    subjects[concept_id]['name'] = pa.concept_semtype.concept.preferred_name
                    
                elif pa.type == 'O':
                    
                    objects[concept_id]  = {}
                    objects[concept_id]['concept_id']  = concept_id
                    objects[concept_id]['type'] = conceptType
                    objects[concept_id]['semtype'] = pa.concept_semtype.semtype
                    
                    if conceptType == 'ENTREZ':
                        # assume preferred_name is a gene symbol to be used as the cui
                        objects[concept_id]['cui']  = pa.concept_semtype.concept.preferred_name
                        # record the entrez_id for possible future reference
                        objects[concept_id]['entrez_id'] = pa.concept_semtype.concept.cui
                    else:
                        objects[concept_id]['cui']  = pa.concept_semtype.concept.cui
                        
                    objects[concept_id]['name'] = pa.concept_semtype.concept.preferred_name
                    
                else:
                    print >>logger, "Unknown PredicationArgument type: ",pa.type
                    continue
        
            try:
                if current_concept_id == 0:
                    
                    results[pid]['subject'] = subjects.values()[0]  # ...default choice, may sometimes fail
                    results[pid]['object']  = objects.values()[0]   # ...default choice, may sometimes fail

                elif current_concept_id in subjects:
                    
                    results[pid]['subject'] = subjects[current_concept_id]
                    results[pid]['object']  = objects.values()[0]  # ...heuristic choice, may sometimes fail
                    
                else:
                    
                    results[pid]['object']  = objects[current_concept_id]
                    results[pid]['subject'] = subjects.values()[0]  # ...heuristic choice, may sometimes fail
                    
            except Exception as e:
                    print >>logger, "retrievePredications error: orphaned predication arguments encountered? ",e
                    continue                
                
        # aggregate the results as a list
        results = list(results.values())
        
        if VERBOSE:
            print >>logger,  "\nquery.retrievePredications() returns:",
            print >>logger,  "\nTotal Hits:", totalHitCount,", Filtered:", filteredHitCount,"Results:\n", results

        return results, totalHitCount, filteredHitCount
    
    """
     User Story #1a - 30-01-2015
     
     Show the relations associated with particular SemMedDb concept
     
     a. Input: 
        cui   = CUI of concept to search for
        start: offset to first entry in database
        length: number of entries to return
        text_filter: string filter to apply to the resulting concept and predicate name strings
        orderColumn: current target of field sorting: 0 == 'subject', 1 == 'predicate', 2 == 'object' field
        orderDir: sort ordering imposed 'orderColumn' field: 'asc' == ascending, 'desc' == descending (default : 'asc')
     
     b. Output:  returns a tuple with
        [0] table of relations (as an array of dictionary indexed entries?) i.e.
        subject_text    predicate    object_text    sentence pmid
        epididymis    LOCATION_OF    obstruction    The surgical treatment of sterility due to obstruction at the epididymis    12255534
        
        [1] number of total Hits
        [2] number of filtered Hits

    Revisions: 
        09-03-2015 added 'start', 'length', 'text_filter', 'orderColumn' and 'orderDir' arguments to support server-side paging
        03-04-2015 cui can now accept either a single identifier or a JSON formatted dictionary of identifiers
        
    LIMITATION: subject, predict, object concept string field ordering is NOT yet implemented (problematic?)

    """
    def retrievePredicationsByCUI(
            self, 
            query, 
            type_filter='', 
            aliases=None, 
            start=0, 
            length=-1,
            text_filter = "",
            orderColumn = '0', # ignored at present
            orderDir = 'asc'   # ignored at present
     ):
        if( query == None or len(str(query).strip()) == 0 ): return [],0,0
            
        concept_id = self.getConceptIdByCUI(query)
        
        if concept_id == None: return [],0,0
        
        # BD3K Hackathon FastR integration point - TURNED OFF FOR NOW
        tophits = [] # self.getTopFastRHits(concept_id)

        return self.retrievePredicationsByConceptId(
                    concept_id,  type_filter, aliases,
                    start, length, text_filter,
                    orderColumn, orderDir, tophits   
        )


    def retrievePredicationsByConceptId(
            self, 
            concept_id, 
            type_filter='', 
            aliases=None, 
            start=0, 
            length=-1,
            text_filter = "",
            orderColumn = '0', # ignored at present
            orderDir = 'asc',  # ignored at present
            tophits = []
     ):

        if VERBOSE:
            print >>logger,\
                "\nsemmedb.service.retrievePredicationsByConceptId("+\
                " concept_id = '"+str(concept_id)+\
                " type_filter = '"+str(type_filter)+\
                " aliases = '"+str(aliases)+\
                "', start = "+str(start)+\
                ", length = "+str(length)+\
                ", text_filter = '"+str(text_filter)+\
                "', orderColumn = "+str(orderColumn)+\
                ", orderDir = '"+str(orderDir)+\
                ", tophits = '",tophits,\
                "' )"

        # sanity checks
        if( concept_id == None ): 
            if aliases== None or len(str(aliases).strip())==0: return [],0,0
            
        # sanity check on the integral value of concept_id
        try:
            concept_id  = int(concept_id)
        except (ValueError, TypeError):
            print >>logger, "ERROR - retrievePredicationsByConceptId(): Invalid concept_id["+str(concept_id)+"]?"
            return [],0,0

        if( concept_id == 0 ): return [],0,0

        # OLD CODE that screened on identifier aliases
        # deprecated somewhat, if only because it is so slow!
        if not aliases == None and len(aliases)>0: # JSON catalog of alias identifiers for the concept?

            aliases = self.htmlparser.unescape(aliases)

            if VERBOSE:
                print >>logger, \
                    "Attempting parsing of candidate JSON string "+\
                    "specifying concept_id catalog:\n",aliases,"\n"
            
            # Parse JSON dictionary of diverse identifiers 
            catalog = json.loads(aliases) 
            qFilter = []
            for idType in catalog:
                
                # should be an array of identifiers
                identifiers = catalog[idType]
                
                if idType == 'OM':
                    # query on the 'omim' field? 
                    # This is tricky since the field is a long string
                    # of colon delimited OMIM numbers...
                    # Here, I'll attempt a simple minded string match
                    for identifier in identifiers:
                        qFilter.append({ 
                                        'predicationargument__concept_semtype__concept__omim__contains' : identifier, 
                                        'predicationargument__concept_semtype__concept__is_orphan'  : False, 
                                        })
                
                else:  # EG or UMLS are used directly
                    qFilter.append({ 
                                    'predicationargument__concept_semtype__concept__cui__in' : identifiers, 
                                    'predicationargument__concept_semtype__concept__is_orphan'  : False,
                                    })
                    
            if len(qFilter) == 0:
                return [],0,0
            
            if VERBOSE: print >>logger,"qFilter: ",qFilter
                        
            return self.retrievePredications(
                            qFilter, concept_id, start, length, type_filter, text_filter, orderColumn, orderDir, tophits
                        )
                          
        else:  # just assume you should pass the concept_id on as a simple query on concept_id  

            return self.retrievePredications(
                            { 
                                'predicationargument__concept_semtype__concept__concept_id' : concept_id, 
                                'predicationargument__concept_semtype__concept__is_orphan'  : False,
                            },
                            concept_id, 
                            start, length, 
                            type_filter, text_filter, 
                            orderColumn, orderDir, 
                            tophits
                        )
    
   
    """
     User Story, subsidiary use case #2a - 02-02-2015
     
     Search concepts in SemMedDb by partial matching of searchString 
     text query against the SemMedDb Concept table preferred_names field
     
     See retrieveConceptsBySearchString below for inputs and outputs

    """
    def retrieveConceptsBySearchString( 
            self, 
            searchString, 
            start = 0, 
            length = -1, 
            exact = False, 
            insensitive = True,
            text_filter = '',
            orderDir = 'asc'
     ):
        if VERBOSE:
            print >>logger,\
                u"\nsemmeddb.service.retrieveSemMedDbConceptsBySearchString("+\
                u" searchString = '"+unicode(searchString)+"'",
            print >>logger,\
                ", start = "+str(start)+\
                ", length = "+str(length)+\
                ", exact = "+str(exact)+\
                ", insensitive = "+str(insensitive),
            print >>logger,\
                u", text_filter = '"+unicode(text_filter),
            print >>logger,\
                "', orderDir = '"+str(orderDir)+\
                "' )"

        # sanity check for empty searchString
        searchString = unicode(searchString).strip()
        if not len(searchString) > 0:
            return [],0,0
        
        # sanity check on the integral values of start and length
        try:
            start  = int(start)
            start  = 0 if start < 0 else start
            length = int(length) 
        except (ValueError, TypeError):
            print >>logger, "ERROR - retrieveSemMedDbConceptsBySearchString(): Invalid start["+str(start)+"] or length["+str(length)+"]?"
            return [],0,0
        
        # Set up appropriate baseline query filter
        if VERBOSE: print >>logger, "Searching for '",searchString,"'...",
        
        conceptQueryFilter = {}
        if(exact):
            if VERBOSE: print >>logger, "exact match...",
            if(insensitive):
                if VERBOSE: print >>logger, "case insensitive...",
                conceptQueryFilter['preferred_name__iexact'] = searchString ;
            else:
                if VERBOSE: print >>logger, "case sensitive...",
                conceptQueryFilter['preferred_name__exact'] = searchString ;
        else:
            if VERBOSE: print >>logger, "inexact match...",
            if(insensitive):
                if VERBOSE: print >>logger, "case insensitive...",
                conceptQueryFilter['preferred_name__icontains'] = searchString ;
            else:
                if VERBOSE: print >>logger, "case sensitive...",
                conceptQueryFilter['preferred_name__contains'] = searchString ;
        
        # only show concepts that are not orphans - 
        # i.e. that have predications (this flag was set by semmeddb.data_audit)
        conceptQueryFilter['is_orphan'] = False ;
        
        # count total hits on the original string query
        totalHitCount = Concept.objects.using(self.getDsid()).distinct().filter(**conceptQueryFilter).count()
        
        # get any DT search filter applied by the user...
        if(text_filter != None):
            text_filter = text_filter.strip()
        
        # ... and computer number filtered hits
        if(len(text_filter)>0):
            if VERBOSE: print >>logger, "filtered with search[value="+str(text_filter)+"]...",
            filteredHitCount = Concept.objects.using(self.getDsid()).distinct().filter(**conceptQueryFilter).filter(preferred_name__icontains=text_filter).count()
        else:
            filteredHitCount = totalHitCount

        # set ordering of results
        if not orderDir == None and str(orderDir) == 'desc':
            orderBy = ['-preferred_name']
        else:
            orderBy = ['preferred_name']
        
        # Finally, get the list of actual results of the filtered search
        # from the page start offset and of length requested
        if(length<1):
            if VERBOSE: 
                print >>logger, "from start["+str(start)+"] to end of table"
            if(len(text_filter)>0):
                # screen with search filter
                concepts = \
                    Concept.objects.using(self.getDsid()).distinct().\
                        filter(**conceptQueryFilter).\
                            filter(preferred_name__icontains=text_filter).\
                                order_by(*orderBy)[start:]
            else:
                # otherwise, take all hits
                concepts = \
                    Concept.objects.using(self.getDsid()).distinct().\
                        filter(**conceptQueryFilter).\
                            order_by(*orderBy)[start:]
        else:
            # filter slice computed from length added to start
            end = start + int(length) 
            if VERBOSE: 
                print >>logger, "from start["+str(start)+"] to end["+str(end-1)+"]"
            if(len(text_filter)>0):
                # screen with search filter
                concepts = \
                    Concept.objects.using(self.getDsid()).distinct().\
                        filter(**conceptQueryFilter).\
                            filter(preferred_name__icontains=text_filter).\
                                order_by(*orderBy)[start:end]
            else:
                # otherwise, take all hits
                concepts = \
                    Concept.objects.using(self.getDsid()).distinct().\
                        filter(**conceptQueryFilter).\
                            order_by(*orderBy)[start:end]

        results = [ 
            { 
                "concept_id"  : c.concept_id,
                "cui"  : c.cui, 
                "name" : c.preferred_name,
                "type" : c.type,
                "semtype" : " ",
            } for c in concepts]
            
        for entry in results:
            semtypes = ConceptSemtype.objects.using(self.getDsid()).distinct().filter(concept=entry["concept_id"])
            for semtype in semtypes:
                entry["semtype"] = semtype.semtype
                break ;
        
        if TRACE:
            print >>logger, "semmeddb.service.retrieveSemMedDbConceptsBySearchString returns:\n",results
        
        # return the results plus the total number of 
        # available matching records in the database 
        return results, totalHitCount, filteredHitCount
  
    """
     User Story #3 - 02-02-2015
     
     Show the predications extracted from a particular abstract (PMID)
    
     a. Input: 
        PMID
        start: offset to first entry in database
        length: number of entries to return
        text_filter: string filter to apply to the resulting concept name strings
        orderColumn: current target of field sorting: 0 == 'subject', 1 == 'predicate', 2 == 'object' field
        orderDir: sort ordering imposed 'orderColumn' field: 'asc' == ascending, 'desc' == descending (default : 'asc')

     b. Output:  returns a tuple with
        [0] result list of associated Predications 
        [1] number of total Hits
        [2] number of filtered Hits
     
    Revisions: 
        09-03-2015, added 'start', 'length', 'text_filter', 'orderColumn' and 'orderDir' arguments to support server-side paging
        
    LIMITATION: subject, predict, object concept string field ordering is NOT yet implemented (problematic?)

    """
    def retrievePredicationsByPmid(
            self,
            pmid,
            start = 0,
            length = -1,
            type_filter = "",
            text_filter = "",
            orderColumn = '0',
            orderDir = 'asc'
    ):
        # sanity check
        if(pmid==None or len(pmid.strip())==0 ): return [],0,0
        
        return self.retrievePredications(
                    { 'sentencepredication__sentence__pmid' : pmid },
                    0, # current_concept_id is undefined 
                    start, length, type_filter, text_filter, orderColumn, orderDir
                )

    """
     User Story #4 - 16-02-2015
     
     Show the sentences related to a given predication
    
     a. Input: 
        predication_id
        start: offset to first entry in database
        length: number of entries to return
        text_filter: string filter to apply to the resulting concept name strings
        orderDir: sort ordering imposed on sentence string field: 'asc' == ascending, 'desc' == descending (default : 'asc')

     b. Output: returns a tuple with
        [0] list of dictionary entries of sentences associated with given predication id, i.e.
        
            sentence                                                                    pmid
            The surgical treatment of sterility due to obstruction at the epididymis    12255534
             
        [1] number of total Hits
        [2] number of filtered Hits
      
    Revision: 
        03-03-2015, added 'start', 'length', and 'text_filter" arguments to support server-side paging

    """
    def retrieveSentencesByPredication(
            self,
            predication_id, 
            start = 0, 
            length = -1,
            text_filter = "",
            orderDir = 'asc'
    ):
        if VERBOSE:
            print >>logger,\
                "\nsemmeddb.service.retrieveSentencesByPredication("+\
                " predication_id = '"+str(predication_id)+\
                "', start = "+str(start)+\
                ", length = "+str(length)+\
                ", text_filter = '"+str(text_filter)+\
                "', orderDir = '"+str(orderDir)+\
                "' )"
 
        # sanity check on the integral values of start and length
        try:
            predication_id  = int(predication_id)
            start  = int(start)
            start  = 0 if start < 0 else start
            length = int(length) 
        except (ValueError, TypeError):
            print >>logger, \
                "ERROR - retrieveSentencesByPredication(): Invalid predication_id["+str(predication_id)+\
                "], start["+str(start)+"] or length["+str(length)+"]?"
            return [],0,0
        
        # First, get all the SentencePredications related to a given predication
        matched_sentence_ids =  \
            [ sp.sentence_id for sp in SentencePredication.objects.using(self.getDsid()).filter(predication=predication_id)]
        if VERBOSE: print >>logger, " matched SentencePredication.sentence_ids:\n"+str(matched_sentence_ids)
                
        # TODO: validate accuracy of this count...  Might be over counting the number of
        # the number of distinct sentences (which is what you are really after here...)
        totalHitCount = \
            Sentence.objects.using(self.getDsid()).\
                filter(sentence_id__in=matched_sentence_ids).\
                    count()
        
        if VERBOSE: print >>logger, " totalHitCount = "+str(totalHitCount)
        
        # get any DT search filter applied by the user...
        if(text_filter != None): text_filter = text_filter.strip()
        
        # ... and computer number filtered hits
        if(len(text_filter)>0):
            if VERBOSE: print >>logger, "filtered with search[value="+str(text_filter)+"]...",
            filteredHitCount = \
                Sentence.objects.using(self.getDsid()).\
                    filter(sentence_id__in=matched_sentence_ids).\
                        filter(sentence__icontains=text_filter).\
                            count()
        else:
            filteredHitCount = totalHitCount
        if VERBOSE: print >>logger, " filteredHitCount = "+str(filteredHitCount)

        # set ordering of results
        if not orderDir == None and str(orderDir) == 'desc':
            orderBy = ['-sentence']
        else:
            orderBy = ['sentence']
        
        if(length<1):
            if VERBOSE: print >>logger, "from start["+str(start)+"] to end of table"
            if(len(text_filter)>0):
                sentences =\
                    Sentence.objects.using(self.getDsid()).\
                        filter(sentence_id__in=matched_sentence_ids).\
                            filter(sentence__icontains=text_filter).\
                                    order_by(*orderBy)[start:]
            else:
                sentences =\
                    Sentence.objects.using(self.getDsid()).\
                        filter(sentence_id__in=matched_sentence_ids).\
                                    order_by(*orderBy)[start:]                
        else:
            end = start + int(length) 
            if VERBOSE: print >>logger, "from start["+str(start)+"] to end["+str(end-1)+"]"
            if(len(text_filter)>0):
                sentences =\
                    Sentence.objects.using(self.getDsid()).\
                        filter(sentence_id__in=matched_sentence_ids).\
                            filter(sentence__icontains=text_filter).\
                                order_by(*orderBy)[start:end]
            else:
                sentences =\
                    Sentence.objects.using(self.getDsid()).\
                        filter(sentence_id__in=matched_sentence_ids).\
                            order_by(*orderBy)[start:end]
        results = [ 
            { 
                "sentence_id" : s.sentence_id,
                "pmid"        : s.pmid,
                "sentence"    : s.sentence,
              
            } for s in sentences ]

        if TRACE:
            print >>logger,  "\nquery.retrieveSentencesByPredication() returns:",
            print >>logger,  "\nTotal Hits:", totalHitCount,", Filtered:", filteredHitCount,"Results:\n", results

        # then construct the associated list of the associated sentences     
        return results, totalHitCount, filteredHitCount

    """
    Predication 'evidence' count consists of the count of 
    sentences associated with the predication
    """
    def countEvidenceForPredication(self,predication_id):
        totalHitCount = \
            Sentence.objects.using(self.getDsid()).distinct().\
                filter(sentencepredication__predication=predication_id).\
                    count()
        return totalHitCount
    
    """
    Count number of Predications associated with a PMID
    """
    def countPredicationsForPmid(self,pmid):
        
        results,totalHits,filteredHits = self.retrievePredicationsByPmid(pmid)
        
        filteredHits = filteredHits # NOP to ignore variable
        
        subjectTypeCounts = {}
        objectTypeCounts  = {}
        for hit in results:
            
            subType = hit['subject']['semtype']
            if not subType in subjectTypeCounts:
                subjectTypeCounts[subType]  = 1
            else:
                subjectTypeCounts[subType] += 1
                
            objectType = hit['object']['semtype']
            if not objectType in objectTypeCounts:
                objectTypeCounts[objectType]  = 1
            else:
                objectTypeCounts[objectType] += 1
                
        return totalHits,subjectTypeCounts,objectTypeCounts
    
    """
    Count number of Predications associated with a PMID
    """
    def countDistinctConceptsForPmidOfSemType(self, pmid, semtype):
        
        results,totalHits,filteredHits = self.retrievePredicationsByPmid(pmid)
        
        totalHits=totalHits #nop
        filteredHits=filteredHits # nop
        
        conceptMap = {}
        for hit in results:
            if hit['subject']['semtype'] == semtype:
                cui = hit['subject']['concept_id']
                if not cui in conceptMap:
                    conceptMap[cui]  = 1
                else:
                    conceptMap[cui] += 1

            if hit['object']['semtype']  == semtype:
                cui = hit['object']['concept_id']
                if not cui in conceptMap:
                    conceptMap[cui]  = 1
                else:
                    conceptMap[cui] += 1
                    
        if VERBOSE:
            print >>logger,  "\ncountDistinctConceptsForPmidOfSemType Results:\n", conceptMap        
        
        return conceptMap
        
    """
    Service to retrieve a list of PMID's identified by concept_id

    """
    def retrieveCitationsByCUI(self, cui):
        concept_citations = ConceptCitations.objects.using(self.getDsid()).filter(cui__iexact=cui)
        if(concept_citations.count()>0):
            # assume only that one record is returned (ignore the rest?)
            # return the list of PMID's for now
            return concept_citations[0].pmid.split(',')
        else:
            return None
    
    """
    Service to retrieve a list of PMID's identified by CUI

    """
    def retrieveCitationsByConcept(self, cui):
        concept_citations = ConceptCitations.objects.using(self.getDsid()).filter(cui__iexact=cui)
        if(concept_citations.count()>0):
            # assume only that one record is returned (ignore the rest?)
            # return the list of PMID's for now
            return concept_citations[0].pmid.split(',')
        else:
            return None
    
    def getConceptDefinition(self,concept_id):
        return u''
    
    """
    Method wraps the details of a Concept, assumed 
    retrieved in the result list passed as the argument
    
    """
    def retrieveConceptDetails(self,result):
        
        if VERBOSE: print >>logger,"Entering SemMedDb.retrieveConceptDetails()"
            
        concept = {}
        concept['concept_id'] = result[0].concept_id
        concept['type']       = result[0].type
        
        if(concept['type']=="ENTREZ"):
            
            # Use the gene symbol instead of the Entrez ID as the CUI...
            concept['cui']  = result[0].preferred_name
            # ...but save the id as a cross-reference
            concept['terms'] = [result[0].cui]
            concept['semtype'] = u'gngm' # we know that this is a gene...
            
        else:
            concept['cui'] = result[0].cui
            concept['semtype'] = u'' # don't yet have the context to decide this here
            
        concept['name']       = result[0].preferred_name
        concept['ghr']        = result[0].ghr
        concept['omim']       = result[0].omim
        return concept

    """
    Service to retrieve details of a Concept identified by SemMedDb concept_id
    
    """
    def getConceptDetails(self,concept_id):
        
        if VERBOSE: print >>logger,"Entering SemMedDb.getConceptDetails(",concept_id,")"
        
        result = Concept.objects.using(self.getDsid()).filter( concept_id = concept_id )
        if(result.count()>0):
            concept = self.retrieveConceptDetails(result)
            concept["query"] = concept_id
            return concept
        else:
            return None
        
        
    def getConceptSemtype(self,relation_id,concept_id):
        
        result = ConceptSemtype.objects.using( self.getDsid() ).\
                    filter( 
                        concept_id = concept_id, 
                        predicationargument__predication_id = relation_id
                    )
        if(result.count()>0):            
            return result[0].semtype
        else:
            return u''
           
    """
    Service to retrieve details of a Concept identified by SemMedDb CUI
    
    April 22, 2015 Revision: the CUI (cross-species) for an Entrez gene is 
    assumed to be the gene symbol exactly matching the Concept.name field 
    but, for other concepts, it should match the Concept.cui field itself
    
    """
    def getConceptDetailsByCUI(self,cui):
        # sanity check on the values of a cui
        # Also is basic protection against SQL injections on the field
        if cui == None or not cui.isalnum():  return None

        if VERBOSE: print >>logger,"Entering SemMedDb.getConceptDetailsByCUI(",cui,")"
        
        queryFilter =  Q(cui__iexact=cui) | Q( type='ENTREZ', preferred_name__iexact = cui )
        result = Concept.objects.using(self.getDsid()).filter(queryFilter)
        
        if(result.count()>0):
            concept = self.retrieveConceptDetails(result)
            concept["query"] = cui
            return concept
        else:
            return None
        
    """
    Lightweight version of getConceptDetailsByCUI(cui)
    """
    def getConceptIdByCUI(self,cui):
        # sanity check on the values of alias, percentile, start and length
        # Also is basic protection against SQL injections on these fields
        if cui == None or not cui.isalnum():  return None

        if VERBOSE: print >>logger,"Entering SemMedDb.getConceptDetailsByCUI(",cui,")"
        
        queryFilter =  Q(cui__iexact=cui) | Q( type='ENTREZ', preferred_name__iexact = cui )
        results = Concept.objects.using(self.getDsid()).filter(queryFilter).values('concept_id')
        
        if(len(results)>0):
            return results[0]['concept_id']
        else:
            return None
    
    """
    Service to retrieve details of a Citation identified by PMID
    
    """
    def getCitationDetails(self,pmid):
        citations = Citations.objects.using(self.getDsid()).filter(pmid=pmid)
        if(citations.count()>0):
            return citations[0]
        else:
            return None

    def getTopFastRHits(self,concept_id):
        
        if concept_id == None or concept_id == 0 or len(str(concept_id))==0:
            return []
        
        try:
            fastRURL = urllib.urlopen(DSID.FASTR+str(concept_id))
            tophitstring = fastRURL.read()
            fastRURL.close()

            # Check for four oh four
            # TODO: Check if this code is necessary; if no user data returns empty string, then..
            if( 
                tophitstring.find("404 - Not Found") >= 0 or 
                tophitstring.startswith("<html>") # probably an error message from the FastR server
            ):
                print >>logger,"getTopFastRHits(): Endpoint '"+DSID.FASTR+str(concept_id)+"' is not online?"
                return []
            elif len(tophitstring.strip())==0:
                return []
            
            # sanity check: strip off flanking delimiters and white space
            tophits = []
            for hit in tophitstring.split(DSID.FASTR_DELIMITER):
                hit = hit.strip()
                if(len(hit)==0): continue
                hit = int(hit)
                if hit == concept_id: continue # ignore itself
                tophits.append(hit)

            return tophits
        
        except IOError as IOE:
            print >>logger,"getTopFastRHits(): IOError?",IOE
            return []
    
        