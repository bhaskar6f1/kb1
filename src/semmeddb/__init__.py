#!/usr/local/bin/python
# coding: utf-8
"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import sys

from django.utils import timezone

from query.datasource import DSID

from models import ( 
    Concept,
    ConceptSemtype,
    Predication,
    PredicationArgument,
    Sentence,
    SentencePredication,
    Citations,
    ConceptCitations
)

class SemMedDbTestData:
    
    CONCEPT1_ID  = None
    CONCEPT3_ID  = None
    
    CONCEPT9_CUI  = '216'
    CONCEPT9_TYPE = 'ENTREZ'
    CONCEPT9_NAME = u"ALDH1A1"

    CONCEPT10_CUI  = 'META'
    CONCEPT10_TYPE = 'C0151449'
    CONCEPT10_NAME = u"primary sjögren's syndrome"
    
    PREDICATION1 = None
    PREDICATION3 = None

"""
Semantic Medline Database Test Query  for unit testing of system
"""    
def loadSemMedDbTestData():
    
    dsid = DSID.SEMMEDDB
    
    #print >>sys.stderr,"Executing SemMedDbTestQuery.loadTestData(",dsid,")"
    
    c1 = Concept.objects.using(dsid).create(
        cui = 'C0003873',
        type = 'META', 
        preferred_name = 'Rheumatoid Arthritis disease',
        ghr = '',
        omim = '180300:604302' 
    )
    
    SemMedDbTestData.CONCEPT1_ID = c1
    
    c2 = Concept.objects.using(dsid).create(
        cui = 'C0001234',
        type = 'META', 
        preferred_name = 'homozygous haemoglobin C disease',
        ghr  = '',
        omim = '' 
    )
    
    c3 = Concept.objects.using(dsid).create(
        cui = '215',
        type = 'ENTREZ', 
        preferred_name = 'ABCD1',
        ghr  = '',
        omim = '' 
    )
    SemMedDbTestData.CONCEPT3_ID = c3
    
    c4 = Concept.objects.using(dsid).create(
        cui = 'C0005678',
        type = 'META', 
        preferred_name = 'heart disease',
        ghr  = '',
        omim = '' 
    )
    
    c5 = Concept.objects.using(dsid).create(
        cui = 'C9876543',
        type = 'ENTREZ', 
        preferred_name = 'WRN1',
        ghr  = '',
        omim = '277700' 
    )
    
    c6 = Concept.objects.using(dsid).create(
        cui = 'C0101010',
        type = 'META', 
        preferred_name = 'benign haemoglobinopathy',
        ghr  = '',
        omim = '' 
    )
    
    c7 = Concept.objects.using(dsid).create(
        cui = 'C1855923',
        type = 'META', 
        preferred_name = 'CWH43',
        ghr  = '',
        omim = '' 
    )
    
    c8 = Concept.objects.using(dsid).create(
        cui = '120227',
        type = 'ENTREZ', 
        preferred_name = 'CYP2R1',
        ghr  = '',
        omim = '' 
    )
    
    c9 = Concept.objects.using(dsid).create(
        cui = SemMedDbTestData.CONCEPT9_CUI,
        type = SemMedDbTestData.CONCEPT9_TYPE,
        preferred_name = SemMedDbTestData.CONCEPT9_NAME,
        ghr  = '',
        omim = '' 
    )
    
    c10 = Concept.objects.using(dsid).create(
        cui = SemMedDbTestData.CONCEPT10_CUI,
        type = SemMedDbTestData.CONCEPT10_TYPE,
        preferred_name = SemMedDbTestData.CONCEPT10_NAME,
        ghr  = '',
        omim = '' 
    )
    
    cs1 = ConceptSemtype.objects.using(dsid).create(
            concept = c1, 
            semtype = 'dsyn',
            novel = 'Y', 
            umls = 'Y',
    )

    cs2 = ConceptSemtype.objects.using(dsid).create(
            concept = c2, 
            semtype = 'dsyn',
            novel = 'Y', 
            umls = 'Y',
    )

    cs3 = ConceptSemtype.objects.using(dsid).create(
            concept = c3, 
            semtype = 'gngm',
            novel = 'Y', 
            umls = 'Y',
    )

    cs4 = ConceptSemtype.objects.using(dsid).create(
            concept = c6, 
            semtype = 'dsyn',
            novel = 'Y', 
            umls = 'Y',
    )

    cs5 = ConceptSemtype.objects.using(dsid).create(
            concept = c4, 
            semtype = 'dsyn',
            novel = 'Y', 
            umls = 'Y',
    )

    cs9 = ConceptSemtype.objects.using(dsid).create(
            concept = c9, 
            semtype = 'gngm',
            novel = 'Y', 
            umls = 'N',
    )

    cs10 = ConceptSemtype.objects.using(dsid).create(
            concept = c10, 
            semtype = 'dsyn',
            novel = 'Y', 
            umls = 'Y',
    )

    s1 = Sentence.objects.using(dsid).create(
        pmid = '19855969',
        type = 'ti', 
        number = 1, 
        sentence = 'Rheumatoid Arthritis in patient with homozygous haemoglobin C disease.', 
    )

    s2 = Sentence.objects.using(dsid).create(
        pmid = '19855969',
        type = 'ab', 
        number = 1, 
        sentence = 'We report a case of RA in a 60-year-old woman with homozygous haemoglobin C disease.', 
    )

    s3 = Sentence.objects.using(dsid).create(
        pmid = '19855970',
        type = 'ab', 
        number = 1, 
        sentence = 'The ABCD1 locus variation controls variable penetrance in rheumatoid arthritis.', # disclaimer: this is a fake abstract sentence!
    )

    s4 = Sentence.objects.using(dsid).create(
        pmid = '19855969',
        type = 'ab', 
        number = 2, 
        sentence = 'These haemoglobin diseases are associated with characteristics abnormalities of the skeleton.', 
    )

    s5 = Sentence.objects.using(dsid).create(
        pmid = '19855969',
        type = 'ab', 
        number = 3, 
        sentence = 'Haemoglobin C disease is a benign hemoglobinopathy rarely associated with skeletal disorders.', 
    )

    p1 = Predication.objects.using(dsid).create(
        predicate = 'PROCESS_OF',
        type = 'semrep', 
    )
    
    SemMedDbTestData.PREDICATION1 = p1
    
    p2 = Predication.objects.using(dsid).create(
        predicate = 'IMPLICATED_WITH',
        type = 'semrep', 
    )
    
    p3 = Predication.objects.using(dsid).create(
        predicate = 'LINKED_TO',
        type = 'semrep', 
    )
    
    SemMedDbTestData.PREDICATION3 = p3
    
    p4 = Predication.objects.using(dsid).create(
        predicate = 'ASSOCIATED_WITH',
        type = 'semrep', 
    )
    
    p5 = Predication.objects.using(dsid).create(
        predicate = 'IS_A',
        type = 'semrep', 
    )
    
    p6 = Predication.objects.using(dsid).create(
        predicate = 'ASSOCIATED_WITH',
        type = 'semrep', 
    )
    
    p7 = Predication.objects.using(dsid).create(
        predicate = 'AFFECTS',
        type = 'semrep', 
    )
    
    creation_timestamp = timezone.now()
    
    SentencePredication.objects.using(dsid).create(
        sentence = s1,
        predication = p1, 
        predication_number = 1,
        curr_timestamp = creation_timestamp
    )
    
    PredicationArgument.objects.using(dsid).create(
        predication = p1,           
        concept_semtype = cs1,       
        type = 'S', 
    )

    PredicationArgument.objects.using(dsid).create(
        predication = p1,           
        concept_semtype = cs2,       
        type = 'O', 
    )

    SentencePredication.objects.using(dsid).create(
        sentence = s2,
        predication = p1, 
        predication_number = 1,
        curr_timestamp = creation_timestamp
    )
    
    SentencePredication.objects.using(dsid).create(
        sentence = s2,
        predication = p3, 
        predication_number = 1,
        curr_timestamp = creation_timestamp
    )
    
    SentencePredication.objects.using(dsid).create(
        sentence = s3,
        predication = p2, 
        predication_number = 1,
        curr_timestamp = creation_timestamp
    )
    
    SentencePredication.objects.using(dsid).create(
        sentence = s4,
        predication = p3, 
        predication_number = 1,
        curr_timestamp = creation_timestamp
    )
    
    SentencePredication.objects.using(dsid).create(
        sentence = s5,
        predication = p5, 
        predication_number = 1,
        curr_timestamp = creation_timestamp
    )
    
    PredicationArgument.objects.using(dsid).create(
        predication = p2,           
        concept_semtype = cs3,       
        type = 'S', 
    )

    PredicationArgument.objects.using(dsid).create(
        predication = p2,           
        concept_semtype = cs1,       
        type = 'O', 
    )

    PredicationArgument.objects.using(dsid).create(
        predication = p3,           
        concept_semtype = cs3,       
        type = 'S', 
    )

    PredicationArgument.objects.using(dsid).create(
        predication = p3,           
        concept_semtype = cs1,       
        type = 'O', 
    )

    PredicationArgument.objects.using(dsid).create(
        predication = p5,           
        concept_semtype = cs5,       
        type = 'S', 
    )
    
    PredicationArgument.objects.using(dsid).create(
        predication = p5,           
        concept_semtype = cs4,       
        type = 'O', 
    )
    
    PredicationArgument.objects.using(dsid).create(
        predication = p6,           
        concept_semtype = cs4,       
        type = 'S', 
    )
    
    PredicationArgument.objects.using(dsid).create(
        predication = p6,           
        concept_semtype = cs2,       
        type = 'O', 
    )
    
    PredicationArgument.objects.using(dsid).create(
        predication = p7,           
        concept_semtype = cs9,       
        type = 'S', 
    )
    
    PredicationArgument.objects.using(dsid).create(
        predication = p7,           
        concept_semtype = cs10,       
        type = 'O', 
    )
    
    Citations.objects.using(dsid).create(
        pmid  = '19851774',
        issn  = '1432-203X',
        dp    = '',
        edat  = '',
        pyear = 2009
    )

    ConceptCitations.objects.using(dsid).create(
        cui  = 'C0003873',
        pmid = '19851774,19855969,15799417,19149034',
    )

    ConceptCitations.objects.using(dsid).create(
        cui  = 'C0001617',
        pmid = '15799417,19149034',
    )
    
