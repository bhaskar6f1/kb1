# coding: utf-8
"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import sys

from django.test import TestCase
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from gbk.settings import DB_ENGINE

from query.datasource import DSID
from query import QueryFactory

from models import ( 
    Concept,
    ConceptSemtype,
    Predication,
    PredicationArgument,
    Sentence,
    SentencePredication,
    Citations,
    ConceptCitations
)

from semmeddb import SemMedDbTestData

class SemMedDbBaselineTest(TestCase):
    
    def test_concept_retrieval(self):
        """Retrieve a Concept"""
        c = Concept.objects.using(DSID.SEMMEDDB).get(cui="C0003873")
        self.assertEqual( unicode(c.preferred_name), u'Rheumatoid Arthritis disease')
        self.assertEqual( unicode(c.omim), u'180300:604302')

    def test_concept_semtype_retrieval(self):
        """Retrieve a ConceptSemtype"""
        cs = ConceptSemtype.objects.using(DSID.SEMMEDDB).get(concept_semtype_id=1)
        self.assertEqual(cs.concept_id, 1)
        self.assertEqual( unicode(cs.semtype), u'dsyn')
        self.assertEqual( unicode(cs.novel), u'Y')
        self.assertEqual( unicode(cs.umls), u'Y')

    def test_predication_retrieval(self):
        """Retrieve a Predication"""
        p = Predication.objects.using(DSID.SEMMEDDB).get(predication_id=1)
        self.assertEqual( unicode(p.predicate), u'PROCESS_OF')
        self.assertEqual( unicode(p.type), u'semrep')

    def test_predication_argumentTest_retrieval(self):
        """Retrieve a PredicationArgument"""
        
        pa = PredicationArgument.objects.using(DSID.SEMMEDDB).get(predication_argument_id=1)
        self.assertEqual( unicode(pa.type), u'S')
        
        p = Predication.objects.using(DSID.SEMMEDDB).get(predication_id=pa.predication_id)
        self.assertEqual( unicode(p.predicate), u'PROCESS_OF')
        
        cs = ConceptSemtype.objects.using(DSID.SEMMEDDB).get(concept_semtype_id=pa.concept_semtype_id)
        
        c = Concept.objects.using(DSID.SEMMEDDB).get(concept_id=cs.concept_id)
        self.assertEqual( unicode(c.preferred_name), u'Rheumatoid Arthritis disease')

    def test_sentence_retrieval(self):
        """Retrieve a Sentence"""
        s = Sentence.objects.using(DSID.SEMMEDDB).get(sentence_id=1)
        self.assertEqual( unicode(s.pmid), u'19855969')
        self.assertEqual( unicode(s.type), u'ti')
        self.assertEqual(s.number, 1)
        self.assertEqual( unicode(s.sentence), u'Rheumatoid Arthritis in patient with homozygous haemoglobin C disease.')
   
    def test_sentence_predication_retrieval(self):
        """Retrieve a SentencePredication"""
        sp = SentencePredication.objects.using(DSID.SEMMEDDB).get(sentence_predication_id=1)
        
        s = Sentence.objects.using(DSID.SEMMEDDB).get(sentence_id=sp.sentence_id)
        self.assertEqual( unicode(s.pmid), u'19855969')
        
        p = Predication.objects.using(DSID.SEMMEDDB).get(predication_id=sp.predication_id)
        self.assertEqual( unicode(p.predicate), u'PROCESS_OF')

        self.assertEqual( sp.predication_number, 1)
        
        # Don't attempt to unit test the absolute equivalence 
        # of the time stamp saved here since for some reason, 
        # MySQL doesn't store and return the identical
        # datatime object under Linux python(?!??).. 
        # the in-memory version has extra detail(?)
        # self.assertEqual(sp.curr_timestamp, self.creation_timestamp)

    def test_concept_citation_retrieval(self):
        """Retrieve a Citation"""
        ci = Citations.objects.using(DSID.SEMMEDDB).get(pmid="19851774")
        self.assertEqual( unicode(ci.issn), u'1432-203X')
        self.assertEqual( ci.pyear, 2009)

    def test_citation_retrieval(self):
        """Retrieve a Concept Citation"""
        cci1 = ConceptCitations.objects.using(DSID.SEMMEDDB).get(cui="C0003873")
        self.assertEqual( unicode(cci1.cui), u'C0003873')
        self.assertEqual( unicode(cci1.pmid), u'19851774,19855969,15799417,19149034')
        cci2 = ConceptCitations.objects.using(DSID.SEMMEDDB).get(cui="C0001617")
        self.assertEqual( unicode(cci2.cui), u'C0001617')
        self.assertEqual( unicode(cci2.pmid), u'15799417,19149034')

class IntegratedSemMedDbTest(TestCase):
    
    def setUp(self):
        self.query_engine = QueryFactory(DSID.SEMMEDDB)
    
    def test_retrieve_relations_from_null_concept_identifier(self):
        
        results = self.query_engine.retrievePredicationsByConceptId(None)
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByConceptId(None):",
        self.assertEqual(results[1], 0)
        
        results = self.query_engine.retrievePredicationsByConceptId(None, '')
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByConceptId(''):",
        self.assertEqual(results[1], 0)
        
        results = self.query_engine.retrievePredicationsByConceptId(None, aliases='{}')
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByConceptId('()'):",
        self.assertEqual(results[1], 0)
        
    def test_retrieve_relations_from_cui(self):
        
        results,totalHits,filteredHits = self.query_engine.retrievePredicationsByCUI('C0003873')
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByConceptId('C0003873'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual(totalHits, 3)
        self.assertEqual(filteredHits, 3)
        self.assertEqual(len(results), 3)
        self.assertEqual(results[0]['predication_id'], 1)
        self.assertEqual(results[1]['predication_id'], 2)
        self.assertEqual(results[2]['predication_id'], 3)

    def test_retrieve_relations_from_concept_id(self):
        
        results,totalHits,filteredHits = self.query_engine.retrievePredicationsByConceptId(1)
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByConceptId(1):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual(totalHits, 3)
        self.assertEqual(filteredHits, 3)
        self.assertEqual(len(results), 3)
        self.assertEqual(results[0]['predication_id'], 1)
        self.assertEqual(results[1]['predication_id'], 2)
        self.assertEqual(results[2]['predication_id'], 3)

        results,totalHits,filteredHits = self.query_engine.retrievePredicationsByConceptId(1, start=1, length=2)
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByConceptId(1, start=1, length=2):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual(totalHits, 3)
        self.assertEqual(filteredHits, 3)
        self.assertEqual(len(results), 2)  # only a constrained range of filtered hits returned
        self.assertEqual(results[0]['predication_id'], 2)
        self.assertEqual(results[1]['predication_id'], 3)

        results,totalHits,filteredHits = self.query_engine.retrievePredicationsByConceptId(1, text_filter='haemoglobin')
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByConceptId(1, text_filter='haemoglobin'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual(totalHits, 3)
        self.assertEqual(filteredHits, 1)
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0]['predication_id'], 1)
        
        results,totalHits,filteredHits = self.query_engine.retrievePredicationsByConceptId(1, text_filter='ABCD1')
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByConceptId(1, text_filter='ABCD1'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual(filteredHits, 2)
        self.assertEqual(len(results), 2)
        self.assertEqual(results[0]['predication_id'], 2)
        self.assertEqual(results[1]['predication_id'], 3)
        
        results,totalHits,filteredHits = self.query_engine.retrievePredicationsByConceptId(1, text_filter='LINKED_TO')
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByConceptId(1, text_filter='LINKED_TO'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual(filteredHits, 1)
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0]['predication_id'], 3)
    
    """ # deprecated search modality for 'aliases'
     
    def test_retrieve_relations_from_concept_identifier_catalog(self):
        
        results,totalHits,filteredHits = self.query_engine.retrievePredicationsByConceptId(1, aliases='{"UMLS":["C0003873"],"EG":["ABCD"]}')
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByConceptId(1, aliases='{""UMLS"":""C0003873"",""EG"":""ABCD""}'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual(totalHits, 3)
        self.assertEqual(filteredHits, 3)
        self.assertEqual(len(results), 3)
        self.assertEqual(results[0]['predication_id'], 1)
        self.assertEqual(results[1]['predication_id'], 2)
        self.assertEqual(results[2]['predication_id'], 3)
    """
    
    def test_retrieve_concepts_like_search_string(self):
        
        # Default search with exact = False, insensitive = True
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('rheumatoid')
        print >>sys.stderr,  "\nSEMMEDDB.retrieveConceptsBySearchString('rheumatoid'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 1)
        self.assertEqual( unicode(concepts[0]['name']), u'Rheumatoid Arthritis disease')
        
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('rheumatoid arthritis disease', exact = True)
        print >>sys.stderr,  "\nSEMMEDDB.retrieveConceptsBySearchString('rheumatoid arthritis disease', exact = True):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 1)
        self.assertEqual( unicode(concepts[0]['name']), u'Rheumatoid Arthritis disease')

        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('rheumatoid', exact = True)
        print >>sys.stderr,  "\nSEMMEDDB.retrieveConceptsBySearchString('rheumatoid', exact = True):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 0)

        if(DB_ENGINE != 'sqlite3'):
            concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('rheumatoid', insensitive = False)
            print >>sys.stderr,  "\nSEMMEDDB.retrieveConceptsBySearchString('rheumatoid', insensitive = False):",
            print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
            self.assertEqual(len(concepts), 0)
        
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('Rheumatoid Arthritis disease', exact = True, insensitive = False)
        print >>sys.stderr,  "\nSEMMEDDB.retrieveConceptsBySearchString('Rheumatoid Arthritis disease', exact = True, insensitive = False):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 1)
        self.assertEqual(unicode(concepts[0]['name']), u'Rheumatoid Arthritis disease')

        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('rheumatoid', exact = True, insensitive = False)
        print >>sys.stderr,  "\nSEMMEDDB.retrieveConceptsBySearchString('rheumatoid', exact = True, insensitive = False):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 0)
 
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('disease')
        print >>sys.stderr,  "\nSEMMEDDB.retrieveConceptsBySearchString('disease'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 3)
        if(DB_ENGINE == 'mysql'):
            # slightly different collation order from sqlite3 (and perhaps others?)
            self.assertEqual(unicode(concepts[0]['cui']), u'C0005678')
            self.assertEqual(unicode(concepts[1]['cui']), u'C0001234')
            self.assertEqual(unicode(concepts[2]['cui']), u'C0003873')
        else:
            self.assertEqual(unicode(concepts[0]['cui']), u'C0003873')
            self.assertEqual(unicode(concepts[1]['cui']), u'C0005678')
            self.assertEqual(unicode(concepts[2]['cui']), u'C0001234')
        
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('disease', orderDir='desc')
        print >>sys.stderr,  "\nSEMMEDDB.retrieveConceptsBySearchString('disease', orderDir='desc'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 3)
        if(DB_ENGINE == 'mysql'):
            # slightly different collation order from sqlite3 (and perhaps others?)
            self.assertEqual(unicode(concepts[0]['cui']), u'C0003873')
            self.assertEqual(unicode(concepts[1]['cui']), u'C0001234')
            self.assertEqual(unicode(concepts[2]['cui']), u'C0005678')
        else:
            self.assertEqual(unicode(concepts[0]['cui']), u'C0001234')
            self.assertEqual(unicode(concepts[1]['cui']), u'C0005678')
            self.assertEqual(unicode(concepts[2]['cui']), u'C0003873')
        
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('disease' , start=1)
        print >>sys.stderr,  "\nSEMMEDDB.retrieveConceptsBySearchString('disease' , start=1):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 2)
        if(DB_ENGINE == 'mysql'):
            # slightly different collation order from sqlite3 (and perhaps others?)
            self.assertEqual(unicode(concepts[0]['cui']), u'C0001234')
            self.assertEqual(unicode(concepts[1]['cui']), u'C0003873')
        else:
            self.assertEqual(unicode(concepts[0]['cui']), u'C0005678')
            self.assertEqual(unicode(concepts[1]['cui']), u'C0001234')
        
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('disease', start=1, length=1)
        print >>sys.stderr,  "\nSEMMEDDB.retrieveConceptsBySearchString('disease' , start=1, length=1):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        if(DB_ENGINE == 'mysql'):
            # slightly different collation order from sqlite3 (and perhaps others?)
            self.assertEqual(unicode(concepts[0]['cui']), u'C0001234')
        else:
            self.assertEqual(len(concepts), 1)
            self.assertEqual(unicode(concepts[0]['cui']), u'C0005678')
        
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('disease' , text_filter='haem')
        print >>sys.stderr,  "\nSEMMEDDB.retrieveConceptsBySearchString('disease', text_filter='haem'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 1)
        self.assertEqual(unicode(concepts[0]['cui']), u'C0001234')
        
    def test_retrieve_concepts_like_search_string_with_foreign_characters(self):
        
        # Default search with exact = False, insensitive = True
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString(u'sjögren')
        print >>sys.stderr,  "\nSEMMEDDB.retrieveConceptsBySearchString('sjögren'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 1)
        self.assertEqual( unicode(concepts[0]['name']), SemMedDbTestData.CONCEPT10_NAME)
        
    def test_count_predications_for_pmid(self):
        totalHits,subjectTypeCount,objectTypeCount = self.query_engine.countPredicationsForPmid('19855969')
        print >>sys.stderr,  "\nSEMMEDDB.countPredicationsForPmid(19855969):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits
        print >>sys.stderr,  "\nSubject types:\n", subjectTypeCount,"\nObject types:\n", objectTypeCount
        self.assertEqual(totalHits, 3)
        
    def test_retrieve_relations_by_pmid(self):
        
        results,totalHits,filteredHits = self.query_engine.retrievePredicationsByPmid('19855969')
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByPmid(19855969):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual(totalHits, 3)
        self.assertEqual(filteredHits, 3)
        self.assertEqual(len(results), 3)
        self.assertEqual(results[0]['predication_id'], 1)
        self.assertEqual(results[1]['predication_id'], 3)
        self.assertEqual(results[2]['predication_id'], 5)

        results,totalHits,filteredHits = self.query_engine.retrievePredicationsByPmid('19855969', start=1, length=1)
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByPmid(19855969, start=1, length=1):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual(totalHits, 3)
        self.assertEqual(filteredHits, 3)
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0]['predication_id'], 3)

        results,totalHits,filteredHits = self.query_engine.retrievePredicationsByPmid('19855969', text_filter='haemoglobinopathy')
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByPmid(19855969, text_filter='haemoglobinopathy'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual(totalHits, 3)
        self.assertEqual(filteredHits, 1)
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0]['predication_id'], 5)

        results,totalHits,filteredHits = self.query_engine.retrievePredicationsByPmid('19855969', text_filter='PROCESS_OF')
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByPmid(19855969, text_filter='PROCESS_OF'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual(totalHits, 3)
        self.assertEqual(filteredHits, 1)
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0]['predication_id'], 1)

        results,totalHits,filteredHits = self.query_engine.retrievePredicationsByPmid('19855969', text_filter='ABCD1')
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByPmid(19855969, text_filter='ABCD1'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual(totalHits, 3)
        self.assertEqual(filteredHits, 1)
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0]['predication_id'], 3)

        results,totalHits,filteredHits = self.query_engine.retrievePredicationsByPmid('19855969', text_filter='Rheumatoid')
        print >>sys.stderr,  "\nSEMMEDDB.retrievePredicationsByPmid(19855969, text_filter='Rheumatoid'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual(totalHits, 3)
        self.assertEqual(filteredHits, 2)
        self.assertEqual(len(results), 2)
        self.assertEqual(results[0]['predication_id'], 1)
        self.assertEqual(results[1]['predication_id'], 3)

    def test_retrieve_sentences_by_predication(self):
        
        testStringA = u'We report a case of RA in a 60-year-old woman with homozygous haemoglobin C disease.'
        testStringB = u'These haemoglobin diseases are associated with characteristics abnormalities of the skeleton.'
        
        result,totalHits,filteredHits = self.query_engine.retrieveSentencesByPredication(3)
        print >>sys.stderr,  "\nSEMMEDDB.retrieveSentencesByPredication(3):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", result
        self.assertEqual(len(result), 2)
        self.assertEqual(unicode(result[0]['sentence']),testStringB)
        self.assertEqual(unicode(result[1]['sentence']),testStringA)

        result,totalHits,filteredHits = self.query_engine.retrieveSentencesByPredication(3,start=1,length=1)
        print >>sys.stderr,  "\nSEMMEDDB.retrieveSentencesByPredication(3,start=1,length=1):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", result
        self.assertEqual(len(result), 1)
        self.assertEqual(unicode(result[0]['sentence']),testStringA)

        result,totalHits,filteredHits = self.query_engine.retrieveSentencesByPredication(3,orderDir='desc')
        print >>sys.stderr,  "\nSEMMEDDB.retrieveSentencesByPredication(3,orderDir='desc'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", result
        self.assertEqual(len(result), 2)
        self.assertEqual(unicode(result[0]['sentence']),testStringA)
        self.assertEqual(unicode(result[1]['sentence']),testStringB)

        result,totalHits,filteredHits = self.query_engine.retrieveSentencesByPredication(3,text_filter='skeleton')
        print >>sys.stderr,  "\nSEMMEDDB.retrieveSentencesByPredication(3,text_filter='skeleton'):",
        print >>sys.stderr,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", result
        self.assertEqual(len(result), 1)
        self.assertEqual(unicode(result[0]['sentence']),testStringB)

    def test_retrieve_citations_by_concept(self):
        result = self.query_engine.retrieveCitationsByConcept('C0003873')
        print >>sys.stderr,  "\nSEMMEDDB.retrieveCitationsByConcept(C0003873):", result

    def test_count_evidence_for_predication(self):
        result = self.query_engine.countEvidenceForPredication(1)
        print >>sys.stderr,  "\nSEMMEDDB.countEvidenceForPredication():", result
        self.assertEqual(result,2)

    def test_count_distinct_concepts_for_Pmid_of_SemType(self):
        result = self.query_engine.countDistinctConceptsForPmidOfSemType('19855969','dsyn')
        print >>sys.stderr,  "\nSEMMEDDB.countDistinctConceptsForPmidOfSemType():", result
        self.assertEqual(len(result.keys()),4)

    def test_get_concept_details(self):
        concept = self.query_engine.getConceptDetails(1)
        print >>sys.stderr,  "\nSEMMEDDB.getConceptDetails(1):", concept
        self.assertIsNotNone(concept)
        self.assertEqual(concept["concept_id"],1L)
        self.assertEqual(unicode(concept["cui"]), u'C0003873')
        self.assertEqual(unicode(concept["type"]), u'META')
        self.assertEqual(unicode(concept["name"]), u'Rheumatoid Arthritis disease')
        self.assertEqual(unicode(concept["ghr"]), u'')
        self.assertEqual(unicode(concept["omim"]), u'180300:604302')

    def test_get_concept_semtype(self):
        semtype = self.query_engine.getConceptSemtype(SemMedDbTestData.PREDICATION1,SemMedDbTestData.CONCEPT1_ID)
        print >>sys.stderr,  "\nSEMMEDDB.getConceptSemtype(PREDICATION1,CONCEPT1_ID):", semtype
        self.assertIsNotNone(semtype)
        self.assertEqual(semtype,'dsyn')

        semtype = self.query_engine.getConceptSemtype(SemMedDbTestData.PREDICATION3,SemMedDbTestData.CONCEPT3_ID)
        print >>sys.stderr,  "\nSEMMEDDB.getConceptSemtype(PREDICATION3,CONCEPT3_ID):", semtype
        self.assertIsNotNone(semtype)
        self.assertEqual(semtype,'gngm')

        # test empty case...
        semtype = self.query_engine.getConceptSemtype(SemMedDbTestData.PREDICATION1,SemMedDbTestData.CONCEPT3_ID)
        print >>sys.stderr,  "\nSEMMEDDB.getConceptSemtype(PREDICATION1,CONCEPT3_ID) is empty '", semtype,"'"
        self.assertIsNotNone(semtype)
        self.assertEqual(semtype,u'')

    def test_get_concept_details_by_meta_cui(self):
        
        concept = self.query_engine.getConceptDetailsByCUI('C0003873')
        print >>sys.stderr,  "\nSEMMEDDB.getConceptDetailsByCUI(C0003873):", concept
        self.assertIsNotNone(concept)
        self.assertEqual(concept["concept_id"],1L)
        self.assertEqual(unicode(concept["cui"]), u'C0003873')
        self.assertEqual(unicode(concept["type"]), u'META')
        self.assertEqual(unicode(concept["name"]), u'Rheumatoid Arthritis disease')
        self.assertEqual(unicode(concept["ghr"]), u'')
        self.assertEqual(unicode(concept["omim"]), u'180300:604302')
        
    def test_get_concept_details_by_entrez_cui(self):
        
        concept = self.query_engine.getConceptDetailsByCUI('ABCD1')
        print >>sys.stderr,  "\nSEMMEDDB.getConceptDetailsByCUI(ABCD1):", concept
        self.assertIsNotNone(concept)
        self.assertEqual(concept["concept_id"],3L)
        self.assertEqual(unicode(concept["cui"]), u'ABCD1')
        self.assertEqual(unicode(concept["type"]), u'ENTREZ')
        self.assertEqual(unicode(concept["name"]), u'ABCD1')
        self.assertEqual(unicode(concept["ghr"]), u'')
        self.assertEqual(unicode(concept["omim"]), u'')
    
    """ # UCSD fastr service now deprecated?
       
    def test_get_fastr_top_hits(self):
        
        topHits = self.query_engine.getTopFastRHits(1347320)
        self.assertEqual(topHits[0], 19063)
    """
    
from data_auditor import auditor

class SemMedDbAuditorTest(TestCase):
    
    def test_data_audit_orphan_deletion(self):
        print >>sys.stderr,  "\n########## Starting test_data_audit_orphan_deletion"
        
        auditor()

        c1 = Concept.objects.using(DSID.SEMMEDDB).get(cui="C0003873")
        self.assertEqual( c1.is_orphan, False, "Not an orphan" )
            
        c2 = Concept.objects.using(DSID.SEMMEDDB).get(cui="C9876543")
        self.assertEqual( c2.is_orphan, True, "Is an orphan" )
            
        c3 = Concept.objects.using(DSID.SEMMEDDB).get(cui="C1855923")
        self.assertEqual( c3.is_orphan, True, "Is an orphan" )
            
        c4 = Concept.objects.using(DSID.SEMMEDDB).get(cui="120227")
        self.assertEqual( c4.is_orphan, True, "Is an orphan" )
            

