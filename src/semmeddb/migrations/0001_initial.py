# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Citations',
            fields=[
                ('pmid', models.CharField(max_length=20, serialize=False, primary_key=True, db_column='PMID')),
                ('issn', models.CharField(max_length=10, db_column='ISSN', blank=True)),
                ('dp', models.CharField(max_length=50, db_column='DP', blank=True)),
                ('edat', models.CharField(max_length=50, db_column='EDAT', blank=True)),
                ('pyear', models.IntegerField(null=True, db_column='PYEAR', blank=True)),
            ],
            options={
                'db_table': 'CITATIONS',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Concept',
            fields=[
                ('concept_id', models.AutoField(serialize=False, primary_key=True, db_column='CONCEPT_ID')),
                ('cui', models.CharField(max_length=20, db_column='CUI')),
                ('type', models.CharField(max_length=10, db_column='TYPE')),
                ('preferred_name', models.CharField(max_length=200, db_column='PREFERRED_NAME')),
                ('ghr', models.CharField(max_length=250, db_column='GHR', blank=True)),
                ('omim', models.CharField(max_length=1000, db_column='OMIM', blank=True)),
            ],
            options={
                'db_table': 'CONCEPT',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ConceptCitations',
            fields=[
                ('concept_citations_id', models.AutoField(serialize=False, primary_key=True, db_column='CONCEPT_CITATIONS_ID')),
                ('cui', models.CharField(unique='True', max_length=20, db_column='CUI')),
                ('pmid', models.TextField(db_column='PMID')),
            ],
            options={
                'db_table': 'CONCEPT_CITATIONS',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ConceptSemtype',
            fields=[
                ('concept_semtype_id', models.AutoField(serialize=False, primary_key=True, db_column='CONCEPT_SEMTYPE_ID')),
                ('semtype', models.CharField(max_length=4, db_column='SEMTYPE')),
                ('novel', models.CharField(max_length=1, db_column='NOVEL')),
                ('umls', models.CharField(max_length=1, db_column='UMLS', blank=True)),
                ('concept', models.ForeignKey(to='semmeddb.Concept', db_column='CONCEPT_ID')),
            ],
            options={
                'db_table': 'CONCEPT_SEMTYPE',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Predication',
            fields=[
                ('predication_id', models.AutoField(serialize=False, primary_key=True, db_column='PREDICATION_ID')),
                ('predicate', models.CharField(max_length=50, db_column='PREDICATE')),
                ('type', models.CharField(max_length=10, db_column='TYPE')),
            ],
            options={
                'db_table': 'PREDICATION',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PredicationArgument',
            fields=[
                ('predication_argument_id', models.AutoField(serialize=False, primary_key=True, db_column='PREDICATION_ARGUMENT_ID')),
                ('type', models.CharField(max_length=1, db_column='TYPE')),
                ('concept_semtype', models.ForeignKey(to='semmeddb.ConceptSemtype', db_column='CONCEPT_SEMTYPE_ID')),
                ('predication', models.ForeignKey(to='semmeddb.Predication', db_column='PREDICATION_ID')),
            ],
            options={
                'db_table': 'PREDICATION_ARGUMENT',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sentence',
            fields=[
                ('sentence_id', models.AutoField(serialize=False, primary_key=True, db_column='SENTENCE_ID')),
                ('pmid', models.CharField(max_length=20, db_column='PMID')),
                ('type', models.CharField(max_length=2, db_column='TYPE')),
                ('number', models.IntegerField(db_column='NUMBER')),
                ('sentence', models.CharField(max_length=999, db_column='SENTENCE')),
            ],
            options={
                'db_table': 'SENTENCE',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SentencePredication',
            fields=[
                ('sentence_predication_id', models.AutoField(serialize=False, primary_key=True, db_column='SENTENCE_PREDICATION_ID')),
                ('predication_number', models.IntegerField(db_column='PREDICATION_NUMBER')),
                ('subject_text', models.CharField(max_length=200, db_column='SUBJECT_TEXT', blank=True)),
                ('subject_dist', models.IntegerField(null=True, db_column='SUBJECT_DIST', blank=True)),
                ('subject_maxdist', models.IntegerField(null=True, db_column='SUBJECT_MAXDIST', blank=True)),
                ('subject_start_index', models.IntegerField(null=True, db_column='SUBJECT_START_INDEX', blank=True)),
                ('subject_end_index', models.IntegerField(null=True, db_column='SUBJECT_END_INDEX', blank=True)),
                ('subject_score', models.IntegerField(null=True, db_column='SUBJECT_SCORE', blank=True)),
                ('indicator_type', models.CharField(max_length=10, db_column='INDICATOR_TYPE', blank=True)),
                ('predicate_start_index', models.IntegerField(null=True, db_column='PREDICATE_START_INDEX', blank=True)),
                ('predicate_end_index', models.IntegerField(null=True, db_column='PREDICATE_END_INDEX', blank=True)),
                ('object_text', models.CharField(max_length=200, db_column='OBJECT_TEXT', blank=True)),
                ('object_dist', models.IntegerField(null=True, db_column='OBJECT_DIST', blank=True)),
                ('object_maxdist', models.IntegerField(null=True, db_column='OBJECT_MAXDIST', blank=True)),
                ('object_start_index', models.IntegerField(null=True, db_column='OBJECT_START_INDEX', blank=True)),
                ('object_end_index', models.IntegerField(null=True, db_column='OBJECT_END_INDEX', blank=True)),
                ('object_score', models.IntegerField(null=True, db_column='OBJECT_SCORE', blank=True)),
                ('curr_timestamp', models.DateTimeField(db_column='CURR_TIMESTAMP')),
                ('predication', models.ForeignKey(to='semmeddb.Predication', db_column='PREDICATION_ID')),
                ('sentence', models.ForeignKey(to='semmeddb.Sentence', db_column='SENTENCE_ID')),
            ],
            options={
                'db_table': 'SENTENCE_PREDICATION',
                'managed': True,
            },
            bases=(models.Model,),
        ),
    ]
