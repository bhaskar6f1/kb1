from django.conf.urls import patterns, url

# graph views -
urlpatterns = patterns('graph.views',
       url(r'^$', 'home', name='home'),
       url(r'^test$', 'test', name='test'),
       url(r'^loadTestData$', 'loadTestDataForGraph', name='loadTestDataForGraph'),
       url(r'^representation$', 'representation', name='representation'),
       url(r'^dragtest$', 'dragtest', name='dragtest'),
)

