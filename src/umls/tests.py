# -*- coding: utf-8 -*-
"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import sys

from django.test import TestCase

from gbk.settings import PROJECT_PATH
from os import path

from query.datasource import DSID
from query import QueryFactory

from models import ( 
    Definition,
    Semtype
)

from umls import UMLSTestData

class UMLSBaselineTest(TestCase):
    
    def test_concept_definition_retrieval(self):
        """ Retrieve an UMLS Concept Definition"""
        print >>sys.stderr,  "\nUMLS.Definition.objects.using(DSID.UMLS).get(cui="+UMLSTestData.UMLS_CONCEPT1_CUI+")"
        d = Definition.objects.using(DSID.UMLS).get(cui=UMLSTestData.UMLS_CONCEPT1_CUI)
        self.assertEqual( unicode(d.definition), UMLSTestData.UMLS_CONCEPT1_DEFINITION)

    def test_concept_semtype_retrieval(self):
        """ Retrieve an UMLS Concept Semantic Type"""
        print >>sys.stderr,  "\nUMLS.Semtype.objects.using(DSID.UMLS).get(cui="+UMLSTestData.UMLS_CONCEPT1_CUI+")"
        s = Semtype.objects.using(DSID.UMLS).filter(cui=UMLSTestData.UMLS_CONCEPT1_CUI)
        self.assertEqual( unicode(s[0].cui), UMLSTestData.UMLS_CONCEPT1_CUI)
        self.assertEqual( unicode(s[0].tui), UMLSTestData.UMLS_CONCEPT1_TUI1)
        self.assertEqual( unicode(s[0].semtype), UMLSTestData.UMLS_CONCEPT1_SEMTYPE1)
        self.assertEqual( unicode(s[1].cui), UMLSTestData.UMLS_CONCEPT1_CUI)
        self.assertEqual( unicode(s[1].tui), UMLSTestData.UMLS_CONCEPT1_TUI2)
        self.assertEqual( unicode(s[1].semtype), UMLSTestData.UMLS_CONCEPT1_SEMTYPE2)

class IntegratedUMLSTest(TestCase):
    
    def setUp(self):
        self.query_engine = QueryFactory(DSID.UMLS)
    
    def test_get_definition_by_existent_cui(self):
        
        #  UMLSTestData.UMLS_CONCEPT2_CUI ==  UMLSTestData.UMLS_CONCEPT3_CUI...
        definition = self.query_engine.getDefinitionsByCUI( UMLSTestData.UMLS_CONCEPT2_CUI )
        print >>sys.stderr,  "\nUMLS.getDefinitionsByCUI( "+UMLSTestData.UMLS_CONCEPT2_CUI+" ):",
        self.assertIsNotNone(definition)
        # ... but UMLSTestData.UMLS_CONCEPT3_SAB == NCI takes precedence 
        # over UMLSTestData.UMLS_CONCEPT2_SAB == MSH in definition search 
        self.assertEqual( definition[0], UMLSTestData.UMLS_CONCEPT3_SAB )
        self.assertEqual( definition[1], UMLSTestData.UMLS_CONCEPT3_DEFINITION )
    
    def test_get_definition_by_non_existent_cui(self):
        
        definition = self.query_engine.getDefinitionsByCUI(UMLSTestData.UMLS_CONCEPT5_CUI)
        print >>sys.stderr,  "\nUMLS.getDefinitionsByCUI("+UMLSTestData.UMLS_CONCEPT5_CUI+"):",
        self.assertIsNone(definition[0]) 
    
    def test_set_definition_of_non_existing_cui(self):
        
        definition = \
            self.query_engine.setDefinitionByCUI(
                UMLSTestData.UMLS_CONCEPT5_CUI,
                UMLSTestData.UMLS_CONCEPT5_AUI,
                UMLSTestData.UMLS_CONCEPT5_SAB,
                UMLSTestData.UMLS_CONCEPT5_DEFINITION
            )
        print >>sys.stderr,  "\nUMLS.setDefinitionByCUI(\n\t"+\
                     UMLSTestData.UMLS_CONCEPT5_CUI+\
                ", "+UMLSTestData.UMLS_CONCEPT5_AUI+\
                ", "+UMLSTestData.UMLS_CONCEPT5_SAB+\
                ",\n\t"+UMLSTestData.UMLS_CONCEPT5_DEFINITION+\
                "\n):"
        self.assertIsNone(definition) # empty old definition

    def test_set_definition_of_existing_cui(self):
        
        definition = \
            self.query_engine.setDefinitionByCUI(
                UMLSTestData.UMLS_CONCEPT4_CUI,
                UMLSTestData.UMLS_CONCEPT4_AUI,
                UMLSTestData.UMLS_CONCEPT4_SAB,
                UMLSTestData.UMLS_CONCEPT5_DEFINITION
            )
        print >>sys.stderr,  "\nUMLS.setDefinitionByCUI(\n\t"+\
                     UMLSTestData.UMLS_CONCEPT4_CUI+\
                ", "+UMLSTestData.UMLS_CONCEPT4_AUI+\
                ", "+UMLSTestData.UMLS_CONCEPT4_SAB+\
                ",\n\t"+UMLSTestData.UMLS_CONCEPT5_DEFINITION+\
                "\n):"
        self.assertIsNotNone(definition)
        self.assertEqual(  definition, 
                           ( UMLSTestData.UMLS_CONCEPT4_DEFINITION, 
                             UMLSTestData.UMLS_CONCEPT4_CUI,
                             UMLSTestData.UMLS_CONCEPT4_AUI,
                             UMLSTestData.UMLS_CONCEPT4_SAB, )
                        ) # old definition returned?

    def test_get_semtype_by_cui(self):
        # Should return the list of TUIs associated with the CUI
        semtypes = self.query_engine.getSemtypesByCUI( UMLSTestData.UMLS_CONCEPT1_CUI )
        print >>sys.stderr,  "\nUMLS.getSemtypesByCUI( "+UMLSTestData.UMLS_CONCEPT1_CUI+" ) found: ", semtypes
        self.assertIsNotNone(semtypes)
        self.assertEqual( semtypes[0], UMLSTestData.UMLS_CONCEPT1_TUI1 )
        self.assertEqual( semtypes[1], UMLSTestData.UMLS_CONCEPT1_TUI2 )
    
        
from umlsloader import loadFile

class UMLSLoadFileTest(TestCase):

    def setUp(self):
        self.query_engine = QueryFactory(DSID.UMLS)

    def test_load_umls(self):
        print >>sys.stderr,  "\numlsloader.loadFile(OUTLIER.RRF) - expect duplication?"
        UMLS_TEST_DATA_PATH = path.join(PROJECT_PATH,"static","data","OUTLIER.RRF")
        loadFile(UMLS_TEST_DATA_PATH)

