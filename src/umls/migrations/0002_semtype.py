# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('umls', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Semtype',
            fields=[
                ('semtype_id', models.AutoField(serialize=False, primary_key=True, db_column='SEMTYPE_ID')),
                ('cui', models.CharField(max_length=8, db_column='CUI')),
                ('tui', models.CharField(max_length=4, db_column='TUI')),
                ('stn', models.CharField(max_length=100, db_column='STN')),
                ('semtype', models.CharField(max_length=50, db_column='STY')),
                ('atui', models.CharField(max_length=11, db_column='ATUI')),
                ('cvf', models.IntegerField(max_length=10, null=True, db_column='CVF')),
            ],
            options={
                'db_table': 'MRSTY',
                'managed': True,
            },
            bases=(models.Model,),
        ),
    ]
