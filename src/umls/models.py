"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import unicode_literals

from django.db import models

"""
UMLS MetaThesaurus MRDEF table data model:

+----------+------------------+------+-----+---------+-------+
| Field    | Type             | Null | Key | Default | Extra |
+----------+------------------+------+-----+---------+-------+
| CUI      | char(8)          | NO   | MUL | NULL    |       |
| AUI      | varchar(9)       | NO   | MUL | NULL    |       |
| ATUI     | varchar(11)      | NO   | PRI | NULL    |       |
| SATUI    | varchar(50)      | YES  |     | NULL    |       |
| SAB      | varchar(40)      | NO   | MUL | NULL    |       |
| DEF      | text             | NO   |     | NULL    |       |
| SUPPRESS | char(1)          | NO   |     | NULL    |       |
| CVF      | int(10) unsigned | YES  |     | NULL    |       |
+----------+------------------+------+-----+---------+-------+

to which I add a Django-friendly AutoField 'DEFINITION_ID'
"""
class Definition(models.Model):
    definition_id = models.AutoField( db_column='DEFINITION_ID', primary_key=True )
    cui           = models.CharField( db_column='CUI',      max_length=8)
    aui           = models.CharField( db_column='AUI',      max_length=9)
    atui          = models.CharField( db_column='ATUI',     max_length=11)
    satui         = models.CharField( db_column='SATUI',    max_length=50, null=True )
    sab           = models.CharField( db_column='SAB',      max_length=40)
    definition    = models.TextField( db_column='DEF',      max_length=10000)
    suppress      = models.CharField( db_column='SUPPRESS', max_length=1)
    cvf           = models.IntegerField( db_column='CVF',   max_length=10, null=True)

    class Meta:
        managed = True
        db_table = 'MRDEF'
        unique_together = (('cui','aui','sab'),)

class Semtype(models.Model):
    semtype_id = models.AutoField( db_column='SEMTYPE_ID', primary_key=True )
    cui  = models.CharField( db_column='CUI',    max_length=8)
    tui  = models.CharField( db_column='TUI',    max_length=4)
    stn  = models.CharField( db_column='STN',    max_length=100)
    semtype = models.CharField( db_column='STY', max_length=50)
    atui = models.CharField( db_column='ATUI',   max_length=11)
    cvf  = models.IntegerField( db_column='CVF', max_length=10, null=True)

    class Meta:
        managed = True
        db_table = 'MRSTY'

