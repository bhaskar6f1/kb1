# -*- coding: utf-8 -*-
"""
This module encapsulates the UMLS definitions loader module
for the Knowledge.Bio application

The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
from gbk.settings import logger

import codecs

from threading import Thread

# from django.db import connections
# print >>logger, connections.queries

DEBUG = True   # diagnostics for code flow
TRACE = False   # dump DEBUG information if true
TEST  = False   # activate stub code

from query.datasource import DSID
from query import QueryFactory

'''
This module wraps functionality to load the UMLS meta-data about
SemMedDb concepts into the database using a background thread.
'''
def loadEntry(query_engine,line):
    
    if DEBUG:
        print >>logger,"#### UMLS Definitions ####"

    # entry split into 6 parts, 
    # the first one being the META CUI, 
    # the last one being the definition
    entry = line.split("|")
    
    cui        = entry[0].strip()
    aui        = entry[1].strip()
    ati        = entry[2].strip() # not really used but does index some duplication
    sab        = entry[4].strip()
    definition = entry[5].strip()
    
    if DEBUG:
        print >>logger,cui,",",aui,",",sab,":\t",unicode(definition,"utf-8"),"\n"

    # Find associated SemMedDb concept, and set it the definition 
    # (create the UMLS definition record if it doesn't exist)
    try:
        if DEBUG:
            print >>logger, "Before setDefinitionByCUI "
            
        old_definition = query_engine.setDefinitionByCUI( cui, aui, sab, definition)
        
        if DEBUG:
            print >>logger, "old definition: ", old_definition
        
        if not old_definition == None:
            print >>logger, 'UMLS loader.loadEntry() warning: duplicate definition spec "',\
                                unicode(line),'" overwriting previous definition: "',old_definition,'" (with ATI: "', ati,'")'
        return True
        
    except Exception as e:
        print >>logger,"UMLS loader.loadEntry() error for line:",unicode(line,"utf-8"),":",e
        return False

def loadFile(filename):
    
    query_engine = QueryFactory(DSID.UMLS)
    
    try:
        n = l = 0
        with codecs.open(filename, mode='r') as datafile:
             
            for line in datafile:
                
                line = line.strip()
                n += 1
                
                if loadEntry(query_engine,line) :
                    l += 1
                    
                if DEBUG:
                    if n%100 == 0:
                        print >>logger,"UMLS loader:",n,"lines loaded so far..."
                else:
                    if n%10 == 0:
                        print >>logger,".", # progress dot...
                    if n%100 == 0:
                        print >>logger # newline every twenty dots
                    if n%1000 == 0:
                        print >>logger,"UMLS loader:",n,"lines loaded so far..."
                
                if TEST and n>=100:
                    break
                    
        print >>logger,"UMLS loader: total of",l," records updated (from ",n," input data lines)"
                   
    except IOError as ioe:
        print >>logger,ioe

def umlsLoad(filename):
    Thread(
        target=loadFile, 
        name="UMLSLoader", 
        kwargs={"filename":filename}
    ).start()

