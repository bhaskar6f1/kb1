"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from django.conf.urls import patterns, url

from gbk.settings import ENABLE_ADMIN_FUNCTIONS

if ENABLE_ADMIN_FUNCTIONS:
    urlpatterns = patterns('query.views',
        url(r'^$',      'home', name='home'),
        url(r'^search/concept$', 'search_concept', name='search_concept'),
        url(r'^search/filter$', 'semantic_filter', name='semantic_filter'),
        url(r'^search$', 'search', name='search'),
        url(r'^test$',   'test',   name='test'),
        url(r'^data$',   'retrieveData', name='retrieveData'),
        #url(r'^concept/details/(?P<cui>\w+)$', 'conceptDetails', name='conceptDetails'),
        url(r'^(?P<context>\w+)/concept/details/(?P<relation_id>\d+)/(?P<concept_id>\d+)$', 'conceptDetails', name='conceptDetails'),
        url(r'^concept/(?P<relation_type>\w+)/(?P<concept_id>\w+)$', 'concept', name='concept'),
        url(r'^sentences/(?P<predication_id>\d+)$', 'sentences', name='sentences'),
        url(r'^citation/(?P<pmid>\d+)$', 'citation', name='citation'),
        url(r'^save$', 'saveMap', name='saveMap'),
        url(r'^load$', 'loadMap', name='loadMap'),
        url(r'^demo$', 'demo', name='demo'),
        url(r'^phsu_report$', 'phsu_report', name='phsu_report'),
        url(r'^report$', 'report', name='report'),
        url(r'^fastr/(?P<cui>\w+)$', 'fastr', name='fastr'),
        # url(r'^delete_tuples$', 'deleteTuples', name='deleteTuples'), # see loadabc2 functionality replacing this feature
    )
else:
    urlpatterns = patterns('query.views',
        url(r'^$',      'home', name='home'),
        url(r'^search/concept$', 'search_concept', name='search_concept'),
        url(r'^search/filter$', 'semantic_filter', name='semantic_filter'),
        url(r'^search$', 'search', name='search'),
        url(r'^data$',   'retrieveData', name='retrieveData'),
        url(r'^(?P<context>\w+)/concept/details/(?P<relation_id>\d+)/(?P<concept_id>\d+)$', 'conceptDetails', name='conceptDetails'),
        url(r'^concept/(?P<relation_type>\w+)/(?P<concept_id>\w+)$', 'concept', name='concept'),
        url(r'^sentences/(?P<predication_id>\d+)$', 'sentences', name='sentences'),
        url(r'^citation/(?P<pmid>\d+)$', 'citation', name='citation'),
        url(r'^save$', 'saveMap', name='saveMap'),
        url(r'^load$', 'loadMap', name='loadMap'),
        url(r'^demo$', 'demo', name='demo'),
        url(r'^fastr/(?P<cui>\w+)$', 'fastr', name='fastr'),
    )
    