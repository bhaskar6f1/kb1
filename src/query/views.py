# -*- coding: utf-8 -*-
"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import json

from django.http import HttpResponse
from django.template.response import TemplateResponse

from query.datasource import DSID
from query import QueryFactory

from gbk.settings import logger

DEBUG = False
TRACE = False
TEST  = False

def home(request):
    return concept(request,'discovery',0)

def test(request):
    return TemplateResponse(request, 'query/test.jade')

from threading import Thread

def deleteTupleDuplicates():
    print >>logger,"Entering thread for deleteTupleDuplicates() task"
    query_engine = QueryFactory(DSID.IMPLICITOME)
    query_engine.deleteTupleDuplicates()


# Deprecated this controller once loadabc2 view generalized 
# to filter out duplicates while loading B concepts    
def deleteTuples(request):
    
    Thread(
        target=deleteTupleDuplicates, 
        name="DeleteTupleDuplicates", 
    ).start()

    return TemplateResponse(request, 'query/delete_tuples.jade')

# See http://datatables.net/manual/server-side
def annotate( mode, data ):
    # stub implementation of a function to post-process 
    # data for more intelligent rendering of the results
    # in the client-side DataTable view
    # i.e. adding DT annotation
    # [
    #    {
    #         "DT_RowId": "row_<start>",
    #         "DT_RowData": {
    #             "pkey": <primary key>
    #         },
    #         "pmid" : 12345,
    #         "sentence" : "Hello World!",
    #    },
    #    ...
    # ]
    dtData = []
    for item in data:
        dtEntry = None 
        if mode == 'text' or mode == 'exptext':
            dtEntry = dtEntry = item
            dtEntry['DT_RowId'] = 'row_'+str(item['concept_id'])
            dtEntry['DT_RowData'] = { 'pkey' : item['concept_id'] }
        elif mode == 'imptext':
            dtEntry = dtEntry = item
            dtEntry['DT_RowId'] = 'row_'+str(item['concept_id'])
            dtEntry['DT_RowData'] = { 'pkey' : item['concept_id'] }
        elif mode == 'cui' or mode == 'explicit':
            dtEntry = item
            dtEntry['DT_RowId']   = 'row_'+str(item['predication_id'])
            dtEntry['DT_RowData'] = { 'pkey' : item['predication_id'] }
        elif mode == 'aliases' or mode == 'implicit':
            dtEntry = item
            dtEntry['DT_RowId']   = 'row_'+str(item['tuple_id'])
            dtEntry['DT_RowData'] = { 'pkey' : item['tuple_id'] }
        elif mode == 'cooccurrence' :
            dtEntry = item
            dtEntry['DT_RowId']   = 'row_'+str(item['tuple_id'])+'.'+str(item['b_concept']['concept_id'])
            dtEntry['DT_RowData'] = { 'pkey' : str(item['tuple_id'])+'.'+str(item['b_concept']['concept_id']) }
        elif mode == 'pmid':
            dtEntry = item
            dtEntry['DT_RowId']   = 'row_'+str(item['predication_id'])
            dtEntry['DT_RowData'] = { 'pkey' : item['predication_id'] }
        elif mode == 'sentences': 
            dtEntry = item
            dtEntry['DT_RowId']   = 'row_'+str(item['sentence_id'])
            dtEntry['DT_RowData'] = { 'pkey' : item['sentence_id'] }
        else:
            return [] # abort if mode is not recognized?
        dtData.append(dtEntry)         
    return dtData

def retrieveData(request):
    
    draw   = request.POST.get('draw')
    start  = request.POST.get('start',0)
    length = request.POST.get('length',1)
    query  = request.POST.get('query')
    type_filter  = request.POST.get('type_filter','')
    aliases      = request.POST.get('aliases', None )
    mode         = request.POST.get('mode')
    text_filter = request.POST.get('search[value]')
    orderColumn  = request.POST.get('order[0][column]')
    orderDir     = request.POST.get('order[0][dir]')
    
    if TRACE:
        print >>logger,\
            "query.views.retrieveData("+\
            " draw = "+str(draw)+\
            ", start = "+str(start)+\
            ", length = "+str(length),
        print >>logger,\
            u", query = '"+unicode(query, encoding='utf-8'),
        print >>logger,\
            ", type_filter = '"+str(type_filter)+\
            ", aliases = '"+str(aliases)+\
            "', mode = '"+str(mode)+"'",
        print >>logger,\
            u", text_filter = '"+unicode(text_filter, encoding='utf-8'),
        print >>logger,\
            "', orderColumn = "+str(orderColumn)+\
            ", orderDir = '"+str(orderDir)+\
            "' )"
    
    if query == None:
        return HttpResponse(json.dumps({'error' : "Data retrieval failed - Search query == None?"}),"application/json")
        
    try:
        draw   = int(draw)
        start  = int(start)
        length = int(length)
        if not orderColumn == None: # should be valid 'int' if not None
            orderColumn = int(orderColumn)
    except (ValueError, TypeError):
        print >>logger, "Invalid draw["+str(draw)+"], start["+str(start)+\
                            "], length["+str(length)+"] or orderColumn["+str(orderColumn)+\
                            "] parameter encountered in query.views.retrieveData()"
        return HttpResponse(json.dumps({'error' : "Data retrieval failed - Invalid search query input parameter?"}),"application/json") 
    
    if  mode == 'imptext' or \
        mode == 'aliases' or \
        mode == 'implicit' or \
        mode == 'cooccurrence':
        query_engine = QueryFactory(DSID.IMPLICITOME)
    elif mode == 'text' or \
         mode == 'exptext' or \
         mode == 'cui' or \
         mode == 'explicit' or \
         mode == 'pmid' or \
         mode == 'sentences' :
        query_engine = QueryFactory(DSID.SEMMEDDB)
    else:
        print >>logger, "Unknown search mode["+str(mode)+"] encountered in query.views.retrieveData()"
        return HttpResponse(json.dumps({'error' : "Data retrieval failed - unknown search mode?"}),"application/json") 
    
    if mode == 'text' or mode == 'exptext' or mode == 'imptext':
        data,totalHits,filteredHits = \
            query_engine.retrieveConceptsBySearchString(
                        query,
                        start=start,
                        length=length,
                        text_filter=text_filter,
                        orderDir=orderDir
            )
    elif mode == 'cui':
        data,totalHits,filteredHits = \
            query_engine.retrievePredicationsByCUI(
                        query,
                        type_filter=type_filter,
                        aliases=aliases,
                        start=start,
                        length=length,
                        text_filter=text_filter
            )
    elif mode == 'explicit':
        data,totalHits,filteredHits = \
            query_engine.retrievePredicationsByConceptId(
                        query,
                        aliases=aliases,
                        start=start,
                        length=length,
                        text_filter=text_filter
            )
    elif mode == 'aliases':
        data,totalHits,filteredHits = \
            query_engine.retrieveImplicitRelationsByAlias(
                        query,
                        start=start,
                        length=length,
                        text_filter=text_filter,
                        # order direction, for ordering by 
                        # scores (only), default to descending
                        orderDir=orderDir if orderColumn == 1 else 'desc'
            )
    elif mode == 'implicit':
        data,totalHits,filteredHits = \
            query_engine.retrieveImplicitRelationsByConceptId(
                        query,
                        start=start,
                        length=length,
                        text_filter=text_filter,
                        # order direction, for ordering by 
                        # scores (only), default to descending
                        orderDir=orderDir if orderColumn == 1 else 'desc'
            )
    elif mode == 'cooccurrence':
        data,totalHits,filteredHits = \
            query_engine.retrieveCooccurrenceOfConcepts(
                        # should be a comma delimited list of   
                        #        tuple_id, A_concept_id
                        # for the pair of Implicitome 
                        # A and C concepts being analyzed
                        query,  
                        start=start,
                        length=length,
                        text_filter=text_filter,
                        # order direction, for ordering by 
                        # scores (only), default to descending
                        orderDir=orderDir if orderColumn == 1 else 'desc'
            )
    elif mode == 'pmid':
        data,totalHits,filteredHits = \
            query_engine.retrievePredicationsByPmid(
                        query,
                        start=start,
                        length=length,
                        text_filter=text_filter,
                        orderColumn=orderColumn,
                        orderDir=orderDir
            )
    elif mode == 'sentences':
        data,totalHits,filteredHits = \
            query_engine.retrieveSentencesByPredication(
                        query,
                        start=start,
                        length=length,
                        text_filter=text_filter,
                        orderDir=orderDir
            )
    else:
        print >>logger, "Unknown search mode["+str(mode)+"] encountered in query.views.retrieveData()"
        return HttpResponse(json.dumps({'error' : "Data retrieval failed - unknown search mode?"}),"application/json") 
  
    data = annotate( mode, data )
    
    if TRACE:
        print >>logger, "retrieveData returns data:\n", data

    results = {
        'draw' : draw,
        'recordsTotal' : totalHits,
        'recordsFiltered' : filteredHits,
        'data' : data
    }
  
    results = json.dumps(results)
    
    return HttpResponse(results,"application/json")
   
def search(request):
    
    query = request.POST.get('query')
    mode  = request.POST.get('mode','text')
    
    if TRACE:
        print >>logger,u"\nSearching '"+unicode(query, encoding='utf-8')+"' as "+unicode(mode)
 
    # for concepts, I can (and should) already 
    # send back more details to display as a name 
    # (default to 'query' otherwise
    name = query
    concept  = None
    
    if mode == 'cui' or mode == 'explicit':
        query_engine = QueryFactory( DSID.SEMMEDDB )
        if not query==None:
            if mode == 'cui':
                concept = query_engine.getConceptDetailsByCUI(query)
            else:
                concept = query_engine.getConceptDetails(query)
            
    elif mode == 'aliases' or mode == 'implicit':
        query_engine = QueryFactory(DSID.IMPLICITOME)
        if not query==None:
            if mode == 'aliases':
                concept = query_engine.findConceptByAlias(query)
            else:
                concept = query_engine.getConceptDetails(query)
                
    if concept != None: name = concept['name']

    ctx = {
           'mode' : mode,
           'query' : query,
           'name' : name
           }

    return TemplateResponse(request, 'query/results.jade',ctx)

def search_concept(request):
    
    query = request.GET.get('query')
    
    ctx = {
       'query' : query,
    }

    return TemplateResponse(request, 'query/concept.jade',ctx)

# Semantic search filter controller
def semantic_filter(request):
    
    return TemplateResponse(request, 'query/filter.jade')


RECENT_VISITOR = "recent_visitor"


def displayMap(  
        request, 
        concept_name, 
        type_filter, 
        rootID, 
        map_data
    ):
    
    # The map will be displayed, centered on a specified root 'concept'
    ctx = {
        'concept_name' : concept_name,
        'type_filter'  : type_filter,

        'rootID'       : rootID,
        
        # mapdata should be a string containing a 
        # JSON formatted cytoscape.js map specification
        'rootNodeConcept'   : map_data,
          
        'UserExpandCount'   : 0.5,
        'UserWeightCutoff'  : 5,
        'UserDataSource'    :"",
    }

    return TemplateResponse( request, 'query/discovery.jade', ctx)

def concept(request,relation_type,concept_id):
    if TRACE:
        print >>logger,\
            "view.concept retrieving predications for "+\
            relation_type+" concept identifier: ",concept_id,

    if relation_type == 'discovery':
        
        # In "Discovery" mode, the concept_id is assumed to be a SemMedDb explicit concept_id
        explicit_query_engine = QueryFactory(DSID.SEMMEDDB)
        #implicit_query_engine = QueryFactory(DSID.IMPLICITOME)
        
        explicit_concept = explicit_query_engine.getConceptDetails(concept_id)
        
        if not explicit_concept == None:
            
            concept_name = explicit_concept['name']
                
            rootID = explicit_concept['type'] + u":"+ explicit_concept['cui']
            rootConceptNode = [
                   {
                        'group': 'nodes',
                        'data': { 
                            'id'  : rootID, 
                            'name': concept_name,
                         },
                        'position': { 
                                'x': 100, 
                                'y': 100 
                         },
                    },
            ]                
            
        else:
            concept_name = 'Unknown'
            rootID = 'META:Unknown'
            rootConceptNode = []
       
        mapdata = json.dumps( rootConceptNode )
        
        return displayMap(  
                request, 
                concept_name, 
                '',     # empty type_filter to start?
                rootID,
                mapdata
        )
    
    else:
        # for concepts, I can (and should) already 
        # send back more details to display as a name
        if relation_type == 'linked' or relation_type == 'implicit':
            query_engine = QueryFactory(DSID.IMPLICITOME)
            
        elif relation_type == 'explicit':
            query_engine = QueryFactory(DSID.SEMMEDDB)
            
        else:
            print >>logger, "Unknown search mode["+str(relation_type)+"] encountered in query.views.concept()"
            return HttpResponse(json.dumps({'error' : "Data retrieval failed - unknown search mode?"}),"application/json") 
        
        if TRACE: print >>logger," querying data source: ", query_engine.getDsid()
       
        concept = query_engine.getConceptDetails(concept_id)
        name = concept['name']
        
        aliases = ''
        mode = relation_type # default mode is the relation type, but...
        
        if relation_type == 'linked':
            # Data display mode is actually now explicit, but with 
            # remapping of implicitome concepts mapping onto the explicit world
            mode = 'explicit' 
            # Substitute a dictionary of multiple identifiers here...
            aliases = json.dumps( query_engine.getAliasesForConceptId(concept_id) )
    
        if TRACE:
            print >>logger, u"Subtitle of concept identifier '"+unicode(concept_id) + u"' is "+unicode(name, encoding='utf-8')
                
        ctx = {
               'mode' : mode,
               'query' : concept_id,
               'aliases' : aliases,
               'name' : name
               }
            
        return TemplateResponse(request, 'query/results.jade',ctx)

def conceptDetails(request,context,relation_id,concept_id):
    
    if DEBUG:
        print >>logger, "query.view.conceptDetails(): retrieving details for concept_id ",concept_id," associated with relation id",relation_id

    explicit_query_engine = QueryFactory(DSID.SEMMEDDB)
    implicit_query_engine = QueryFactory(DSID.IMPLICITOME)
    umls_query_engine     = QueryFactory(DSID.UMLS)
    
    if context == "explicit":
        
        concept = explicit_query_engine.getConceptDetails(concept_id)
    
        if not concept == None:
    
            concept['semtype'] = explicit_query_engine.getConceptSemtype(relation_id,concept_id)
            
            # the only time I retrieve concept definitions is when I'm displaying 'details'
            
            definition = umls_query_engine.getDefinitionsByCUI( concept['cui'] )
            if not definition[0] == None: 
                concept['definition'] = definition ;
            
            # don't need 'type', 'query' or 'concept_id' (primary key) on the front end
            del concept['type']  
            del concept['query'] 
            del concept['concept_id']
            
    else: # implicit or cooccurrence table concept details requested
        
        # try to access the Implicitome version of the concept?
        # in the Implicitome, a CUI is simply one kind of concept alias
        concept = implicit_query_engine.getConceptDetails(concept_id)
        
        if not concept == None:
            
            # the only time I retrieve concept definitions is when I'm displaying 'details' 
            definition = umls_query_engine.getDefinitionsByCUI( concept['cui'] )
            if not definition[0] == None: 
                concept['definition'] = definition ;
                
            # don't need 'type', 'query' or 'concept_id' (primary key) on the front end
            del concept['type']  
            del concept['query'] 
            del concept['concept_id'] 
            
    if not concept == None:
        
        details = [
            ( 'cui',  concept['cui'],),
            ( 'name', concept['name'],),
        ]
        
        if 'semtype' in concept:
            details.append( ('semtype', concept['semtype'],) )
            
        if 'tui' in concept:
            details.append( ('tui', concept['tui'],) )
            
        if 'definition' in concept:    
            details.append( ('definition', concept['definition'],) )
        
        if 'omim' in concept and not concept['omim'] == None:    
            details.append( ('omim', concept['omim'],) )
        
        if 'terms' in concept:    
            details.append( ('terms', concept['terms'],) )
        
        conceptJson = json.dumps( details )
        
    else:
        conceptJson = json.dumps({'Result' : "Details for "+ str(context) +"Concept Id "+ str(concept_id) +" not found?"})
        
    return HttpResponse( conceptJson,"application/json" )

def sentences(request,predication_id):
    if TRACE:
        print >>logger,"view.sentences retrieving sentences by predication_id: ",predication_id
        
    predication = request.GET.get('predicate')

    ctx = {
           'mode' : 'sentences',
           'query' : predication_id,
           'name' : predication
           }
        
    return TemplateResponse(request, 'query/results.jade',ctx)

def citation(request,pmid):
    if TRACE:
        print >>logger,"view.citation retrieving predications by pmid: ",pmid
        
    ctx = {
           'mode' : 'pmid',
           'query' : pmid,
           'name' : pmid
           }
        
    return TemplateResponse(request, 'query/results.jade',ctx)

from django.utils.encoding import smart_str

# Second iteration - explicit search for sentences, 
# then collecting the associated PMIDs
def getPMIDByPredicationId(predication_id):
    
    explicit_query_engine = QueryFactory(DSID.SEMMEDDB)
    
    results,totalHits,filteredHits = \
        explicit_query_engine.retrieveSentencesByPredication(predication_id)
    
    if TRACE:
        print >>logger,"getPMIDByPredicationId("+predication_id+"): ",
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results 
    
    pmids = [] 
    for sentence in results:
        pmid = sentence["pmid"]
        if not pmid in pmids:
            pmids.append(pmid)  
    
    return ','.join(pmids)

"""
This function generates a data file representing 
the contents of a Cytoscape.js Concept Map JSON string
"""
def generateConceptMapFile( concept_name, query, rootID, concept_map ):
    
    concept_map = json.loads( concept_map )
    
    # Second iteration: 
    # Create two separate sections using 'comment' headers,
    # using Windows EOL for good measure to keep the comment lines
    # separate from the content they tag...
    
    # ...First section: Global Concept Map values
    result  = "#\r\n" 
    result += "# Knowledge.Bio Version 1.1 Concept Map Dump\r\n" 
    result += "#\r\n"
    if not concept_name == None: 
        result += "# concept_name="+concept_name+"\r\n"
    else:
        result += "# concept_name=Unknown\r\n" 
    if not query == None: 
        result += "# query="+query+"\r\n"
    else:
        result += "# query=Unknown\r\n" 
    if not rootID == None:
        result += "# rootID="+rootID+"\r\n"
    else:
        result += "# rootID=Unknown\r\n" 
    
    # ...Second section: a tab delimited table of values
    result += "#\r\n" 
    result += "# Concept Relations Table\r\n" 
    result += "#\r\n" 
    result += "# Type\tSubject ID\tSubject Name\tRelation/Score\tObject ID\tObject Name\tPMIDs\r\n" 
    result += "#\r\n" 
    
    if "edges" in concept_map["elements"]: 
        edgeData = concept_map["elements"]["edges"]
        
        # 7++ column, tab-delimited table. 
        # Column headers: Relationship_type Subject_id, Subject_label, Predicate_label, Object_id, Object_label, PMIDs
        for edge in edgeData:
    
            details = edge["data"]
            
            # dissect the edge id for its actual identity
            category = details["id"][0]
            if category == 'E':
                relation  = 'Explicit'
            elif category == 'I':
                relation  = 'Implicit'
            elif category == 'C':
                relation  = 'Co-occurrence'
            else:
                relation  = 'Unknown'
            
            identifier = details["id"][2:]
            
            relation += "\t"+ details["source"]      # "ENTREZ:NGLY1", 
            relation += "\t"+ details["source_name"] # "NGLY1",
            
            # Not really very meaningful to end user, even if important for internal indexing of relations... 
            #relation += "\t"+ identifier             # "ER8311012", "IR123456",  etc
            
            relation += "\t"+ details["name"]        # "COEXISTS_WITH" or Implicit 'score'
            relation += "\t"+ details["target"]      # "META:C0017968", 
            relation += "\t"+ details["target_name"] # "Glycoproteins",
            
            # Pet the Explicit Evidence PMID's here
            if category == 'E':
                relation += "\t" + getPMIDByPredicationId( identifier )
        
            result += relation+"\r\n"
    
    # ...Third section: echo the JSON concept map received
    result += "# Concept Map Image\r\n" # use Windows EOL for good measure
    result += json.dumps( concept_map )
    
    return result

#
# 12-5-2015 First iteration on a server-side Knowledg.Bio concept map saving mechanism
#
def saveMap(request):
    # TODO:See https://djangosnippets.org/snippets/365/ for a more complete solution?
    
    concept_map = request.POST.get('concept_map')
    
    filename = request.POST.get('filename','map')
    filename = filename.replace(" ","_")

    concept_name = request.POST.get('concept_name')
    query        = request.POST.get('query')
    rootID       = request.POST.get('rootID')
        
    if concept_map == None or len(concept_map.strip()) == 0:
        return HttpResponse(json.dumps({'error' : "Empty graph?"}),"application/json; charset=utf-8'") 
    
    if TRACE:
        print >>logger, "\nSaving file '"+filename+"' of concept_Map:"+concept_map
        print >>logger, "\nConcept Name: '"+str(concept_name)+"' with query:"+str(query)+\
                        " with rootID: "+str(rootID)
        
    result = generateConceptMapFile( concept_name, query, rootID, concept_map )
    
    response = HttpResponse( result, content_type='application/force-download')
    response['Content-Disposition'] = 'attachment; filename=%s.kb' % smart_str(filename)
    response['Content-Length'] = len(result)
    
    return response

from tempfile import mkstemp
from os import fdopen,access,F_OK,R_OK

def loadMapFile( filepath ):
    
    if TRACE: 
        print >>logger, "loadMapFile("+filepath+")"
        
    # Attempt to open the file
    if filepath == None or len( filepath.strip() )== 0:
        return None,

    cm = {}
    cm["error"] = None
    cm["concept_name"] = 'Unknown'
    cm["type_filter"] = ''
    cm["rootID"] = u"META" + u":"+ u"Unknown"
    
    map_data_input = ""
    mapfound = False
    try:
        with open(filepath,'r') as mapfile:
            for line in mapfile:
                line = line.strip()
                if line.startswith("# Concept Map Image"):
                    mapfound = True
                    continue
                elif line.startswith("# concept_name="):
                    tagvalue = line.split("=")
                    cm["concept_name"] = tagvalue[1].strip()
                elif line.startswith("# rootID="):
                    tagvalue = line.split("=")
                    cm["rootID"] = tagvalue[1].strip()                    
                elif mapfound:
                    map_data_input += line
                else:
                    continue
                
    except IOError as io:
        cm["error"] = str(io)
        return cm
    
    map_data_input = json.loads(map_data_input)
    elements = nodes = edges = None
    
    mapdata = []
    if "elements" in map_data_input:
        elements = map_data_input["elements"] 
        if "nodes" in elements:
            nodes = elements["nodes"]
            for node in nodes:
                newnode = {}
                newnode["group"]    = "nodes"
                newnode["data"]     = node["data"]
                newnode["position"] = node["position"]
                newnode["classes"]  = node["classes"]
                mapdata.append(newnode)
                
        if "edges" in elements:
            edges = elements["edges"]
            for edge in edges:
                newedge = {}
                newedge["group"] = "edges"
                newedge["data"]  = edge["data"]
                mapdata.append(newedge)

    cm["map_data"] = json.dumps( mapdata )

    if TRACE: 
        print >>logger, "map data output:\n", cm["map_data"]
        
    return cm              

def loadAndDisplayMap( request, localfilepath ):
    # Loads and extracts a Cytoscape map from the Knowledge.Bio formatted file.
    # The map and its context are loaded into a complex data structure, here called 'concept_map'
    concept_map = loadMapFile( localfilepath )
    
    if TRACE:
        print >>logger, "loadAndDisplayMap(", \
                    "\nconcept_name:", concept_map["concept_name"], \
                    "\nrootID:",       concept_map["rootID"], \
                    "\n)"
    
    if not concept_map["error"] == None:
        return HttpResponse(json.dumps({'error' : str(concept_map.error)}),"application/json")
    else:
        return displayMap(  
                    request, 
                    concept_map["concept_name"], 
                    concept_map["type_filter"], 
                    concept_map["rootID"], 
                    concept_map["map_data"],
        )

from gbk.settings import PROJECT_PATH
from os import path  

def demo(request):
    return loadAndDisplayMap( request, path.join(PROJECT_PATH,"static","data","Sepiapterin_reductase-8.kb") )

def loadMap(request):
    '''
     Method to upload an Knowledge.Bio formatted dataset file
    '''
    if request.method == 'POST':
        
        gbk_file_data = request.POST.get('gbk_file_data')
        
        # localfilepath points to the 
        # local temporary file caching the data
        
        localfd, localfilepath = mkstemp()
        cachefile = fdopen(localfd,'w')
        
        if TRACE: print >>logger,"cachefile: ", localfilepath
    
        cachefile.write( gbk_file_data )
     
        cachefile.close()

        if localfilepath and \
                access(localfilepath, F_OK) and \
                access(localfilepath, R_OK):
            
            return loadAndDisplayMap( request, localfilepath )
        
        else:
            errmsg = "cachefile: ", localfilepath, " cannot be read?"
            print >>logger,errmsg
    else:
        errmsg = "Can't load a GET request?"

    return HttpResponse(json.dumps({'error' : errmsg }),"application/json")
        

from os.path import join

def report(request):
    if TRACE:
        print >>logger,"Entering view.report: "
    
    query_engine = QueryFactory(DSID.SEMMEDDB)
        
    # First iteration is a hard coded report based 
    # on data immediately required by Ben

    file1 = join(PROJECT_PATH,'testdata','tpp1_interactors.txt')
    
    tpp1_total  = 0
    tpp1_subjectTypeCounts = {}
    tpp1_objectTypeCounts  = {}
    with open(file1+'.out','w') as output1:
        with open(file1,'r') as input1:
            i=0
            print >>logger,'Processing: ',file1
            print >>output1,'# Processing: ',file1
            for pmid in input1:
                i += 1
                
                if TEST and i>5: break
                
                pmid = pmid.strip()
                
                totalHits,subjectTypeCounts,objectTypeCounts =\
                                        query_engine.countPredicationsForPmid(pmid)
                
                if TEST or (TRACE and (i % 1000 == 0)) : 
                    print >>logger, '['+str(i)+']: ', pmid," = ",totalHits," | STC: ",subjectTypeCounts," | OTC:",objectTypeCounts
                    
                print >>output1,pmid,"\t", totalHits,"\t", subjectTypeCounts,"\t", objectTypeCounts
                output1.flush()
                
                tpp1_total += totalHits
                
                for subType in subjectTypeCounts:
                    if not subType in tpp1_subjectTypeCounts:
                        tpp1_subjectTypeCounts[subType]  = subjectTypeCounts[subType]
                    else:
                        tpp1_subjectTypeCounts[subType] += subjectTypeCounts[subType]
                        
                for objectType in objectTypeCounts:
                    if not objectType in tpp1_objectTypeCounts:
                        tpp1_objectTypeCounts[objectType]  = objectTypeCounts[objectType]
                    else:
                        tpp1_objectTypeCounts[objectType] += objectTypeCounts[objectType]
                        
        print >>output1,'# Summary: ',"tpp1_total=", tpp1_total, \
                        ", tpp1_subjectTypeCounts=", tpp1_subjectTypeCounts,\
                        ", tpp1_objectTypeCounts=", tpp1_objectTypeCounts
                
    file2 = join(PROJECT_PATH,'testdata','adcy5_interactors.txt')
    
    adcy5_total = 0
    adcy5_subjectTypeCounts = {}
    adcy5_objectTypeCounts  = {}
    
    with open(file2+'.out','w') as output2:
        with open(file2,'r') as input2:
            i=0
            print >>logger,'Processing: ',file2
            print >>output2,'# Processing: ',file2
            for pmid in input2:
                i += 1
                
                if TEST and i>5: break
                
                pmid = pmid.strip()
                
                totalHits,subjectTypeCounts,objectTypeCounts =\
                                        query_engine.countPredicationsForPmid(pmid)
                
                if TEST or (TRACE and (i % 1000 == 0)) : 
                    print >>logger, '['+str(i)+']: ', pmid," = ",totalHits," | STC: ",subjectTypeCounts," | OTC: ",objectTypeCounts
                    
                print >>output2,pmid,"\t", totalHits,"\t", subjectTypeCounts,"\t", objectTypeCounts
                output2.flush()
        
                adcy5_total += totalHits
                
                for subType in subjectTypeCounts:
                    if not subType in adcy5_subjectTypeCounts:
                        adcy5_subjectTypeCounts[subType]  = subjectTypeCounts[subType]
                    else:
                        adcy5_subjectTypeCounts[subType] += subjectTypeCounts[subType]
                        
                for objectType in objectTypeCounts:
                    if not objectType in adcy5_objectTypeCounts:
                        adcy5_objectTypeCounts[objectType]  = objectTypeCounts[objectType]
                    else:
                        adcy5_objectTypeCounts[objectType] += objectTypeCounts[objectType]
                        
        print >>output2,'# Summary: ',"adcy5_total=", adcy5_total, \
                        ", adcy5_subjectTypeCounts=", adcy5_subjectTypeCounts,\
                            ", adcy5_objectTypeCounts=", adcy5_objectTypeCounts
                
    ctx = { 
           "tpp1_total" : tpp1_total,
           "tpp1_subjectTypeCounts" : tpp1_subjectTypeCounts,
           "tpp1_objectTypeCounts" : tpp1_objectTypeCounts,
           "adcy5_total":adcy5_total,
           "adcy5_subjectTypeCounts" : adcy5_subjectTypeCounts,
           "adcy5_objectTypeCounts" : adcy5_objectTypeCounts,
    }
        
    return TemplateResponse(request, 'query/report.jade',ctx)

def phsu_report(request):
    print >>logger,"Entering view.phsu_report: "
    
    query_engine = QueryFactory(DSID.SEMMEDDB)
        
    file1 = join(PROJECT_PATH,'testdata','tpp1_interactors.txt')
    
    tpp1_conceptCounts = {}

    with open(file1+'_phsu_concepts.out','w') as output1:
        with open(file1,'r') as input1:
            i=0
            print >>logger,'Processing: ',file1
            print >>output1,'# Processing: ',file1
            for pmid in input1:
                i += 1
                
                if TEST and i>10: break
                
                pmid = pmid.strip()
                
                phsu_concepts = query_engine.countDistinctConceptsForPmidOfSemType(pmid,'phsu')
                                        
                if TEST:
                    print >>logger, pmid, phsu_concepts

                print >>output1, pmid,"\t", phsu_concepts
                output1.flush()

                for cui in phsu_concepts.keys():
                    if not cui in tpp1_conceptCounts:
                        tpp1_conceptCounts[cui]  = phsu_concepts[cui]
                    else:
                        tpp1_conceptCounts[cui] += phsu_concepts[cui]
                
        total_tpp1_count = len(tpp1_conceptCounts.keys())
        print >>output1,"# Summary of "+str(total_tpp1_count)+" tpp1_conceptCounts:\n", tpp1_conceptCounts

    file2 = join(PROJECT_PATH,'testdata','adcy5_interactors.txt')
    
    adcy5_conceptCounts = {}
    
    with open(file2+'_phsu_concepts.out','w') as output2:
        with open(file2,'r') as input2:
            i=0
            print >>logger,'Processing: ',file2
            print >>output2,'# Processing: ',file2
            for pmid in input2:
                i += 1
                
                if TEST and i>10: break
                
                pmid = pmid.strip()
                
                phsu_concepts = query_engine.countDistinctConceptsForPmidOfSemType(pmid,'phsu')
                                        
                if TEST:
                    print >>logger, pmid, phsu_concepts
                
                print >>output2, pmid,"\t", phsu_concepts
                output2.flush()

                for cui in phsu_concepts.keys():
                    if not cui in adcy5_conceptCounts:
                        adcy5_conceptCounts[cui]  = phsu_concepts[cui]
                    else:
                        adcy5_conceptCounts[cui] += phsu_concepts[cui]
                        
        total_adcy5_count = len(adcy5_conceptCounts.keys())
        print >>output2,"# Summary of "+str(total_adcy5_count)+" total_adcy5_count:\n", adcy5_conceptCounts
                
    ctx = { 
           "tpp1_total" : total_tpp1_count,
           "adcy5_total": total_adcy5_count,
    }
        
    return TemplateResponse(request, 'query/report2.jade',ctx)



def fastr(self,cui):
    return HttpResponse(
# Hard coded NGLY1 fastR hits to test
"C0162740,C0036025,C0014239,C0007634,C0020114,C0000768,C0000967,C0005157,C0007585,C0008059,C0010834,C0014181,C0014834,C0017968,C0019247,C0023895,C0024660,C0025116,C0026882,C0032098,C0033684,C0036563,C0178453,C0242768,C0263643,C0333262,C0598964,C1140618,C1167518"
)
    

