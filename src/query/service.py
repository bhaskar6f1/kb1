"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
"""
This module encapsulates a query service 
# business module for the Knowledge.Bio application
"""
import sys

DEBUG = False # diagnostics for code flow
TRACE = False # dump results from query if true

from query.datasource import DSID

class Query:
    
    def __init__(self,dataSourceId):
        """
        Constructor initialized with a data source identifier (dsid)
        """
        self.dsid = dataSourceId
    
    def getDsid(self):
        #print >>sys.stderr,"getDsid() returns: ",self.dsid
        return self.dsid
        
    def isDsid(self,theDsid):
        return self.dsid == theDsid
        
    #
    # THIS VERSION IS NOT CURRENTLY USED - SemMedDb only is queried by string (called directly)
    #
    """
     User Story #2 - 02-02-2015
     
     Search concepts in Knowledg.Bio data sources by partial matching of 
     searchString against corresponding Concept name fields
    
     a. Input: 
         searchString: text search string (e.g. seizures)
         start: offset to first entry in database
         length: number of entries to return
         exact: boolean flag designating if the match must be exact ("True") or partial ("False"), default: False
         insensitive: boolean flag designating if the match must be case_insensitive ("True") or case sensitive ("False"), default: True
         text_filter: string filter to apply to the resulting concept name strings
         orderDir: sort ordering imposed on concept name field: 'asc' == ascending, 'desc' == descending (default : 'asc')
         
     b. Output:  returns a tuple with
        [0] result list of matching Concept objects (with CUIs, that can be used in User Story #1)
        [1] number of total Hits
        [2] number of filtered Hits
     
    Revision: 
        03-03-2015, added 'start', 'length', 'text_filter' and 'orderDir' arguments to support server-side paging
        20-03-2015, refactored to 'delegation' design pattern to redirect search to SemMedDbQuery or ImplicitomeQuery, based on datasource

    """

    def retrieveConceptsBySearchString( self, searchString, **kwargs ):
        
        if self.isDsid(DSID.SEMMEDDB):
            pass
            #results, totalHits, filteredHits = \
            #    SemMedDbQuery.retrieveConceptsBySearchString( self, searchString, **kwargs )
        elif self.isDsid(DSID.IMPLICITOME):
            pass
            #results, totalHits, filteredHits = \
            #    ImplicitomeQuery.retrieveConceptsBySearchString( self, searchString, **kwargs )
        else:
            print >>sys.stderr, "WARNING: query.service.retrieveConceptsBySearchString: unknown Dsid: ",self.getDsid()
            return [],0,0
            
        return None # results, totalHits, filteredHits
    
