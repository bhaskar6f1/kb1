# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('implicitome', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='concept',
            name='is_orphan',
            field=models.BooleanField(default=0, db_column='is_orphan'),
            preserve_default=True,
        ),
    ]
