# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Concept',
            fields=[
                ('concept_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255, blank=True)),
                ('definition', models.CharField(max_length=10000, blank=True)),
            ],
            options={
                'db_table': 'concept',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Dblink',
            fields=[
                ('dblink_id', models.AutoField(serialize=False, primary_key=True)),
                ('concept_id', models.IntegerField()),
                ('db_id', models.CharField(max_length=4)),
                ('identifier', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'dblink',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Relation',
            fields=[
                ('relation_id', models.AutoField(serialize=False, primary_key=True)),
                ('concept1_id', models.IntegerField()),
                ('concept2_id', models.IntegerField()),
                ('relationtype_id', models.IntegerField()),
            ],
            options={
                'db_table': 'relation',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Term',
            fields=[
                ('term_id', models.AutoField(serialize=False, primary_key=True)),
                ('concept_id', models.IntegerField()),
                ('subterm_id', models.IntegerField()),
                ('text', models.CharField(max_length=255, blank=True)),
                ('casesensitive', models.IntegerField(null=True, blank=True)),
                ('ordersensitive', models.IntegerField(null=True, blank=True)),
                ('normalised', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'term',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tuples',
            fields=[
                ('tuple_id', models.AutoField(serialize=False, primary_key=True)),
                ('sub_id', models.IntegerField()),
                ('sub_name', models.CharField(max_length=100, blank=True)),
                ('obj_id', models.IntegerField()),
                ('obj_name', models.CharField(max_length=100, blank=True)),
                ('score', models.DecimalField(max_digits=20, decimal_places=16)),
                ('percentile', models.DecimalField(null=True, max_digits=12, decimal_places=10, blank=True)),
                ('linked_concepts', models.TextField(max_length=500, blank=True)),
            ],
            options={
                'db_table': 'tuples',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='term',
            unique_together=set([('concept_id', 'subterm_id')]),
        ),
        migrations.AlterUniqueTogether(
            name='relation',
            unique_together=set([('concept1_id', 'concept2_id')]),
        ),
        migrations.AlterUniqueTogether(
            name='dblink',
            unique_together=set([('concept_id', 'db_id', 'identifier')]),
        ),
    ]
