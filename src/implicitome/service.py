# -*- coding: utf-8 -*-
"""
This module encapsulates the Implicitome query service module
for the Knowledge.Bio application

The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
from datetime import datetime

from operator import itemgetter #, attrgetter , methodcaller

from django.db.transaction import commit
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from gbk.settings import logger, PROJECT_PATH

DEBUG  = False  # diagnostics for code flow
DEBUG2 = True  # diagnostics for code flow
TRACE  = False  # dump TRACE information if true
TEST   = False  # activate stub code

from query.datasource import DSID
from query.service import Query

from umls.service import UMLSQuery

from implicitome.models import (
    Concept,
    Dblink,
    Term,
    Tuples,
)

from django.db import connections

class ImplicitomeQuery(Query):
    
    # Size of Implicitome tuples table used in 
    # Knowledge.Bio as of July 29, 2015
    # Probably should NOT be hard coded, but...
    TOTAL_TUPLE_COUNT = 202705006 ;
    
    TOP_TENTH_PERCENTILE = 90.0 
    TOP_ONE_PERCENTILE   = 99.0
    
    TOP_TENTH_PERCENTILE_QUERY_LIMIT = '20270500' 
    TOP_ONE_PERCENTILE_QUERY_LIMIT   = '2027050' 
    
    DEFAULT_PERCENTILE = TOP_ONE_PERCENTILE
    DEFAULT_PERCENTILE_QUERY_LIMIT = TOP_ONE_PERCENTILE_QUERY_LIMIT
    
    def __init__(self):
        Query.__init__(self, DSID.IMPLICITOME)
        
    def directQuery(self,query,*param):
        
        cursor = connections[self.getDsid()].cursor()
    
        if not param==None and  len(param)>0:
            if TRACE: print >>logger, "Executing directQuery('"+query+"' with parameters '"+str(param)+")"
            cursor.execute(query, param)
        else:
            if TRACE: print >>logger, "Executing directQuery('"+query+"')"
            cursor.execute(query)
    
        return cursor.fetchall()

    def queryLimit(self,start,length):
        limit = " LIMIT "
        if length<1:
            if start <= 0:
                if TRACE: 
                    print >>logger, ", all entries in the table"
                return ''  # no LIMIT filter at all
            if TRACE: print >>logger, ", from start["+str(start)+\
                                        "] entry to the default query limit of "+\
                                        ImplicitomeQuery.DEFAULT_PERCENTILE_QUERY_LIMIT
            limit += ImplicitomeQuery.DEFAULT_PERCENTILE_QUERY_LIMIT+" OFFSET "+str(start)
        else:
            if start <= 0:
                if TRACE:
                    print >>logger, ", get first length["+str(length)+"] entries"
                limit += str(length) # simple LIMIT
            else:
                if TRACE: 
                    print >>logger, ", get first "+str(length)+" entries from entry "+str(start)
                limit += str(length)+" OFFSET "+str(start)
        return limit
    
    # Common implementation of querying to retrieve Predications 
    # given a query filter anchored on the Predication model.   
    # LIMITATION: subject, predict, object concept string field ordering 
    # is NOT implemented on the database query end but rather, in memory,
    # since the Django model 'orderBy' query method doesn't seem to 
    # work on attributes nested deeply in a model query (requiring
    # joins across multiple table to get associated with the output).
    #

    """
     User Story #1b - 23-03-2015
     
     Show the relations associated with particular Implicitome concept
     
     a. Input: 
        alias = canonical or alias name of a concept to search for (Note: this is a dblink entry, *not* the Implicitome concept identifier) 
        start: offset to first entry in database
        length: number of entries to return
        text_filter: string filter to apply to the resulting concept and predicate name strings
     
     b. Output:  returns a (Pythonic) tuple with
        [0] table of implicitly related concepts,
            as an array of dictionary indexed entries with general format:
            
            tuple_id, score, subject_concept_details, related_concept_details
            
            where 'details' are concept_id, type, cui, name ++
        
        [1] number of total Hits
        [2] number of filtered Hits

    Revisions: 
        Adapted from SemMedDb equivalent user story 1a) code
        
    18 May 2015 - Allowing the 'Implicit' result data to diverge from the 'Explicit' subject-predicate-object
                  since 'Implicit' data doesn't really have a predicate (only a score) and no inherent directionality
                  (concepts discovered are just implicitly 'related' to the query concept)
        
    LIMITATION: 
    * subject, predict, object concept string field ordering is NOT implemented (problematic?)
    * Due to the extensive size of the Implicitome database, to achieve an acceptable query performance,
      this implementation artificially limits access to concepts by using a preliminary subquery to 
      top percentile of entries in the Tuples table, which is assumed to be ordered by score.

    """
    def retrieveImplicitRelationsByAlias(
            self, 
            alias,  
            percentile=DEFAULT_PERCENTILE, 
            start=0, 
            length=-1,
            text_filter = "",
            orderDir="desc",
     ):
        
        # First, identify the concept id from the given concept identifier
        concept = self.findConceptByAlias(alias)
        
        # No matching concept in Implicitome
        if concept == None: 
            return [],0,0
        
        if TRACE:
            print >>logger,"Implicitome Concept for alias '"+str(alias)+"' is ", concept
        
        return self.retrieveImplicitRelationsByConceptId(
            concept["concept_id"], percentile, 
            start, length, text_filter, orderDir,
        )

    """
    See retrieveImplicitRelationsByAlias above for details. 
    The 'query' here is the Implicitome concept_id.
    """
    def retrieveImplicitRelationsByConceptId(
            self, 
            concept_id,  
            percentile=DEFAULT_PERCENTILE, 
            start=0, 
            length=-1,
            text_filter = "",
            orderDir="desc",
     ):
        if DEBUG:
            print >>logger,\
                "\nimplicitome.service.retrieveImplicitRelationsByConceptId("+\
                " concept_id = '"+str(concept_id)+\
                "', percentile = "+str(percentile)+\
                ", start = "+str(start)+\
                ", length = "+str(length)+\
                ", text_filter = '"+str(text_filter)+\
                "', orderDir = '"+str(orderDir)+\
                "' )"
        
        if text_filter == None:
            text_filter = ''
        elif not text_filter.isalnum():
            # Silently ignore non-alphanumeric searches for now (for safety sake...)
            # TODO: perhaps allow some additional legitimate symbols like hyphen?
            text_filter = ''
        
        if not orderDir == None and str(orderDir) == 'asc':
            orderDir = 'ASC'
        else:
            orderDir = 'DESC'
            
        try:
            percentile = float(percentile)
            start  = int(start)
            length = int(length) 
        except (ValueError, TypeError):
            print >>logger, "ERROR - retrieveImplicitRelationsByConceptId(): "+\
                                "Invalid percentile["+str(percentile)+\
                                "], start["+str(start)+\
                                "] or length["+str(length)+"]?"
            return [],0,0
        
        percentile = 0.0 if percentile <= 0.0 else percentile
        percentile = 99.9999 if percentile >= 100.0 else percentile
        start  = 0 if start <= 0 else start
        
        selectCountStar = "SELECT COUNT(*) "
        
        selectStar      = "SELECT * "
        
        subqueryFilter  = "FROM (SELECT * FROM tuples LIMIT "+ImplicitomeQuery.DEFAULT_PERCENTILE_QUERY_LIMIT+") AS T "+\
                            "WHERE T.percentile >="+str(percentile) 
        
        queryConceptFilter = " AND (T.sub_id="+str(concept_id)+" OR T.obj_id="+str(concept_id)+")"
        
        querySearchFilter  = " AND ( UPPER(T.sub_name) LIKE UPPER('%"+str(text_filter)+"%')"+\
                                " OR UPPER(T.obj_name) LIKE UPPER('%"+str(text_filter)+"%') )"

        orderByScore = " ORDER BY T.score "+orderDir
        
        # Artificially constrain hits to upper percentile of tuples table 
        # (as defined by DEFAULT_PERCENTILE_QUERY_LIMIT) then count all the tuples 
        # in the given percentile which contain the concept_id either 
        # as subject id or object id
      
        totalHitCountQuery = selectCountStar + subqueryFilter + queryConceptFilter
        if DEBUG: print >>logger, "Total hits count query:",totalHitCountQuery
        totalHitCount = self.directQuery(totalHitCountQuery)[0][0]
                       
        if DEBUG:
            print >>logger,"Implicitome total hits:", totalHitCount
        
        if(len(text_filter)>0):
            filteredHitCountQuery = selectCountStar + subqueryFilter + queryConceptFilter + querySearchFilter
            if DEBUG: print >>logger, "Filtered hits count query:",filteredHitCountQuery
            filteredHitCount = self.directQuery(filteredHitCountQuery)[0][0]
            if DEBUG: print >>logger, "found "+str(filteredHitCount)+" hits filtered by '"+str(text_filter)+"'..."
        else:
            filteredHitCount = totalHitCount
            if DEBUG: print >>logger,"Hits unfiltered..."
            
        if(len(text_filter)>0):
            pagedFilteredHitQuery = selectStar + subqueryFilter +\
                                queryConceptFilter + querySearchFilter +\
                                orderByScore + self.queryLimit(start,length) 
            if DEBUG: print >>logger, "Paged filtered hits query:",pagedFilteredHitQuery
            tuples = self.directQuery(pagedFilteredHitQuery)
        else:
            pagedFullHitQuery = selectStar + subqueryFilter +\
                                queryConceptFilter +\
                                orderByScore + self.queryLimit(start,length)
            if DEBUG: print >>logger, "Page all hits query:",pagedFullHitQuery
            tuples = self.directQuery(pagedFullHitQuery)

        # then construct the full list of the complete tuple details
        results = {}
        # Fields indices are:
        # [0]: tuple_id
        # [1]: sub_id
        # [2]: sub_name
        # [3]: obj_id
        # [4]: obj_name
        # [5]: score
        # [6]: percentile
        for relation in tuples:
            if DEBUG: print >>logger, "\nIMPLICITOME Relation entry: ",relation,"\n"
            rid = relation[0] # tuple_id
            if rid not in results:
                
                results[rid] = {} # first time seen... create...
                results[rid]['tuple_id'] = rid  
                results[rid]['score']    = relation[5] # Decimal score, Unicode stringified!
                
                if not ( relation[1] == concept_id or relation[3] == concept_id):
                    # Oops! Error: unrelated tuple encountered?
                    print >>logger, "ERROR - retrieveImplicitRelationsByConceptId(): "+\
                                        "Apparently unrelated tuple '"+rid+"' encountered? Skipping..."
                    continue
                
                elif relation[1] == concept_id:
                    subject_concept_id   = relation[1]
                    subject_concept_name = self.getConceptName(relation[1])
                    related_concept_id   = relation[3]
                    related_concept_name = self.getConceptName(relation[3])
                    
                elif relation[3] == concept_id:
                    subject_concept_id   = relation[3]
                    subject_concept_name = self.getConceptName(relation[3])
                    related_concept_id   = relation[1]
                    related_concept_name = self.getConceptName(relation[1])
                    
                results[rid]['subject'] = {}

                # Need additional query concept details here(?)
                subject_concept = self.getAnnotatedConcept(subject_concept_id)
                
                results[rid]['subject']['concept_id'] = subject_concept_id 
                results[rid]['subject']['type']       = subject_concept['type']
                results[rid]['subject']['cui']        = subject_concept['cui']
                results[rid]['subject']['name']       = subject_concept_name 
                results[rid]['subject']['semtype']    = subject_concept['semtype']
                results[rid]['subject']['tui']        = subject_concept['tui']
                
                # Need additional related concept details here(?)
                related_concept = self.getAnnotatedConcept(related_concept_id)
                
                if related_concept == None: continue
                
                results[rid]['related'] = {}
                results[rid]['related']['concept_id'] = related_concept_id 
                results[rid]['related']['type']       = related_concept['type']
                results[rid]['related']['cui']        = related_concept['cui']
                results[rid]['related']['name']       = related_concept_name
                results[rid]['related']['semtype']    = related_concept['semtype']
                results[rid]['related']['tui']       = related_concept['tui']
        
        # aggregate the results as a list
        results = list(results.values())
        
        # ...then, sort the list on the Decimal score...
        reverse = True if orderDir == "DESC" else False
        results.sort(key=itemgetter('score'),reverse=reverse)
        
        # ... finally, convert the Decimal formatted score to a unicode string, for JSON serialization
        for entry in results: entry['score'] = unicode(entry['score']) 
        
        if DEBUG:
            print >>logger,  "\nquery.retrieveImplicitRelationsByConceptId() returns:",
            print >>logger,  "\nTotal Hits:", totalHitCount,", Filtered:", filteredHitCount,", returning ",len(results)," results:\n", results

        return results, totalHitCount, filteredHitCount

    """
     Implicitome ABC Model Co-occurrence User Story - 25-06-2015
     
     Show the relations associated with particular Implicitome concept
     
     a. Input: 
        tuple_id,a_concept_id
        
        where tuple_id     = the Implicitome tuple_id corresponding to Implicit relationship between an A concept and a C concept
        and   a_concept_id = the Implicitome concept_id of the A concept
     
     b. Output:  returns a (Pythonic) tuple with
        [0] table of implicitly related concepts,
            as an array of dictionary indexed entries with general format:
            
            tuple_id, score, subject_concept_details, related_concept_details
            
            where 'details' are concept_id, type, cui, name ++
        
        [1] number of total Hits   # will be 5 or less in the initial implementation
        [2] number of filtered Hits

    """ 
    def retrieveCooccurrenceOfConcepts(
            self, 
            query,  
            start=0, 
            length=-1,
            text_filter = "",
            orderDir="desc",
     ):
        if DEBUG:
            print >>logger,\
                "\nimplicitome.service.retrieveCooccurrenceOfConcepts("+\
                " query = '"+str(query)+\
                ", start = "+str(start)+\
                ", length = "+str(length)+\
                ", text_filter = '"+str(text_filter)+\
                "', orderDir = '"+str(orderDir)+\
                "' )"
        
        if text_filter == None:
            text_filter = ''
        elif not text_filter.isalnum():
            # Silently ignore non-alphanumeric searches for now (for safety sake...)
            # TODO: perhaps allow some additional legitimate symbols like hyphen?
            text_filter = ''
        
        if not orderDir == None and str(orderDir) == 'asc':
            orderDir = 'ASC'
        else:
            orderDir = 'DESC'
            
        try:
            start  = int(start)
            length = int(length) 
        except (ValueError, TypeError):
            print >>logger, "ERROR - retrieveCooccurrenceOfConcepts(): "+\
                                "start["+str(start)+\
                                "] or length["+str(length)+"]?"
            return [],0,0
        
        tuple_id, subject_concept_id = query.split(",")
        
        # sanity check for empty query
        if int(tuple_id) == 0:
            return [],0,0
        
        implicit_relation = None
        try:
            implicit_relation = Tuples.objects.using(self.getDsid()).get(tuple_id = tuple_id)

        except MultipleObjectsReturned:
            print >>logger, "ERROR - retrieveCooccurrenceOfConcepts(): Implicitome relation '"+tuple_id+"' not unique?"
            return [],0,0

        except  ObjectDoesNotExist:
            print >>logger, "ERROR - retrieveCooccurrenceOfConcepts(): Implicitome relation '"+tuple_id+"' unknown?"
            return [],0,0
        
        linked_concepts = implicit_relation.linked_concepts
        
        if linked_concepts == None:
            return [],0,0
        
        linked_concept_data = linked_concepts.split("|")

        if int(implicit_relation.sub_id) == int(subject_concept_id):
            a_concept = self.getAnnotatedConcept( implicit_relation.sub_id )
            c_concept = self.getAnnotatedConcept( implicit_relation.obj_id )
        else:
            a_concept = self.getAnnotatedConcept( implicit_relation.obj_id )
            c_concept = self.getAnnotatedConcept( implicit_relation.sub_id )
              
        results = {}
        
        for i in range(0,len(linked_concept_data),3):
            
            b_concept_id   = linked_concept_data[i]
            b_concept      = self.getAnnotatedConcept( b_concept_id )
            b_contribution = linked_concept_data[i+2]
            
            if not b_concept_id in results:
                results[b_concept_id] = {}
                results[b_concept_id]["a_concept"] = {}
                results[b_concept_id]["b_concept"] = {}
                results[b_concept_id]["c_concept"] = {}
                
            if DEBUG:
                print >>logger, "\tb_concept(",b_concept_id,"):[", u','.join( [ unicode(key)+":"+unicode(b_concept[key]) for key in b_concept ] ),"]"
            
            results[b_concept_id]["tuple_id"]                  = tuple_id
            results[b_concept_id]["a_concept"]["type"]         = a_concept["type"]
            results[b_concept_id]["a_concept"]["cui"]          = a_concept["cui"]
            results[b_concept_id]["a_concept"]["name"]         = a_concept["name"]
            results[b_concept_id]["a_concept"]["semtype"]      = a_concept["semtype"]
            results[b_concept_id]["a_concept"]["tui"]          = a_concept["tui"]
            results[b_concept_id]["b_concept"]["concept_id"]   = int(b_concept_id)
            results[b_concept_id]["b_concept"]["type"]         = b_concept["type"]
            results[b_concept_id]["b_concept"]["cui"]          = b_concept["cui"]
            results[b_concept_id]["b_concept"]["name"]         = b_concept["name"]
            results[b_concept_id]["b_concept"]["semtype"]      = b_concept["semtype"]
            results[b_concept_id]["b_concept"]["tui"]          = b_concept["tui"]
            results[b_concept_id]["b_concept"]["contribution"] = b_contribution
            results[b_concept_id]["c_concept"]["type"]         = c_concept["type"]
            results[b_concept_id]["c_concept"]["cui"]          = c_concept["cui"]
            results[b_concept_id]["c_concept"]["name"]         = c_concept["name"]
            results[b_concept_id]["c_concept"]["semtype"]      = c_concept["semtype"]
            results[b_concept_id]["c_concept"]["tui"]          = c_concept["tui"]

        results = results.values()

        results = sorted( results , key=lambda result: float(result["b_concept"]["contribution"]), reverse=True)

        return results,len(results),len(results)

    """
     User Story, subsidiary use case #2b - 20-03-2015
     
     Search concept names in Implicitome by partial matching of 
     a searchString text query against Implicitome Concept table name field
     
     See retrieveConceptsBySearchString below for inputs and outputs
     
     July 8, 2015 Revision: extend string search to the 'term' table as well
                            then consolidate the list of concept id's for retrieval

    """
    def retrieveConceptsBySearchString( 
            self, 
            searchString, 
            start = 0, 
            length = -1, 
            exact = False, 
            insensitive = True,
            text_filter = '',
            orderDir = 'asc'
     ):
        if TRACE:
            print >>logger,\
                "\nimplicitome.service.retrieveImplicitomeConceptsBySearchString(",
            print >>logger,\
                u" searchString = '" + unicode(searchString) + u"'",
            print >>logger,\
                ", start = "  + str(start)+\
                ", length = " + str(length)+\
                ", exact = "  + str(exact)+\
                ", insensitive = "  + str(insensitive)+\
                ", text_filter = '" + str(text_filter)+\
                "', orderDir = '"   + str(orderDir)+\
                "' )"

        # sanity check for empty searchString
        searchString = unicode(searchString).strip()
        if not len(searchString) > 0:
            return [],0,0

        # sanity check on the integral values of start and length
        try:
            start  = int(start)
            start  = 0 if start < 0 else start
            length = int(length) 
        except (ValueError, TypeError):
            print >>logger, "ERROR - retrieveConceptsBySearchString(): Invalid start["+str(start)+"] or length["+str(length)+"]?"
            return [],0,0
        
        # First, compose the Implicitome.Concept table query
        # Setting up appropriate baseline query filter
        if TRACE: print >>logger, "Searching Concept table...",
        conceptQueryFilter = {}
        if(exact):
            if TRACE: print >>logger, "exact match...",
            if(insensitive):
                if TRACE: print >>logger, "case insensitive...",
                conceptQueryFilter['name__iexact'] = searchString ;
            else:
                if TRACE: print >>logger, "case sensitive...",
                conceptQueryFilter['name__exact'] = searchString ;
        else:
            if TRACE: print >>logger, "inexact match...",
            if(insensitive):
                if TRACE: print >>logger, "case insensitive...",
                conceptQueryFilter['name__icontains'] = searchString ;
            else:
                if TRACE: print >>logger, "case sensitive...",
                conceptQueryFilter['name__contains'] = searchString ;
        
        # only show concepts that are not orphans - 
        # i.e. that have predications (this flag was set by semmeddb.data_audit)
        conceptQueryFilter['is_orphan'] = False ;
        
        # count total hits on the original string query
        allConcepts = Concept.objects.using(self.getDsid()).\
                        filter(**conceptQueryFilter).\
                            values("concept_id")
        
        # Second, compose the Implicitome.Term table query
        # Setting up appropriate baseline query filter
        if TRACE: print >>logger, "Searching Term table...",
        termQueryFilter = {}
        if(exact):
            if TRACE: print >>logger, "exact match...",
            if(insensitive):
                if TRACE: print >>logger, "case insensitive...",
                termQueryFilter['text__iexact'] = searchString ;
            else:
                if TRACE: print >>logger, "case sensitive...",
                termQueryFilter['text__exact'] = searchString ;
        else:
            if TRACE: print >>logger, "inexact match...",
            if(insensitive):
                if TRACE: print >>logger, "case insensitive...",
                termQueryFilter['text__icontains'] = searchString ;
            else:
                if TRACE: print >>logger, "case sensitive...",
                termQueryFilter['text__contains'] = searchString ;
        
        # count total hits on the original string query
        allTerms = Term.objects.using(self.getDsid()).\
                        filter(**termQueryFilter).\
                            values("concept_id","text")
        
        # get any DT search filter applied by the user...
        if(text_filter != None):
            text_filter = text_filter.strip()

        # ... and computer number filtered hits
        if len(text_filter)>0:
            filtered_search = True
        else:
            filtered_search = False
        
        if filtered_search:
            
            if TRACE: print >>logger, "filtered with search[value="+str(text_filter)+"]...",
            
            # ...filter the Concepts
            filteredConcepts = Concept.objects.using(self.getDsid()).\
                                    filter(**conceptQueryFilter).\
                                        filter(name__icontains=text_filter).\
                                            values("concept_id")
            # ... and filter the Terms                 
            filteredTerms = Term.objects.using(self.getDsid()).\
                                    filter(**termQueryFilter).\
                                        filter(text__icontains=text_filter).\
                                            values("concept_id")
        
            # Gather all the concept_id's not filtered out
            filteredConceptHits = {}
            for c in filteredConcepts:
                filteredConceptHits[ c['concept_id'] ] = 1
            
            for c in filteredTerms:
                filteredConceptHits[ c['concept_id'] ] = 1
                
            filteredConceptHitIds = filteredConceptHits.keys()
        else:
            filteredConceptHitIds = []
            
        # All the concept_id's (without filtering)
        allConceptHits = {}
        for c in allConcepts:
            allConceptHits[ c['concept_id'] ] = []

        for c in allTerms:
            if not c['concept_id'] in allConceptHits:
                allConceptHits[ c['concept_id'] ] = []
            allConceptHits[ c['concept_id'] ].append( c['text'] )
                
        allConceptHitIds = allConceptHits.keys()
        totalHitCount = len( allConceptHitIds )
        if filtered_search:                   
            filteredHitCount = len( filteredConceptHitIds )
        else:
            filteredHitCount = totalHitCount

        # Finally, get the list of actual results of the filtered search
        # from the page start offset and of length requested
        
        targetQueryFilter = {}
        if filtered_search:
            targetQueryFilter["concept_id__in"] = filteredConceptHitIds
        else:
            targetQueryFilter["concept_id__in"] = allConceptHitIds
        
        # only show concepts that are not orphans - i.e. that have tuples 
        # (this flag was set by the implicitome.data_audit)
        targetQueryFilter['is_orphan'] = False ;
                
        # set ordering of results
        if(orderDir == 'desc'):
            orderBy = ['-name']
        else:
            orderBy = ['name']
        
        if( length<1 ):
            
            if TRACE: 
                print >>logger, "from start["+str(start)+"] to end of table"
            
            concepts = Concept.objects.using(self.getDsid()).\
                                    filter(**targetQueryFilter).order_by(*orderBy).\
                                        values("concept_id","name")[start:]
        else:
            # filter slice computed from length added to start
            end = start + int(length) 
            if TRACE: 
                print >>logger, "from start["+str(start)+"] to end["+str(end-1)+"]"
                
            concepts = Concept.objects.using(self.getDsid()).\
                                    filter(**targetQueryFilter).order_by(*orderBy).\
                                        values("concept_id","name")[start:end]
        results = [ 
            { 
                "concept_id"  : c['concept_id'],
                "name"        : c['name'],
            } for c in concepts]
        
        results = [ self.annotateConcept(c) for c in results ]
       
        if TRACE:
            print >>logger, "implicitome.service.retrieveConceptsBySearchString returns:\n", results
        
        # return the results plus the total number of 
        # available matching records in the database 
        return results, totalHitCount, filteredHitCount


    """
    Method wraps the details of a Concept, assumed 
    retrieved in the result list passed as the argument
    
    """
    def retrieveConceptDetails(self,result):
        
        count = result.count()
        
        if DEBUG: print >>logger,"Entering Implicitome.retrieveConceptDetails() with ",count," results found..."
        
        if(count>0):
            concept = {}
            concept['concept_id'] = result[0].concept_id
            concept['cui']        = u''
            concept['name']       = result[0].name
            concept['type']       = u''
            concept['semtype']    = u''
            concept['tui']        = u''
            concept['definition'] = result[0].definition
            concept['ghr']        = u''
            concept['omim']       = u''
            
            if DEBUG: 
                print >>logger,"Concept Id:", concept['concept_id'], ", Name: ", concept['name']
            
            # Check dblink table for UMLS CUI, OMIM id's and xrefs
            concept = self.annotateConcept(concept)
            
            if TRACE: 
                print >>logger,"...found concept:", concept
                
            return concept
            
        # ... if I fall through to here, nothing found?
        return None

    """
    Service to retrieve basic details of a Concept retrieved by concept_id
    """
    def getConceptName(self,concept_id):
        
        if TRACE: 
            print >>logger,"Entering SemMedDb.getConceptName(",concept_id,")"
            
        result = Concept.objects.using( self.getDsid() ).filter( concept_id__exact=concept_id ).values('name')
        if len(result)>0:
            return result[0]['name']
        else:
            return None # not found, fail silently?


    """
    Service to retrieve basic details of a Concept retrieved by concept_id
    """
    def getConceptDetails(self,concept_id):
        
        if TRACE: 
            print >>logger,"Entering SemMedDb.getConceptDetails(",concept_id,")"
            
        result = Concept.objects.using( self.getDsid() ).filter( concept_id__exact=concept_id )
        
        concept = self.retrieveConceptDetails(result)
        concept["query"] = concept_id
        return concept
              
    """
    Method to annotate a concept with provided dblink-retrieved identifier aliases, 
    setting the canonical type and cui of the Concept along the way
    """
    def annotateConceptAliases(self,concept,identifiers):
        idMap = {}
        for entry in identifiers:
            if not entry.db_id in idMap:
                # can be one to many aliases, i.e. various orthologous EG entries
                idMap[entry.db_id] = []  
            idMap[entry.db_id].append(entry.identifier)
        
        # default is unknown
        concept['type'] = u''
        concept['cui']  = u''
        concept['semtype'] = ''
        concept['omim'] = u'' 
        concept['tui'] = ''
        
        if 'EG' in idMap:
            concept['type'] = u'ENTREZ'
            # I will use the concept name 
            # (assumed already defined in 
            # 'concept' variable by caller) 
            # a.k.a. gene symbol here instead?
            concept['cui']  = concept['name']
            concept['semtype'] = 'gngm' 
            
        elif 'OM' in idMap:
            concept['type'] = u'OMIM'
            concept['cui']  = idMap['OM'][0]
            concept['semtype'] = 'dsyn'
        
        elif 'CHEB' in idMap:
            concept['type'] = u'CHEB'
            concept['cui']  = idMap['CHEB'][0]
            concept['semtype'] = 'phsu' 
        
        # override all other concepts but EG with UMLS cui, if present
        if not 'EG' in idMap and 'UMLS' in idMap:
            
            concept['type'] = u'META'
            concept['cui']  = idMap['UMLS'][0]
            
        if not concept['semtype']:
            # last ditch effort to get semtype(?)
            
            umls_db = UMLSQuery()
            # defer translation of tui into semtype in the front end UI
            concept['tui'] = umls_db.getSemtypesByCUI(concept['cui'])
            
            if DEBUG:
                print >>logger,"Found semtype tui ",concept['tui']," for cui: ", concept['cui']
            
        if 'OM' in idMap:
            concept['omim'] = idMap['OM'][0]
            
        return concept

    """
    Service to find a concept associated with one of two cases:
    1) Assumed to be an Entrez id, an exact match to the Concept.name field.
    2) Otherwise, a non-Entrez id with an exact match to a Dblink.identifier field
    """
    def findConceptByAlias(self,alias):
        
        if TRACE: print >>logger,"Entering Implicitome.findConceptByAlias(",alias,")"
         
        # sanity check on the values of alias, which is also a
        # basic protection against SQL injections on these fields
        if alias == None or not alias.isalnum(): return None
        
        # First, check for an exact match to the Concept.name 
        # field as a possible candidate (Entrez) gene symbol
        result = Concept.objects.using( self.getDsid()).filter( name__exact = alias )
        concept = self.retrieveConceptDetails(result)
        
        # If concept found by name field in main Concept table...
        if not concept == None:
            
            concept["query"] = alias
            return concept
        
        else: 
            # ....If the concept alias is not found in main Concept table...
            # ... then check the dblink table for alias names
            identifiers = Dblink.objects.using( self.getDsid()).filter(db_id__in=('UMLS','EG','OM','CHEB'), identifier__iexact = alias )
            
            if identifiers.count()>0:
                # Found an exact match to a Dblink (could be a cui?)
                concept = self.getConceptDetails( identifiers[0].concept_id )
                if not concept == None:
                    concept = self.annotateConceptAliases( concept, identifiers )
                    concept["query"] = alias
                    return concept
            
            # else: # my third source of identification could be the term.text field?
            else:
                terms = Term.objects.using( self.getDsid()).filter( text__exact = alias )
                if terms.count()>0:
                    concept = self.getConceptDetails( terms[0].concept_id )
                    if not concept == None:
                        concept = self.annotateConceptAliases( concept, identifiers )
                        concept["query"] = alias
                        return concept

        # if I fall through here, I failed to find the concept?
        return None

    """
    Annotate a specified concept with all of its 
    aliases recorded in the Implicitome dblink table
    """
    def annotateConcept(self,concept):
        
        if concept == None:
            print >>logger,"WARNING: annotateConcept() - concept argument is empty?"
            return None
        
        # get all relevant identifiers
        identifiers = \
            Dblink.objects.using(self.getDsid()).filter(
                db_id__in=('UMLS', 'OM', 'EG','CHEB'),
                concept_id__exact=concept['concept_id']
            )
        if identifiers.count()>0:
            concept = self.annotateConceptAliases( concept, identifiers )
        else:
            concept['type']    = u''
            concept['cui']     = u''
            concept['semtype'] = u'' 
            concept['tui']     = u'' 
        
        # get all related terms
        terms = Term.objects.using(self.getDsid()).\
                    filter( concept_id__exact=concept['concept_id'] ).values("text")
        concept['terms']  = [ term["text"] for term in terms ]
           
        return concept

    def getAnnotatedConcept( self, concept_id ):
        concept = self.getConceptDetails( concept_id )
        return concept
    
    def getAliasesForConceptId(self,concept_id):
        """
        This method retrieves the list of all the UMLS, OM, CHEB and EG biological identifiers related to a concept.
        """
        if concept_id == None:
            print >>logger,"WARNING: getAliasesForConceptId() - concept_id argument is null?"
            return {}
        identifiers = \
            Dblink.objects.using(self.getDsid()).filter(
                db_id__in=('UMLS', 'OM', 'EG', 'CHEB'),
                concept_id__exact=concept_id
            )
        if identifiers.count()>0:
            hits = {}
            for entry in identifiers:
                if not entry.db_id in hits:
                    # can be one to many aliases, i.e. various orthologous EG entries
                    hits[entry.db_id] = []  
                hits[entry.db_id].append(entry.identifier)
            return hits
        else:
            if TRACE:
                print >>logger,"WARNING: getAliasesForConceptId() - no identifier mappings exist for concept id '"+str(concept_id)+"'?"
            return {}

    # Deprecated this code after abcload2.py was generalized 
    # to filter out duplicates while loading the B concepts
    def deleteTupleDuplicates( self ):
        
        # Perhaps better to have this thread log its progress 
        # within another log file, other than stderr...
        with open(PROJECT_PATH+"/logs/deleteTupleDuplicates.log", mode='w+') as dtlog:
        
            print >>dtlog,"Entering Implicitome.deleteTupleDuplicates(), querying for ids at ",datetime.utcnow()
            dtlog.flush()
            
            cursor = connections[ self.getDsid() ].cursor()
            
            # First, compile the large (>1.3 million) list of duplicate tuple_id's
            
            # Test SQL Query
            # target_tuple_entry_query = "SELECT * FROM tuples LIMIT 100000"
            
            # Count first...
            target_tuple_count_query = \
                "SELECT COUNT(*) FROM tuples AS A, tuples AS B "+\
                    "WHERE B.tuple_id = A.tuple_id+1 AND  A.sub_id = B.sub_id AND  A.obj_id = B.obj_id LIMIT 10000"
                    
            # ... get the entries later...
            target_tuple_entry_batch_query = \
                "SELECT B.tuple_id FROM tuples AS A, tuples AS B "+\
                    "WHERE B.tuple_id = A.tuple_id+1 AND  A.sub_id = B.sub_id AND  A.obj_id = B.obj_id LIMIT 10000"
            
            cursor.execute(target_tuple_count_query)
            noEntries = cursor.fetchall()[0][0]
            
            print >>dtlog,"... iterating over ",noEntries," number of duplicate Tuples entries..."
            dtlog.flush()
           
            while(noEntries>0):
                
                print >>dtlog,"... found  ",noEntries, " more tuples to process at ",datetime.utcnow()
    
                cursor.execute(target_tuple_entry_batch_query)
                tupleEntries4Deletion = cursor.fetchall()
                
                print >>dtlog,"... getting the ids at ",datetime.utcnow()
                
                # print >>dtlog,"...gathering ids at ",datetime.utcnow()
                
                target_tuple_ids = [ tuple_entry[0] for tuple_entry in tupleEntries4Deletion]
                
                print >>dtlog,"... completed iteration to capture ids at ",datetime.utcnow()
                dtlog.flush()
                
                # Then, iterate through the list of duplicate tuple_id's, deleting them batch-by-batch
                num_ids = len(target_tuple_ids)
                batchsize = 100
                for start in range(0,num_ids,batchsize):
                    
                    end = start+batchsize
                    end = num_ids if num_ids < end else end
                    subset = target_tuple_ids[start:end]
                    
                    subset = ','.join([ str(tuple_id) for tuple_id in subset])
                    print >>dtlog,"Deleting tuple_id subset: ", subset
                    dtlog.flush()
                              
                    target_tuple_deletion_query = \
                            "DELETE FROM tuples WHERE tuple_id IN ("+subset+")"
                        
                    # target_tuple_deletion_query = "SELECT tuple_id FROM tuples WHERE tuple_id IN ("+subset+")"
        
                    cursor.execute(target_tuple_deletion_query)
                    
                    # tuplesDeleted = cursor.fetchall()
                    # for entry in tuplesDeleted:
                    #    print >>dtlog, entry[0]
                    #    
                    # Test run without deleting
                    # if start > 1000:
                    #    break
                    commit( self.getDsid() )
                    
                    # count next batch of id's
                    cursor.execute(target_tuple_count_query)
                    noEntries = cursor.fetchall()[0][0]
        
        print >>dtlog,"Completed Implicitome.deleteTupleDuplicates() at ",datetime.utcnow()
        dtlog.flush()
        
def percentileLimit(percentile):
    percentile = int(percentile)
    #print >>logger,"percentileLimit( percentile: ", percentile,")",
    if percentile > int(ImplicitomeQuery.DEFAULT_PERCENTILE) : 
        limit = ImplicitomeQuery.DEFAULT_PERCENTILE_QUERY_LIMIT
    elif percentile <= 0 : 
        limit = ImplicitomeQuery.TOTAL_TUPLE_COUNT
    else:
        limit = int( ImplicitomeQuery.TOTAL_TUPLE_COUNT * (100-percentile)/100)
    #print >>logger," returns limit = ", limit
    return limit
