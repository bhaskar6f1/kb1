"""
This module encapsulates our attempts at fixing 
B concept assignments in the Knowledge.Bio application.

abcLoad  - classical read and write records
abcLoad2 - in=memory merge of records from the ABC file 
           and original Tuples table into a new Tuples table

The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""

from threading import Thread
import codecs

# from django.db import connections
# print >>logger, connections.queries

from django.db.transaction import commit
from django.db import connections
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from gbk.settings import logger, PROJECT_PATH, DEBUG_MODULE

# default debug flags all off?
DEBUG = False  # highlights data-related issues  
TRACE = False  # exhaustive tracing of code execution
TEST2 = False  # to test abcLoad2 with a smaller number of data points
TEST  = False  # to test with a smaller number of data points
LOCALTEST = False  # constraint for local testing of SQL

# selectively reset debug flags?
if "abcloader" in DEBUG_MODULE:
    debug_level = DEBUG_MODULE['abcloader']
    if debug_level == 'DEBUG':
        DEBUG = True
    elif debug_level == 'TEST2':
        DEBUG = True
        TEST2 = True
    elif debug_level == 'TRACE':
        DEBUG = True
        TRACE = True

from query.datasource import DSID
from query import QueryFactory

from service import ImplicitomeQuery

from implicitome.models import (
    Concept,
    Tuples,
    TuplesMerged
)

def getRelations( query_engine, a_concept_id, c_concept_id, limit=ImplicitomeQuery.DEFAULT_PERCENTILE_QUERY_LIMIT ):
    
    selectStar      = "SELECT * "
        
    subqueryFilter  = "FROM (SELECT * FROM tuples LIMIT "+limit+") AS T "
        
    queryConceptFilter = " WHERE (T.sub_id="+str(a_concept_id)+" AND T.obj_id="+str(c_concept_id)+")"+\
                            " OR (T.sub_id="+str(c_concept_id)+" AND T.obj_id="+str(a_concept_id)+")"
    
    totalQuery = selectStar + subqueryFilter + queryConceptFilter
    
    if DEBUG:
        print >>logger,"getRelations() query: ", totalQuery
    
    implicit_relations = query_engine.directQuery( totalQuery )
    
    return implicit_relations

# Database accessor for linked concepts

# if 'validating' false, then set the new linked_concepts value to discovered tuples
# else just check that the retrieved linked_concepts are the same as argument provided
#
# July 28th update: limit search of concepts to the top one percentile
#                   of records, that is, the DEFAULT_PERCENTILE_QUERY_LIMIT
#
def findOrAddLinkedConcepts( 
            query_engine, 
            a_concept_id, c_concept_id, 
            linked_concepts, 
            limit=ImplicitomeQuery.DEFAULT_PERCENTILE_QUERY_LIMIT  
    ):
    
    if TRACE:
        print >>logger, "\n### ABCL: findOrAddLinkedConcepts(",\
                            "\n### ABCL: \ta_concept_id: ", a_concept_id,\
                            "\n### ABCL: \tc_concept_id: ", c_concept_id,\
                            "\n### ABCL: \tlinked_concepts: ",  linked_concepts,\
                        ")\n### ABCL: ... Querying Tuples..."
        
    implicit_relations = getRelations( query_engine, a_concept_id, c_concept_id, limit )
        
    if TRACE: print >>logger, "### ABCL: ... returned from Tuples query"
        
    found_or_added = False
    modified = False
    
    # relation fields indices are:
    # [0]: tuple_id
    # [1]: sub_id
    # [2]: sub_name
    # [3]: obj_id
    # [4]: obj_name
    # [5]: score
    # [6]: percentile    
    # [7]: linked_concepts
    updatedRelations = []   
    for relation in implicit_relations:
        
        if DEBUG: 
            print >>logger, "\n### ABCL: Found Tuple: ", relation[0], "|", relation[1], "|", relation[3]
            
        if not relation[7] == None:
            existing_linked_concepts = unicode(relation[7]).strip()
            if TRACE:
                print >>logger, "### ABCL: Relation.linked_concepts = ", existing_linked_concepts            
            if len( existing_linked_concepts ) > 0:
                if  existing_linked_concepts == linked_concepts:
                    if TRACE:
                        print >>logger, "...existing linked concept found! "
                    found_or_added = True
              
                else:  # not the same string???
                    print >>logger, "### ABCL: findOrAddLinkedConcepts error for tuple: ",\
                                    relation[0],", relation.linked_concepts: ",\
                                    relation[7]," != given: ",linked_concepts
                
        # null or empty string... needs to be set linked_concepts to given tuple
        if TRACE:
            print >>logger, "...attempting to add new linked concepts '",\
                            linked_concepts,"' to tuple: ", relation[0] 
        updatedRelations.append( ( relation[0], linked_concepts ) )
        
    for item in updatedRelations:
        relation_saved = None
        try:
            relation_saved = Tuples.objects.using( query_engine.getDsid() ).get(tuple_id = item[0])
            relation_saved.linked_concepts = item[1]
            relation_saved.save()
            if DEBUG:
                print >>logger, "### ABCL: tuple ID '", item[0], "' now has linked B concepts: ", item[1]
            found_or_added = True
            modified = True
            
        except MultipleObjectsReturned:
            print >>logger, "### ABCL error: retrieveCooccurrenceOfConcepts(): Implicitome relation '",item[0],"' not unique?"

        except  ObjectDoesNotExist:
            print >>logger, "### ABCL error: retrieveCooccurrenceOfConcepts(): Implicitome relation '",item[0],"' unknown?"

    if modified: # save all modifications
        commit( query_engine.getDsid() )
        
    if not found_or_added and DEBUG:
        print >>logger, "### ABCL  warning: Tuple for a_concept_id: ", a_concept_id,\
                        " and c_concept_id: ", c_concept_id, " not found?"
    
    return found_or_added

# For local testing mode only: create the concepts and tuples as needed...
def testHostData( query_engine, a_concept_id, a_name, c_concept_id, c_name ):
    Concept.objects.using(query_engine.getDsid()).update_or_create(
        concept_id = a_concept_id,
        name = a_name,
        definition = ''
    )
    Concept.objects.using(query_engine.getDsid()).update_or_create(
        concept_id = c_concept_id,
        name = c_name,
        definition = ''
    )
    Tuples.objects.using(query_engine.getDsid()).create(
        sub_id     = a_concept_id,
        sub_name   = a_name,
        obj_id     = c_concept_id,
        obj_name   = c_name,
        score      = 0,   # not sure what to set these to!
        percentile = 0, # not sure what to set these to!
        linked_concepts = u''
    )
    commit( query_engine.getDsid() )
    
'''
This module wraps functionality to load the ABC model linked 
implicitome concepts into the database using a background thread.
'''
def loadEntry( query_engine, line, limit=ImplicitomeQuery.DEFAULT_PERCENTILE_QUERY_LIMIT, echo=DEBUG, test=TEST ):
                
    entry = line.split("|")
    
    if len(entry)<6:
        if DEBUG:
            print >>logger,"\n### ABCL warning: B concepts missing or not a properly formed ABC record? Line:", line
        return False
    
    a_concept_id = entry[0]
    a_name       = entry[1]
    
    c_concept_id = entry[2]
    c_name       = entry[3]
    
    linked_concepts = '|'.join(entry[4:])  # rejoin tail B concepts together
    linked_concepts = unicode(linked_concepts).strip()
    if len(linked_concepts) == 0: 
        if DEBUG:
            print >>logger,"\n### ABCL warning: B concepts missing? Line:", line
        return False
              
    if TRACE:
        print >>logger,"\n#### ABC Entry:"
        print >>logger,"A[", a_concept_id,"]: ", a_name
        print >>logger,"C[", c_concept_id,"]: ", c_name
        print >>logger,"B linked concepts:",     linked_concepts
        print >>logger,"###################\n"

    if test: 
        testHostData( query_engine, a_concept_id, a_name, c_concept_id, c_name )
    
    # Find associated A-C Implicitome Tuple then add linked_concepts string
    try:
        
        if TRACE:
            print >>logger,"### ABCL: Trying found_or_added = findOrAddLinkedConcepts"
        
        # first, check if the given linked_concepts already associated with the specified A-C id's
        found_or_added = findOrAddLinkedConcepts( query_engine, a_concept_id, c_concept_id, linked_concepts, limit )
        
        if TRACE:
            print >>logger,"### ABCL: Exited findOrAddLinkedConcepts..."

        lineClip = 60 if len(line)>60 else len(line)
        
        if found_or_added:
            
            if TRACE:
                print >>logger,"... found or added linked_concepts: ", linked_concepts
                
            # ... then double check if properly created, by attempting to add it again
            found_again = findOrAddLinkedConcepts( query_engine, a_concept_id, c_concept_id, linked_concepts, limit )
            
            if found_again: 
                
                if echo:
                    print >>logger,"### ABCL: updated tuple, A[", a_name,"] => C[", c_name,"] <=> B[", linked_concepts[ 0 : lineClip ],"...]"
                    
                return True
 
        # otherwise, if I get to this point, then no valid tuple
        # available to which B concepts could be added (fail silently unless tracing...)
        if echo:
            print >>logger,"### ABCL: missing tuple, line not processed:", line[ 0 : lineClip ],"..."
        
    except Exception as e:
        print >>logger,"\n### ABCL error: for input data:", line,": ",e
    
    return False

def loadFile( filename, limit=ImplicitomeQuery.DEFAULT_PERCENTILE_QUERY_LIMIT, echo=DEBUG, test=TEST ):
    
    query_engine = QueryFactory(DSID.IMPLICITOME)
    try:
        n = l = 0
        with codecs.open(filename, encoding='utf-8', mode='r') as datafile:
            for line in datafile:
                
                line = line.strip()
                
                n += 1
                
                if loadEntry( query_engine, line, limit, echo, test=test ):

                    l += 1
                
                if n%10 == 0:
                    print >>logger,".", # progress dot...
                if n%500 == 0:
                    print >>logger # newline every fifty dots
                if n%10000 == 0:
                    print >>logger,"### ABCL: ",n,"lines loaded so far..."
                
                if test and n>=100:
                    break
                    
        print >>logger,"### ABCL: total of",l," tuples updated (from ",n," input data lines)"
                  
    except IOError as ioe:
        print >>logger,ioe

     
from os.path import basename

def abcLoad( filename, limit = ImplicitomeQuery.DEFAULT_PERCENTILE_QUERY_LIMIT, echo=DEBUG ):
    
    print >>logger,"Calling ABC loader (ABCL) thread with filename '",filename, "' with threshold percentile limit = ", limit
        
    Thread(
        target=loadFile, 
        name="ABCLoader_"+basename(filename), 
        kwargs={
            "filename" : filename,
            "limit"    : limit,
            "echo"     : echo,
            "test"     : TEST,
        }
    ).start()

def countFile(filename,test=TEST):
    
    try:
        n = b = 0
        with codecs.open(filename, encoding='utf-8', mode='r') as datafile:
            for line in datafile:
                line = line.strip()
                n += 1
                
                entry = line.split("|")
                
                if len(entry)<6:
                    if DEBUG:
                        print >>logger,"### BCC warning: B concepts missing or not a properly formed ABC record? Line:",line
                    continue
                
                linked_concepts = '|'.join(entry[4:])  # rejoin tail B concepts together
                linked_concepts = unicode(linked_concepts).strip()
                if len(linked_concepts) == 0: 
                    if DEBUG:
                        print >>logger,"### BCC warning: B concepts missing? Line:",line
                    continue
                
                # count B concept entry found
                b += 1
                
                if n%100 == 0:
                    print >>logger,".", # progress dot...
                if n%2500 == 0:
                    print >>logger # newline every 25 dots
                if n%100000 == 0:
                    print >>logger,"### BCC: ",b," non-null B concept records found among a total of ",n," lines loaded so far..."
                
                if test and n >= 10:
                    break
                   
        print >>logger,"### BCC: ",b," non-null B concept records loaded for a total of ",n," input data lines audited"
                  
    except IOError as ioe:
        print >>logger,ioe

def abcCount(filename):
    print >>logger,"Calling B concept counter (BCC) thread with filename: ",filename
    Thread(
        target=countFile, 
        name="ABCCounter_"+basename(filename), 
        kwargs={"filename":filename}
    ).start()

"""
The ABCLoad2 algorithm generates a new SQL Tuples table for the Implicitome table
by merging the old Tuples table as a Stream with the in-memory contents of
the whole ABC B concepts file of new data.

Note that this algorithm requires a huge amount of memory to hold the
ABC file in memory for the merge.

The Implicitome database should have created a "TuplesMerged" table to hold the 
cleaned up copy of the data. The data model for this is identical to Tuples:

+-----------------+----------------+------+-----+--------------------+----------------+
| Field           | Type           | Null | Key | Default            | Extra          |
+-----------------+----------------+------+-----+--------------------+----------------+
| tuple_id        | int(11)        | NO   | PRI | NULL               | auto_increment |
| sub_id          | int(11)        | NO   |     | NULL               |                |
| sub_name        | varchar(100)   | YES  |     | NULL               |                |
| obj_id          | int(11)        | NO   |     | NULL               |                |
| obj_name        | varchar(100)   | YES  |     | NULL               |                |
| score           | decimal(20,16) | NO   |     | 0.0000000000000000 |                |
| percentile      | decimal(12,10) | YES  |     | NULL               |                |
| linked_concepts | text           | YES  |     | NULL               |                |
+-----------------+----------------+------+-----+--------------------+----------------+

The new table needs to be manually substituted into the Implicitome database once
it is loaded (see MySQL documentation on how to do this...)

"""
    
# Huge in-memory dictionary of concept indexed ABC concepts     
ABCCatalog = {}
missing_B_concepts = 0
     
def loadEntry2( line, logfile ):
                
    entry = line.split("|")
    
    if len(entry)<6:
        missing_B_concepts += 1 
        print >>logfile,"\n### ABCL2 warning: B concepts missing or not a properly formed ABC record? Line:", line
        logfile.flush()
        return False
    
    a_concept_id = str(entry[0].strip())
    a_name       = entry[1]
    
    c_concept_id = str(entry[2].strip())
    c_name       = entry[3]
    
    linked_concepts = '|'.join(entry[4:])  # rejoin tail B concepts together
    linked_concepts = unicode(linked_concepts).strip()
    if len(linked_concepts) == 0:
        missing_B_concepts += 1
        if TRACE:
            print >>logfile,"\n### ABCL2 warning: B concepts missing? Line:", line
        return False
              
    if TRACE:
        print >>logfile,"\n#### ABCL2:"
        print >>logfile,"A[", a_concept_id,"]: ", a_name
        print >>logfile,"C[", c_concept_id,"]: ", c_name
        print >>logfile,"B linked concepts:",     linked_concepts
        print >>logfile,"###################\n"
        logfile.flush()
   
    if not a_concept_id in ABCCatalog:
        ABCCatalog[a_concept_id] = {}
        
    if not c_concept_id in ABCCatalog[a_concept_id]:
        ABCCatalog[a_concept_id][c_concept_id] = [1, linked_concepts] # B concept with hit counter
        return True
    
    else:
        ABCCatalog[a_concept_id][c_concept_id][0] += 1 # increment the counter
        if DEBUG:
            print >>logfile,"\nDuplicate entry: ", line[0:50]
            logfile.flush()
        return False

def readABCFile2( filename, logfile ):
    
    print >>logfile,"\n### Entering ABCL2.readABCFile2()..."
    
    # Read in the ABC concept file, one line at a time.
    try:
        n = l = 0
        with codecs.open(filename, encoding='utf-8', mode='r') as datafile:
            
            for line in datafile:
                
                line = line.strip()
                
                n += 1
                
                if loadEntry2( line, logfile ): l += 1
                
                if n%25 == 0:
                    print >>logfile,".",
                if n%500 == 0:
                    print >>logfile
                    logfile.flush()
                    
                if n%10000 == 0:
                    print >>logfile,"### ABCL: ",n," lines loaded so far..."
                    logfile.flush()
                
                if TEST2 and n >= 100000:
                    break
                   
        print >>logfile,"### ABCL2: total of",l," unique B concept entries recorded from ",n," input data lines)"
        logfile.flush()

    except IOError as ioe:
        print >>logfile,ioe

def mergeTuples2( logfile, limit ):
    
    print >>logfile,"\n### Entering ABCL2.mergeTuples2() with threshold percentile query limit of ", limit
    
    query_engine = QueryFactory(DSID.IMPLICITOME)
    
    # use a direct query here, to have more control
    cursor = connections[ query_engine.getDsid() ].cursor()
    
    batchsize = 1000
    n = 0

    relation_negatives = 0
    object_negatives   = 0
    duplicate_tuples   = 0

    # Keep track of pairs of concept_ids recently seen
    # in order to filter out tandem duplicate records
    recent_subject_concept_id = None
    recent_object_concept_id = None
    
    # Visit all subsets of Tuples entries 
    # over given percentile threshhold limit
    for start in range( 0, int(limit), batchsize ):  
        
        # ... get the entries later...
        subsetQuery  =  "SELECT * FROM tuples "
        
        if TEST2: 
            # Spanning TEST2 concepts:
            # ALDH1A1==3063788 is known to be at the top of, whereas
            # URB1==3059821 is known to be at the tail of, the ABC model file
            subsetQuery +=  " WHERE sub_id=3063788 OR sub_id=3059821" 
            
        subsetQuery +=  " LIMIT "+str(batchsize)+" OFFSET "+str(start)
        
        print >>logfile,"... reading records from ",str(start)," to ",str(start+batchsize-1)
        if DEBUG: 
            print >>logfile,"... using SQL query: ",subsetQuery
        logfile.flush()
        
        cursor.execute( subsetQuery )
        
        if TRACE:
            print >>logfile,"... now, iterating through the subset of records ..."
            logfile.flush()
        
        tupleSubset = cursor.fetchall()
        
        for entry in tupleSubset:
            
            # Fields indices are:
            # [0]: tuple_id
            # [1]: sub_id
            # [2]: sub_name
            # [3]: obj_id
            # [4]: obj_name
            # [5]: score
            # [6]: percentile   
            # [7]: linked_concepts   
                     
            n += 1
                            
            if n%25 == 0:
                print >>logfile,".",
                
            if n%500 == 0:
                print >>logfile
                logfile.flush()
                
            if n%10000 == 0:
                print >>logfile,"### ABCL2: ",n," tuples merged so far..."
                logfile.flush()
            
            if TEST2 and n >= 14000:
                break
           
            a_concept_id = str(entry[1])
            c_concept_id = str(entry[3])
            
            if not ( a_concept_id in ABCCatalog or c_concept_id in ABCCatalog ) :
                relation_negatives += 1
                if DEBUG: 
                    print >>logfile,"\n### ABCL2: no B concepts for relation A[", a_concept_id,"] <=> C[", c_concept_id,"]"
                    logfile.flush()
                continue
            else:
                if a_concept_id in ABCCatalog:
                    if not c_concept_id in ABCCatalog[a_concept_id]:
                        object_negatives += 1    
                        if DEBUG:
                            print >>logfile,"\n### ABCL2: no B concepts for object in relation A[", a_concept_id,"] => C[", c_concept_id,"]"
                            logfile.flush()
                        continue
                    else:
                        subject_concept_id = a_concept_id
                        object_concept_id  = c_concept_id 
                
                else: # c_concept_id in ABCCatalog 
                    if not a_concept_id in ABCCatalog[c_concept_id]:
                        object_negatives += 1    
                        if DEBUG:
                            print >>logfile,"\n### ABCL22: no B concepts for object in relation C[", a_concept_id,"] => A[", c_concept_id,"]"
                            logfile.flush()
                        continue
                    else:
                        subject_concept_id = c_concept_id
                        object_concept_id  = a_concept_id 
    
            if TRACE:
                print >>logfile,"A[", subject_concept_id,"] => C[", object_concept_id,"] has B concepts"
                logfile.flush()
            
            # Filter out tandem duplicate Tuple records while doing the ABC loading
            if subject_concept_id == recent_subject_concept_id and \
               object_concept_id  == recent_object_concept_id:
                duplicate_tuples += 1
                if DEBUG:
                    print >>logfile,"\n#### ABCL2.mergeTuples2: found tandem duplication of relation A[", subject_concept_id,"] => C[", object_concept_id,"] ... discarded!"
                    logfile.flush()
                continue
            
            else:
                recent_subject_concept_id = subject_concept_id
                recent_object_concept_id  = object_concept_id
            
            # B concept found for relationship! Insert into new Tuples Merge table
            mergedTuple = TuplesMerged(
                                # AUTOFIELD Primary Key - I DO NOT think that it is a 
                                # foreign key anywhere hence needs to be conserved...
                                # tuple_id   = entry[0], 
                                sub_id     = entry[1], # use numeric value here...
                                sub_name   = entry[2],
                                obj_id     = entry[3], # ...and here...
                                obj_name   = entry[4],
                                score      = entry[5],
                                percentile = entry[6],
                           # ... but need to use string equivalents, to successfully retrieve B concepts from ABCCatalog
                           linked_concepts = ABCCatalog[subject_concept_id][object_concept_id][1]  # get the stored linked_concept  
            )
            
            try:
                mergedTuple.save( using = query_engine.getDsid() )
                commit( query_engine.getDsid() ) # commit the transaction
                
            except Exception as e:
                print >>logfile,"\n#### ABCL2.mergeTuples2: could not save merged relation A[", subject_concept_id,"] => C[", object_concept_id,"]?\n",e
                logfile.flush()
                
        if TEST2 and n >= 14000:
            break
    
    # Generate and report some stats on the ABC Catalog
    duplStats = {}
    for subject_concept_id in ABCCatalog.keys():
        for object_concept_id in ABCCatalog[subject_concept_id].keys():
            freq = ABCCatalog[subject_concept_id][object_concept_id][0]
            if not freq in duplStats:
                duplStats[freq] = 1
            else:
                duplStats[freq] += 1
            
    print >>logfile, "\nABC model file"
    print >>logfile, "Line Frequency"
    print >>logfile, "F\tNo. Entries"
    total_entries = 0
    freq_count = 0
    for freq in sorted(duplStats.keys()):
        print >>logfile, freq,"\t", duplStats[freq]
        total_entries += duplStats[freq]
        freq_count += freq * duplStats[freq]
        
    print >>logfile, "\nAverage: ", freq_count / total_entries, " input lines seen per ABC file entry"
    
    # Report the run numbers here...
    print >>logfile,"\n### Leaving ABCL2.mergeTuples2(). ",n," tuples merged."
    print >>logfile,"\n\t",missing_B_concepts," missing B concepts lines found in ABC model file."
    print >>logfile,"\n\t",relation_negatives," A=>C implicit relations observed to lack associated B concepts."
    print >>logfile,"\n\t",object_negatives," relations with object concepts (only) lacking B concepts associations\n(i.e. subject concepts are associated with other objects having B concepts)."
    print >>logfile,"\n\t",duplicate_tuples," tandem duplications in tuple relations filtered out."
    logfile.flush()

def loadFile2( filename, limit=ImplicitomeQuery.DEFAULT_PERCENTILE_QUERY_LIMIT ):
    
    with codecs.open(PROJECT_PATH+"/logs/abclog2.log", encoding='utf-8', mode='w+') as logfile:
        
        print >>logfile,"\n### Entering ABCL2.loadFile2()..."
        logfile.flush()
        
        # Read in the ABC B concepts into in-memory dictionary (?)
        # Can likely only effective run this step inside a
        # machine/process within a huge memory AWS instance (e.g. 256 GB?)
        readABCFile2( filename, logfile )
        
        # Then, iterate through the entire existing Tuples table, 
        # merging B concepts into a new "TupleMerged" table of records.
        #
        # Note that this step probably needs an allocation of significant
        # (50GB?) free hard drive space for the new MySQL table
        mergeTuples2( logfile, limit )

def abcLoad2( filename, limit = ImplicitomeQuery.DEFAULT_PERCENTILE_QUERY_LIMIT ):
    
    print >>logger,"Calling ABC loader 2 (ABCL2) thread with filename '",filename, "' with threshold percentile  limit number of tuples = ", limit
        
    Thread(
        target=loadFile2, 
        name="ABCLoader2_"+basename(filename), 
        kwargs={
            "filename" : filename,
            "limit"    : limit,
        }
    ).start()

