"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from __future__ import unicode_literals

from django.db import models

# These Django models roughly align with the Implicitome
# but not exactly. The following model tweaks are necessary:
#
# All models augmented at least one Autoincrement primary key, i.e.
#
# Needed to add the 'dblink_id' key to the Dblink table. The 'id' field of 
# dblink was also renamed to 'identifier' to avoid 
# any clashes with the Django conventional use of 'id'
#
# Needed to add the 'relation_id' Autoincrement primary key field to Relation
#
# Needed to add the 'term_id' Autoincrement primary key field to Term, 
# after renaming the previous 'termid' field to 'subterm_id'
#
# For consistency, all 'id' fields were rewritten to include
# an underscore between the root name and 'id' suffix of the field name

class Concept(models.Model):
    concept_id = models.AutoField(primary_key=True)
    name       = models.CharField(max_length=255, blank=True)
    definition = models.CharField(max_length=10000, blank=True)
    is_orphan  = models.BooleanField(db_column="is_orphan",default=0)

    class Meta:
        managed = True
        db_table = 'concept'


class Dblink(models.Model):
    dblink_id  = models.AutoField(primary_key=True)
    concept_id = models.IntegerField()
    db_id      = models.CharField(max_length=4)
    identifier = models.CharField(max_length=255)

    class Meta:
        managed  = True
        db_table = 'dblink'
        unique_together = (('concept_id','db_id','identifier'),)

class Relation(models.Model):
    relation_id     = models.AutoField(primary_key=True)
    concept1_id     = models.IntegerField()
    concept2_id     = models.IntegerField()
    relationtype_id = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'relation'
        unique_together = (('concept1_id','concept2_id'),)

class Term(models.Model):
    term_id    = models.AutoField(primary_key=True)
    concept_id = models.IntegerField()
    subterm_id = models.IntegerField()
    text      = models.CharField(max_length=255, blank=True)
    casesensitive  = models.IntegerField(blank=True, null=True)
    ordersensitive = models.IntegerField(blank=True, null=True)
    normalised     = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'term'
        unique_together = (('concept_id','subterm_id'),)

class Tuples(models.Model):
    tuple_id = models.AutoField(primary_key=True)
    sub_id   = models.IntegerField()
    sub_name = models.CharField(max_length=100, blank=True)
    obj_id   = models.IntegerField()
    obj_name = models.CharField(max_length=100, blank=True)
    score    = models.DecimalField(max_digits=20, decimal_places=16)
    percentile = models.DecimalField(max_digits=12, decimal_places=10, blank=True, null=True)
        
    # June 2, 2015 release 0.10 - string of pipe separated B concept 3-tuples of id/name/contribution
    linked_concepts = models.TextField(max_length=500, blank=True)
    
    class Meta:
        managed = True
        db_table = 'tuples'

class TuplesMerged(models.Model):
    tuple_id = models.AutoField(primary_key=True)
    sub_id   = models.IntegerField()
    sub_name = models.CharField(max_length=100, blank=True)
    obj_id   = models.IntegerField()
    obj_name = models.CharField(max_length=100, blank=True)
    score    = models.DecimalField(max_digits=20, decimal_places=16)
    percentile = models.DecimalField(max_digits=12, decimal_places=10, blank=True, null=True)
        
    # June 2, 2015 release 0.10 - string of pipe separated B concept 3-tuples of id/name/contribution
    linked_concepts = models.TextField(max_length=500, blank=True)
    
    class Meta:
        managed = True
        db_table = 'tuples_merged'
