# -*- coding: utf-8 -*-
"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from gbk.settings import logger

from django.test import TestCase

from gbk.settings import DB_ENGINE

from query.datasource import DSID
from query import QueryFactory

from models import ( 
    Concept,
    Dblink,
    Relation,
    Term,
    Tuples,
)

from implicitome import ImplicitomeTestData

class ImplicitomeBaselineTest(TestCase):
    
    def test_concept_retrieval(self):
        """ Retrieve an Implicitome Concept """
        c = Concept.objects.using(DSID.IMPLICITOME).get(name=ImplicitomeTestData.CONCEPT1_NAME)
        self.assertEqual( unicode(c.definition), ImplicitomeTestData.CONCEPT1_DEFINITION)
       
    def test_dblink_retrieval(self):
        """Retrieve an Implicitome Dblink"""
        d = Dblink.objects.using(DSID.IMPLICITOME).get(concept_id=3)
        self.assertEqual( unicode(d.db_id), u'UMLS')
        self.assertEqual( unicode(d.identifier), ImplicitomeTestData.CONCEPT3_UMLS)

    def test_relation_retrieval(self):
        """Retrieve an Implicitome Relation"""
        r = Relation.objects.using(DSID.IMPLICITOME).get(relation_id=1)
        self.assertEqual( r.concept1_id, 1 )
        self.assertEqual( r.concept2_id, 3 )
        self.assertEqual( r.relationtype_id, 2 )

    def test_term_retrieval(self):
        """Retrieve an Implicitome Term"""
        trm = Term.objects.using(DSID.IMPLICITOME).get(term_id=1)
        self.assertEqual( trm.concept_id, 1 )
        self.assertEqual( trm.subterm_id, 0 )
        self.assertEqual( unicode(trm.text), ImplicitomeTestData.CONCEPT1_NAME)
        self.assertEqual( trm.casesensitive, 0 )
        self.assertEqual( trm.ordersensitive, 1 )
        self.assertEqual( trm.normalised, 0 )
  
    def test_tuples_retrieval(self):
        """Retrieve an Implicitome Tuples entry"""
        tpl = Tuples.objects.using(DSID.IMPLICITOME).get(tuple_id = ImplicitomeTestData.TUPLE1)
        self.assertEqual( tpl.sub_id, ImplicitomeTestData.CONCEPT1_CONCEPT_ID )
        self.assertEqual( unicode(tpl.sub_name), ImplicitomeTestData.CONCEPT1_NAME)
        self.assertEqual( tpl.obj_id, ImplicitomeTestData.CONCEPT3_CONCEPT_ID )
        self.assertEqual( unicode(tpl.obj_name), ImplicitomeTestData.CONCEPT3_NAME)
        self.assertEqual( str( tpl.score )[0:6],      "0.0625" ) # convert to string to avoid rounding errors in comparison
        self.assertEqual( str( tpl.percentile )[0:4], "90.0" )   # convert to string to avoid rounding errors in comparison

        """Retrieve an Tuple with Concept Linkages"""
        tp4 = Tuples.objects.using(DSID.IMPLICITOME).get(tuple_id = ImplicitomeTestData.TUPLE4)
        self.assertEqual( tp4.sub_id, ImplicitomeTestData.CONCEPT3_CONCEPT_ID )
        self.assertEqual( unicode(tp4.sub_name), ImplicitomeTestData.CONCEPT3_NAME)
        self.assertEqual( tp4.obj_id, ImplicitomeTestData.CONCEPT5_CONCEPT_ID )
        self.assertEqual( unicode(tp4.obj_name), ImplicitomeTestData.CONCEPT5_NAME)
        self.assertEqual( str( tp4.score )[0:6],      "0.0575" ) # convert to string to avoid rounding errors in comparison
        self.assertEqual( str( tp4.percentile )[0:4], "89.5"   ) # convert to string to avoid rounding errors in comparison
        self.assertEqual( unicode(tp4.linked_concepts), ImplicitomeTestData.TUPLE4_LINKED_CONCEPTS)

class IntegratedImplicitomeTest(TestCase):
    
    def setUp(self):
        self.query_engine = QueryFactory(DSID.IMPLICITOME)
    
    def test_retrieve_concepts_like_search_string(self):
        
        # Default search with exact = False, insensitive = True
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('synthetase')
        print >>logger,  "\nIMPLICITOME.retrieveConceptsBySearchString('synthetase'):",
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 1)
        self.assertEqual( unicode(concepts[0]['name']), u'2-5a synthetase')
        
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('2-5a Synthetase', exact = True)
        print >>logger,  "\nIMPLICITOME.retrieveConceptsBySearchString('2-5a Synthetase', exact = True):",
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 1)
        self.assertEqual( unicode(concepts[0]['name']), u'2-5a synthetase')

        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('synthetase', exact = True)
        print >>logger,  "\nIMPLICITOME.retrieveConceptsBySearchString('synthetase', exact = True):",
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 0)

        if(DB_ENGINE != 'sqlite3'):
            concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('Synthetase', insensitive = False)
            print >>logger,  "\nIMPLICITOME.retrieveConceptsBySearchString('Synthetase', insensitive = False):",
            print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
            self.assertEqual(len(concepts), 0)
        
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('2-5a synthetase', exact = True, insensitive = False)
        print >>logger,  "\nIMPLICITOME.retrieveConceptsBySearchString('2-5a synthetase', exact = True, insensitive = False):",
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 1)
        self.assertEqual(unicode(concepts[0]['name']), u'2-5a synthetase')

        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('Synthetase', exact = True, insensitive = False)
        print >>logger,  "\nIMPLICITOME.retrieveConceptsBySearchString('Synthetase', exact = True, insensitive = False):",
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 0)
 
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('a')
        print >>logger,  "\nIMPLICITOME.retrieveConceptsBySearchString('a'):",
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 7)
        self.assertEqual(concepts[0]['concept_id'], 1)
        self.assertEqual(concepts[1]['concept_id'], 4)
        self.assertEqual(concepts[2]['concept_id'], 3)
        self.assertEqual(concepts[3]['concept_id'], 6)
        self.assertEqual(concepts[4]['concept_id'], 5)
        self.assertEqual(concepts[5]['concept_id'], 2)
        self.assertEqual(concepts[6]['concept_id'], 7)
        
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('a', orderDir='desc')
        print >>logger,  "\nIMPLICITOME.retrieveConceptsBySearchString('a', orderDir='desc'):",
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 7)
        self.assertEqual(concepts[0]['concept_id'], 7)
        self.assertEqual(concepts[1]['concept_id'], 2)
        self.assertEqual(concepts[2]['concept_id'], 5)
        self.assertEqual(concepts[3]['concept_id'], 6)
        self.assertEqual(concepts[4]['concept_id'], 3)
        self.assertEqual(concepts[5]['concept_id'], 4)
        self.assertEqual(concepts[6]['concept_id'], 1)
        
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('a' , start=1)
        print >>logger,  "\nIMPLICITOME.retrieveConceptsBySearchString('a' , start=1):",
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 6)
        self.assertEqual(concepts[0]['concept_id'], 4)
        self.assertEqual(concepts[1]['concept_id'], 3)
        self.assertEqual(concepts[2]['concept_id'], 6)
        self.assertEqual(concepts[3]['concept_id'], 5)
        self.assertEqual(concepts[4]['concept_id'], 2)
        self.assertEqual(concepts[5]['concept_id'], 7)
        
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('a', start=1, length=1)
        print >>logger,  "\nIMPLICITOME.retrieveConceptsBySearchString('a' , start=1, length=1):",
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 1)
        self.assertEqual(concepts[0]['concept_id'], 4)
        
        concepts,totalHits,filteredHits = self.query_engine.retrieveConceptsBySearchString('a' , text_filter='catalyzes')
        print >>logger,  "\nIMPLICITOME.retrieveConceptsBySearchString('a', text_filter='catalyzes'):",
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", concepts
        self.assertEqual(len(concepts), 1)
        self.assertEqual(concepts[0]['concept_id'], 2)

    def test_get_concept_name(self):
        name = self.query_engine.getConceptName(1)
        print >>logger,  "\nIMPLICITOME.getConceptName(1):", name
        self.assertIsNotNone(name)
        self.assertEqual(name, ImplicitomeTestData.CONCEPT1_NAME)
        
    def test_get_unknown_concept_name(self):
        name = self.query_engine.getConceptName(100)
        print >>logger,  "\nIMPLICITOME.getConceptName(100) - unknown:", str(name)
        self.assertIsNone(name)
        
    def test_get_concept_details(self):
        concept = self.query_engine.getConceptDetails(1)
        print >>logger,  "\nIMPLICITOME.getConceptDetails(1):", concept
        self.assertIsNotNone(concept)
        self.assertEqual(concept["concept_id"],1)
        self.assertEqual(concept["cui"], ImplicitomeTestData.CONCEPT1_NAME )
        self.assertEqual(unicode(concept["type"]), u'ENTREZ')
        self.assertEqual(unicode(concept["name"]), ImplicitomeTestData.CONCEPT1_NAME)
        self.assertEqual(unicode(concept["definition"]), ImplicitomeTestData.CONCEPT1_DEFINITION)
        self.assertEqual(unicode(concept["ghr"]), u'')
        self.assertEqual(unicode(concept["omim"]), ImplicitomeTestData.CONCEPT1_OM )

    def test_annotated_concept(self):
        concept = self.query_engine.getAnnotatedConcept(1)
        print >>logger,  "\nIMPLICITOME.getAnnotatedConcept(1):",concept
        self.assertIsNotNone(concept)
        self.assertEqual(concept["concept_id"],1)
        self.assertEqual(unicode(concept["cui"]), ImplicitomeTestData.CONCEPT1_NAME) 
        self.assertEqual(unicode(concept["type"]), u'ENTREZ')
    
    def test_find_concept_by_alias(self):
        concept = self.query_engine.findConceptByAlias(ImplicitomeTestData.CONCEPT1_UMLS)
        print >>logger,  "\nIMPLICITOME.findConceptByAlias("+ImplicitomeTestData.CONCEPT1_UMLS+"):", concept
        self.assertIsNotNone(concept)
        self.assertEqual(concept["concept_id"],1)
        self.assertEqual(concept["cui"], ImplicitomeTestData.CONCEPT1_UMLS)
        self.assertEqual(unicode(concept["type"]), u'META')
        self.assertEqual(unicode(concept["name"]), ImplicitomeTestData.CONCEPT1_NAME)
        self.assertEqual(unicode(concept["definition"]), ImplicitomeTestData.CONCEPT1_DEFINITION)
        self.assertEqual(unicode(concept["ghr"]), u'')
        self.assertEqual(unicode(concept["omim"]), u'')

        concept = self.query_engine.findConceptByAlias(ImplicitomeTestData.CONCEPT1_EG1)
        print >>logger,  "\nIMPLICITOME.findConceptByAlias("+ImplicitomeTestData.CONCEPT1_EG1+"):", concept
        self.assertIsNotNone(concept)
        self.assertEqual(concept["concept_id"],1)
        # CUI for Entrez gene is the concept name, not the original CUI
        self.assertEqual(concept["cui"], ImplicitomeTestData.CONCEPT1_NAME)
        self.assertEqual(unicode(concept["type"]), u'ENTREZ')
        self.assertEqual(unicode(concept["name"]), ImplicitomeTestData.CONCEPT1_NAME)
        self.assertEqual(unicode(concept["definition"]), ImplicitomeTestData.CONCEPT1_DEFINITION)
        self.assertEqual(unicode(concept["ghr"]), u'')
        self.assertEqual(unicode(concept["omim"]), u'')

        concept = self.query_engine.findConceptByAlias(ImplicitomeTestData.CONCEPT1_EG2)
        print >>logger,  "\nIMPLICITOME.findConceptByAlias("+ImplicitomeTestData.CONCEPT1_EG2+"):", concept
        self.assertIsNotNone(concept)
        self.assertEqual(concept["concept_id"],1)
        # CUI for Entrez gene is the concept name, not the original CUI
        self.assertEqual(concept["cui"], ImplicitomeTestData.CONCEPT1_NAME) 
        self.assertEqual(unicode(concept["type"]), u'ENTREZ')
        self.assertEqual(unicode(concept["name"]), ImplicitomeTestData.CONCEPT1_NAME) 
        self.assertEqual(unicode(concept["definition"]), ImplicitomeTestData.CONCEPT1_DEFINITION)
        self.assertEqual(unicode(concept["ghr"]), u'')
        self.assertEqual(unicode(concept["omim"]), u'')

        concept = self.query_engine.findConceptByAlias(ImplicitomeTestData.CONCEPT1_OM)
        print >>logger,  "\nIMPLICITOME.findConceptByAlias("+ImplicitomeTestData.CONCEPT1_OM+"):", concept
        self.assertIsNotNone(concept)
        self.assertEqual(concept["concept_id"],1)
        self.assertEqual(concept["cui"], ImplicitomeTestData.CONCEPT1_OM )
        self.assertEqual(unicode(concept["type"]), u'OMIM')
        self.assertEqual(unicode(concept["name"]), ImplicitomeTestData.CONCEPT1_NAME)
        self.assertEqual(unicode(concept["definition"]), ImplicitomeTestData.CONCEPT1_DEFINITION)
        self.assertEqual(unicode(concept["ghr"]), u'')
        self.assertEqual(unicode(concept["omim"]), ImplicitomeTestData.CONCEPT1_OM)

    def test_get_identifiers_from_concept_id(self):
        identifiers = self.query_engine.getAliasesForConceptId(None)
        print >>logger,  "\nIMPLICITOME.getAliasesForConceptId(None) is invalid? Returns:", identifiers, " as expected!"
        self.assertEqual(identifiers,{})

        identifiers = self.query_engine.getAliasesForConceptId(100000)
        print >>logger,  "\nIMPLICITOME.getAliasesForConceptId("+str(100000)+") is non-existent value? Returns:", identifiers, " as expected!"
        self.assertEqual(identifiers,{})

        identifiers = self.query_engine.getAliasesForConceptId(ImplicitomeTestData.CONCEPT1_CONCEPT_ID)
        print >>logger,  "\nIMPLICITOME.getAliasesForConceptId("+ str(ImplicitomeTestData.CONCEPT1_CONCEPT_ID)+ ") Returns:", identifiers
        self.assertIsNotNone(identifiers)
        self.assertIsNotNone(identifiers.get('UMLS',None))
        self.assertEqual(unicode(identifiers['UMLS'][0]), ImplicitomeTestData.CONCEPT1_UMLS)
        self.assertIsNotNone(identifiers.get('OM',None))
        self.assertEqual(unicode(identifiers['OM'][0]), ImplicitomeTestData.CONCEPT1_OM)
        self.assertIsNotNone(identifiers.get('EG',None))
        self.assertEqual(unicode(identifiers['EG'][0]), ImplicitomeTestData.CONCEPT1_EG1)
        self.assertEqual(unicode(identifiers['EG'][1]), ImplicitomeTestData.CONCEPT1_EG2)

    def test_retrieve_implicit_relations_from_concept_id(self):
        
        results,totalHits,filteredHits = \
            self.query_engine.retrieveImplicitRelationsByConceptId(3) # default percentile is 99.0%
        print >>logger,  "\nIMPLICITOME.retrieveImplicitRelationsByConceptId(3)", 
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual( totalHits, 0 )
        self.assertEqual( filteredHits, 0 )
        self.assertEqual( len(results), 0 )
                
    """
    May 18th, 2015 revision: changed structure of Implicit result data
    to single related 'other' concept rather than 'subject/object' pair 
    plus using 'tuple_id' rather than 'relation_id' as the key 
    (Modified  unit tests to suit)
    """
    def test_retrieve_implicit_relations_from_alias(self):
        
        results,totalHits,filteredHits = \
            self.query_engine.retrieveImplicitRelationsByAlias(None,
                            percentile=0.0 # full permissive
            ) 
        print >>logger,  "\nIMPLICITOME.retrieveImplicitRelationsByAlias( None, percentile=0.0 )", 
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual( totalHits, 0 )
        self.assertEqual( filteredHits, 0 )
        self.assertEqual( len(results), 0 )
                
        results,totalHits,filteredHits = \
            self.query_engine.retrieveImplicitRelationsByAlias('Some Non-existent id',
                            percentile=0.0 # full permissive
            ) 
        print >>logger,  "\nIMPLICITOME.retrieveImplicitRelationsByAlias( 'Some Non-existent id', percentile=0.0 )", 
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual( totalHits, 0 )
        self.assertEqual( filteredHits, 0 )
        self.assertEqual( len(results), 0 )
                
        results,totalHits,filteredHits = \
            self.query_engine.retrieveImplicitRelationsByAlias(
                            ImplicitomeTestData.CONCEPT3_UMLS
            ) # default percentile is 99.0%
        print >>logger,  "\nIMPLICITOME.retrieveImplicitRelationsByAlias("+ str(ImplicitomeTestData.CONCEPT3_UMLS)+")", 
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual( totalHits, 0 )
        self.assertEqual( filteredHits, 0 )
        self.assertEqual( len(results), 0 )
                
        results,totalHits,filteredHits = \
            self.query_engine.retrieveImplicitRelationsByAlias(
                            ImplicitomeTestData.CONCEPT3_UMLS,
                            percentile=95.0
            )
        print >>logger,  "\nIMPLICITOME.retrieveImplicitRelationsByAlias("+ str(ImplicitomeTestData.CONCEPT3_UMLS)+", percentile=", 95.0,")", 
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual( totalHits, 0 )
        self.assertEqual( filteredHits, 0 )
        self.assertEqual( len(results), 0 )
                
        results,totalHits,filteredHits = \
            self.query_engine.retrieveImplicitRelationsByAlias(
                            ImplicitomeTestData.CONCEPT3_UMLS,
                            percentile=90.0
            )
        print >>logger,  "\nIMPLICITOME.retrieveImplicitRelationsByAlias("+ str(ImplicitomeTestData.CONCEPT3_UMLS)+", percentile=", 90.0,")", 
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual( totalHits, 3 )
        self.assertEqual( filteredHits, 3 )
        self.assertEqual( len(results), 3 )
        
        self.assertEqual( results[0]['tuple_id'], ImplicitomeTestData.TUPLE3 )
        self.assertEqual( results[0]['score'][0:6], u'0.1875' ) # cheat a bit here with unicode string slice to avoid Decimal precision woes
        self.assertEqual( results[0]['subject']['concept_id'], 3L )                
        self.assertEqual( results[0]['subject']['name'] , ImplicitomeTestData.CONCEPT3_NAME )
        self.assertEqual( results[0]['related']['concept_id'], 4L )                
        self.assertEqual( results[0]['related']['name'] , ImplicitomeTestData.CONCEPT4_NAME )
        self.assertEqual( results[0]['related']['cui']  , ImplicitomeTestData.CONCEPT4_UMLS )
        self.assertEqual( results[0]['related']['type'] , u'META' )
        
        self.assertEqual( results[1]['tuple_id'], ImplicitomeTestData.TUPLE2 )
        self.assertEqual( results[1]['score'][0:5], u'0.125' ) # cheat a bit here with unicode string slice to avoid Decimal precision woes
        self.assertEqual( results[1]['subject']['concept_id'], 3L )                
        self.assertEqual( results[1]['subject']['name'] , ImplicitomeTestData.CONCEPT3_NAME )
        self.assertEqual( results[1]['related']['concept_id'], 2L )                
        self.assertEqual( results[1]['related']['name'] , ImplicitomeTestData.CONCEPT2_NAME )
        self.assertEqual( results[1]['related']['cui']  , ImplicitomeTestData.CONCEPT2_UMLS )
        self.assertEqual( results[1]['related']['type'] , u'META' )
        
        self.assertEqual( results[2]['tuple_id'], ImplicitomeTestData.TUPLE1 )
        self.assertEqual( results[2]['subject']['concept_id'], 3L )                
        self.assertEqual( results[2]['subject']['name'] , ImplicitomeTestData.CONCEPT3_NAME )
        self.assertEqual( results[2]['related']['concept_id'], 1L )                  
        self.assertEqual( results[2]['related']['name'], ImplicitomeTestData.CONCEPT1_NAME )
        self.assertEqual( results[2]['related']['cui'] , ImplicitomeTestData.CONCEPT1_NAME )
        self.assertEqual( results[2]['related']['type'], u'ENTREZ' )
        
        results,totalHits,filteredHits = \
            self.query_engine.retrieveImplicitRelationsByAlias(
                            ImplicitomeTestData.CONCEPT3_UMLS,
                            percentile=90.0,
                            orderDir="asc"
            )
        print >>logger,  "\nIMPLICITOME.retrieveImplicitRelationsByAlias("+ str(ImplicitomeTestData.CONCEPT3_UMLS)+", percentile=", 90.0,", orderDir='asc' )", 
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual( totalHits, 3 )
        self.assertEqual( filteredHits, 3 )
        self.assertEqual( len(results), 3 )
        
        self.assertEqual( results[0]['tuple_id'], ImplicitomeTestData.TUPLE1 )
        self.assertEqual( results[1]['tuple_id'], ImplicitomeTestData.TUPLE2 )
        self.assertEqual( results[2]['tuple_id'], ImplicitomeTestData.TUPLE3 )

        results,totalHits,filteredHits = \
            self.query_engine.retrieveImplicitRelationsByAlias(
                            ImplicitomeTestData.CONCEPT3_UMLS,
                            percentile=90.0,
                            text_filter = u"adenine",
            )
        print >>logger,  "\nIMPLICITOME.retrieveImplicitRelationsByAlias("+\
                            str(ImplicitomeTestData.CONCEPT3_UMLS)+", percentile=", 90.0,\
                            "text_filter = 'adenine')" 
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual( totalHits, 3 )
        self.assertEqual( filteredHits, 1 )
        self.assertEqual( len(results), 1 )
        
        self.assertEqual( results[0]['tuple_id'], ImplicitomeTestData.TUPLE3 )
        self.assertEqual( results[0]['score'][0:6], u'0.1875' ) # cheat a bit here with unicode string slice to avoid Decimal precision woes
        self.assertEqual( results[0]['subject']['concept_id'], 3L )                
        self.assertEqual( results[0]['subject']['name'] , ImplicitomeTestData.CONCEPT3_NAME )
        self.assertEqual( results[0]['related']['concept_id'], 4L )                
        self.assertEqual( results[0]['related']['name'] , ImplicitomeTestData.CONCEPT4_NAME )
        self.assertEqual( results[0]['related']['cui']  , ImplicitomeTestData.CONCEPT4_UMLS )
        self.assertEqual( results[0]['related']['type'] , u'META' )

    def test_retrieve_cooccurrence_of_concepts(self):
        # Query is a comma-delimited A - C pair of Implicitome concept ids for co-occurrence analysis
        # returns a list of A - B tuples (including scores) that have B co-occurrence with C
        query = str(ImplicitomeTestData.TUPLE4)+","+str(ImplicitomeTestData.CONCEPT3_CONCEPT_ID)
        results,totalHits,filteredHits = self.query_engine.retrieveCooccurrenceOfConcepts( query )  
        print >>logger,  "\nIMPLICITOME.retrieveCooccurrenceOfConcepts("+query+")", 
        print >>logger,  "\nTotal Hits:", totalHits,", Filtered:", filteredHits,"Results:\n", results
        self.assertEqual( totalHits, 2 )
        self.assertEqual( filteredHits, 2 )
        self.assertEqual( len(results), 2 )

        self.assertEqual( results[0]['tuple_id'], str(ImplicitomeTestData.TUPLE4) ) # target A-C concept tuple...
        self.assertEqual( results[0]['a_concept']['type'] , 'META' )
        self.assertEqual( results[0]['a_concept']['cui'] , ImplicitomeTestData.CONCEPT3_UMLS )
        self.assertEqual( results[0]['a_concept']['name'] , ImplicitomeTestData.CONCEPT3_NAME )
        self.assertEqual( results[0]['b_concept']['concept_id'], ImplicitomeTestData.CONCEPT1_CONCEPT_ID )                
        self.assertEqual( results[0]['b_concept']['type'] , 'ENTREZ' )
        self.assertEqual( results[0]['b_concept']['cui'] , ImplicitomeTestData.CONCEPT1_NAME )
        self.assertEqual( results[0]['b_concept']['name'] , ImplicitomeTestData.CONCEPT1_NAME )
        # cheat a bit here with unicode string slice to avoid Decimal precision woes
        self.assertEqual( str(results[0]['b_concept']['contribution'])[0:5], "12.75" ) 
        self.assertEqual( results[0]['c_concept']['name'] , ImplicitomeTestData.CONCEPT5_NAME )

        self.assertEqual( results[1]['tuple_id'], str(ImplicitomeTestData.TUPLE4) ) # target A-C concept tuple...
        self.assertEqual( results[1]['a_concept']['type'] , 'META' )
        self.assertEqual( results[1]['a_concept']['cui'] , ImplicitomeTestData.CONCEPT3_UMLS )
        self.assertEqual( results[1]['a_concept']['name'] , ImplicitomeTestData.CONCEPT3_NAME )
        self.assertEqual( results[1]['b_concept']['concept_id'], ImplicitomeTestData.CONCEPT4_CONCEPT_ID )                
        self.assertEqual( results[1]['b_concept']['type'] , 'META' )
        self.assertEqual( results[1]['b_concept']['cui'] , ImplicitomeTestData.CONCEPT4_UMLS )
        self.assertEqual( results[1]['b_concept']['name'] , ImplicitomeTestData.CONCEPT4_NAME )
        # cheat a bit here with unicode string slice to avoid Decimal precision woes
        self.assertEqual( str(results[1]['b_concept']['contribution'])[0:4], "2.25" ) 
        self.assertEqual( results[1]['c_concept']['name'] , ImplicitomeTestData.CONCEPT5_NAME )

from gbk.settings import PROJECT_PATH
from os import path

from abcloader import findOrAddLinkedConcepts, loadEntry, loadFile, loadFile2

FIRST_MISSING_A_CONCEPT_ID="3063780"
FIRST_MISSING_C_CONCEPT_ID="151449"
FIRST_MISSING_LINKED_CONCEPTS=u"43352|Xerostomia|14.5231598383|314719|dryness of eye|6.32990135957|13238|dry eye syndromes|5.50340050567|1707516|Corneal Sensitivity|4.88618279119|455242|Covering eye|3.05354399702"
FIRST_MISSING_ENTRY=u"3063780|ALDH3A1|151449|primary sjögren's syndrome|43352|Xerostomia|14.5231598383|314719|dryness of eye|6.32990135957|13238|dry eye syndromes|5.50340050567|1707516|Corneal Sensitivity|4.88618279119|455242|Covering eye|3.05354399702"

class BConceptLoadingTest(TestCase):

    def setUp(self):
        self.query_engine = QueryFactory(DSID.IMPLICITOME)

    def test_check_missing_relation(self):
        print >>logger,  "\nabcloader.findOrAddLinkedConcepts(FIRST_MISSING_ENTRY) - not found?\n"
        found = findOrAddLinkedConcepts(
                    self.query_engine,
                    ImplicitomeTestData.FIRST_MISSING_A_CONCEPT_ID,
                    ImplicitomeTestData.FIRST_MISSING_C_CONCEPT_ID,
                    ImplicitomeTestData.FIRST_MISSING_LINKED_CONCEPTS
        )
        self.assertFalse(found)
        
    def test_load_missing_entry(self):
        print >>logger,  "\nabcloader.loadEntry(SECOND_MISSING_ENTRY) - added then found?\n"
        
        created = loadEntry(
                    self.query_engine,
                    ImplicitomeTestData.SECOND_MISSING_ENTRY,
                    test=True # special flag to trigger internal local loading of testing tuple data
        )
        self.assertTrue(created) # created
        
        found = findOrAddLinkedConcepts(
                    self.query_engine,
                    ImplicitomeTestData.SECOND_MISSING_A_CONCEPT_ID,
                    ImplicitomeTestData.SECOND_MISSING_C_CONCEPT_ID,
                    ImplicitomeTestData.SECOND_MISSING_LINKED_CONCEPTS
        )
        self.assertTrue(found) # see if you find it again...

    """ # Deprecated tests
    
    def test_load_abc(self):
        print >>logger,  "\nabcloader.loadFile(abc_test_data)\n"
        
        ABC_TEST_DATA_PATH = path.join(PROJECT_PATH,"static","data","abc_test_data")
        loadFile(
                 ABC_TEST_DATA_PATH,
                 echo=True,
                 test=True # special flag to trigger internal local loading of testing tuple data
                 )
        
    def test_load_abc2(self):
        print >>logger,  "\nabcloader.loadFile2(abc_test_data)\n"
        ABC_TEST_DATA_PATH = path.join(PROJECT_PATH,"static","data","abc_test_data")
        loadFile2(ABC_TEST_DATA_PATH)
    """    
