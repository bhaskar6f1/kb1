/*

     Knowledge.Bio - Dashboard JavaScript code

The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

var globals = {
    DEBUG: false,
};

// define global semantic_types.json loading callback
var semtype_class_catalog = {} ;
var id2semtype = {} ;
var subtype2semcode= {} ;
var semcode2name = {} ;
    
var loadCatalog = function( data ) {
    var idx ;
    for( idx in data.subtypes ) {
        var st = data.subtypes[idx] ;
        id2semtype[st.id] = st.code ;
        semcode2name[st.code] = st.name ;
        subtype2semcode[st.subtype] = st.code ;
    }
    for( idx in data.types ) {
        var sg = data.types[idx] ;
        var st_class = "other_node" ;
        if(sg.code === "GENE") {
            st_class = "gene_node" ;
        } else if(sg.code === "DISO") {
            st_class = "disorder_node" ;
        } else if(sg.code === "CHEM") {
            st_class = "chem_node" ;
        } else if(sg.code === "PHYS") {
            st_class = "phys_node" ;
        }
        for( idx in sg.children ) {
            var st_id = sg.children[idx] ;
            semtype_class_catalog[id2semtype[st_id]] = st_class ;
        }
    }
} ;
                        
// Load in Semantic Types from JSON data structure
$.ajax({
    url: semantic_type_url, // located as global in discovery.jade
    dataType: 'json',
    success: loadCatalog,
    async: false
});

var dataBinder = function( arg ) {
        return function(d) { 
            /* alert(" dataBinder closure called? Mode: "+arg["mode"]) ; */
            d.mode     = arg.mode ; 
            d.query    = arg.query ;
            d.type_filter = arg.type_filter ;
            d.csrfmiddlewaretoken = arg.csrf_token ; 
        } ;
    } ;

explicit_table_args = {
                "mode": "cui",
                "query" : rootQueryContext.query(),
                "type_filter" : current_type_filter,
                "csrf_token"  : csrf_token
            } ;
var explicit_data_query = dataBinder( explicit_table_args ) ;

implicit_table_args = {
                "mode": "aliases",
                "query" : rootQueryContext.query(),
                "csrf_token" : csrf_token
            } ;          
var implicit_data_query = dataBinder(implicit_table_args) ;

cooccurrence_table_args = {
                "mode": "cooccurrence",
                "query" : "0,0", /* empty to start */
                "csrf_token" : csrf_token
            } ;          
var cooccurrence_data_query = dataBinder(cooccurrence_table_args) ;

evidence_table_args = {
                "mode"  : "sentences",
                "query" : 0,  /* empty to start */
                "csrf_token" : csrf_token
            } ;          
var evidence_data_query = dataBinder(evidence_table_args) ;

pmid_table_args = {
                "mode"  : "pmid",
                "query" : "",  /* empty to start */
                "csrf_token" : csrf_token
            } ;          
var pmid_data_query = dataBinder(pmid_table_args) ;

function rowDataDiagnostic( context, row_data ) {
    var row_data_object = '' ;
    for (var entry in row_data) {
        row_data_object += entry +" : "+JSON.stringify(row_data[entry])+"\n" ;
    }
    console.log(context+" row:\n\t"+row_data_object ) ; 
}

function explicitConceptRenderer( context, resource ) {
    /* Using closure here to affect the data used for linkage/display */
    return function ( row_data, type, val, meta ) {
    
        var item = row_data[resource] ;
        
        /* alert("Explicit Row data: "+JSON.stringify(row_data)) ; */
        
        /* rowDataDiagnostic("explicitConceptRenderer",row_data) ; */
        
        if (type == 'display') {

            var selected_concept_id       = item.concept_id ;
            var selected_concept_type     = item.type.trim() ;
            var selected_concept_cui      = item.cui.trim() ;
            
            if ( selected_concept_cui.length > 0 ) {
                
                /* alert("Current Concept Name: "+selected_concept_name) ; */
            
                var el_class = "subject" ;
                var concept_div_id ;
                if(context == "pmid" ) {
                    concept_div_id = "PMID_" ;
                } else {
                    concept_div_id = "MAIN_" ;
                }
                concept_div_id += 'ER_relation_'+row_data.DT_RowData.pkey+'_'+resource+'_'+selected_concept_cui ; 
                
                var concept_div    =  '<div class="btn clickable '+el_class+'" id="' + concept_div_id + '"' ;
                
                concept_div += " onclick='clickConcept(" ;
                concept_div += '"explicit"'  ;
                concept_div += ',"' + concept_div_id + '"' ;
                concept_div += ',"' + explicit_table_args.query + '"' ;
                concept_div += ','  + selected_concept_id ;
                concept_div += ',"' + selected_concept_type + '"' ;
                concept_div += ',"' + selected_concept_cui + '"' ;
                
                concept_div += ',"' + item.name.replace("'", "&apos;") + '"' ;
                concept_div += ',"' + row_data.subject.type+":"+row_data.subject.cui + '"' ;
                concept_div += ',"' + row_data.subject.name.replace("'", "&apos;") + '"' ;
                concept_div += ',"' + row_data.subject.semtype + '"' ;
                
                /* explicit relation has a name */
                concept_div += ',"ER' + row_data.predication_id.toString() + '"' ;
                concept_div += ',"' + row_data.predicate + '"' ;
                
                concept_div += ',"' + row_data.object.type + ":" + row_data.object.cui + '"' ;
                concept_div += ',"' + row_data.object.name.replace("'", "&apos;") + '"' ; 
                concept_div += ',"' + row_data.object.semtype + '"' ;
                
                concept_div += '," "," "," "' ; /* ignored extra arguments */
                concept_div += ");'>" + item.name + '</div>' ;
                
                /* alert("concept_div: "+concept_div ) ; */
                
                return concept_div ;
            }
            
        } else {

            /* 'filter', 'sort', 'type' and undefined all just use the name string */
            return item.name;

        }

    } ;
}

function getSemtype(concept) {
    var semtypeSeen = concept.semtype ;
    if( semtypeSeen === '') {
        if( typeof(concept.tui) !== 'undefined' && concept.tui.length>0 ) {
            /* not rigorous: just translate the first tui seen */
            semtypeSeen = subtype2semcode[concept.tui[0]] ; 
        }
    }
    return semtypeSeen ;
}


function implicitConceptRenderer( ) {
    /* Using closure here to affect the data used for linkage/display */
    return function ( row_data, type, val, meta ) {
        
        if (type == 'display') {

            rowDataDiagnostic("implicitConceptRenderer",row_data) ;

            var el_class = "subject" ;
            var concept_div_id = 'DT_IR_relation_'+row_data.DT_RowData.pkey ; 
            var concept_div =  '<div class="btn clickable '+el_class+'" id="' + concept_div_id + '"' ;

            concept_div += " onclick='clickConcept(" ;
            concept_div += '"implicit"'  ;
            concept_div += ',"' + concept_div_id + '"'  ;
            concept_div += ',"' + implicit_table_args.query + '"'  ;
            
            concept_div += ',' + row_data.related.concept_id  ;
            concept_div += ',"' + row_data.related.type + '"'  ;
            concept_div += ',"' + row_data.related.cui  + '"'  ;
            
            concept_div += ',"' + row_data.related.name.replace("'", "&apos;") + '"'  ;
            
            concept_div += ',"' + row_data.subject.type + ":" + row_data.subject.cui + '"'  ; 
            concept_div += ',"' + row_data.subject.name.replace("'", "&apos;") + '"'  ;
            concept_div += ',"' + getSemtype(row_data.subject)  + '"'  ;
            
            /* implicit relation has a score */
            concept_div += ',"IR' + row_data.tuple_id.toString()+"."+row_data.subject.concept_id + '"' ;
            concept_div += ',"'   + score(row_data.score).toString() + '"' ;

            concept_div += ',"' + row_data.related.type + ":" + row_data.related.cui + '"'  ; 
            concept_div += ',"' + row_data.related.name.replace("'", "&apos;") + '"'  ;
            concept_div += ',"' + getSemtype(row_data.related)  + '"'  ;
            
            concept_div += '," "," "," "' ; /* ignored extra arguments */
            concept_div += ");'" ;
            concept_div += '>' + row_data.related.name + '</div>' ;
            
            /* alert("concept_div: "+concept_div ) ; */
            
            return concept_div ;
            
        } else {
            /* 'filter', sort' 'type' and undefined all just use the name string */
            return row_data.related.name;
        }
    } ;
}

function dragRelation( id, name ) {
    
    $( "#"+id ).draggable({
            containment: "document",
            cursor: "grabbing",
            cursorAt: { top: 23, left: 23 },
            helper: function( event ) {
                return '<div id="'+id+'" style="width: 50px; height: 50px; padding: 0.5em; float: left; margin: 0 10px 10px 0; z-index: 1;" >' + name + '</div>' ;
            }
        });
}

function dragRelationEvent( relation_type, row_div, row_data ) {
    
    var label ;
    /* Tweak the row id to make it unique to the given table,
       so as to avoid id clashes between relation tables.
       Not sure if this will break something or be innocuous 
    */
    var drid = relation_type + "_" + $(row_div).attr('id') ;
    $(row_div).attr('id',drid) ;
    
    if( relation_type == "implicit") {
        label = 
            row_data.subject.name.replace("'", "&apos;")+
            " " + row_data.score + " " + 
            row_data.related.name.replace("'", "&apos;") ;
        $(row_div).attr(
            "onmouseover",
            "dragRelation('" + drid + "','"+ label + "');"
        ) ;
        
        $(row_div).addClass("drag-relation") ;
        
        $(row_div).attr("subject_qualified_cui",      row_data.subject.type+":"+row_data.subject.cui) ;
        $(row_div).attr("subject_name",    row_data.subject.name.replace("'", "&apos;")) ;
        $(row_div).attr("subject_semtype", row_data.subject.semtype ) ;

        /* implicit relation has a score instead */
        $(row_div).attr("relation_label",score( row_data.score )) ;
        $(row_div).attr("relation_id",  "IR" + row_data.tuple_id.toString()+"."+row_data.subject.concept_id) ;
        
        $(row_div).attr("object_qualified_cui",      row_data.related.type+":"+row_data.related.cui) ;
        $(row_div).attr("object_name",    row_data.related.name.replace("'", "&apos;")) ;
        $(row_div).attr("object_semtype", row_data.related.semtype ) ;

    } else { /* Explicit or PMID relation? */
    
        label = 
            row_data.subject.name.replace("'", "&apos;") +
            " " + row_data.predicate + " " + 
            row_data.object.name.replace("'", "&apos;") ;
            
        $(row_div).attr(
            "onmouseover",
            "dragRelation('" + drid + "','"+ label + "');"
        ) ;
                
        $(row_div).addClass("drag-relation") ;

        $(row_div).attr("subject_qualified_cui",      row_data.subject.type+":"+row_data.subject.cui) ;
        $(row_div).attr("subject_name",    row_data.subject.name.replace("'", "&apos;")) ;
        $(row_div).attr("subject_semtype", row_data.subject.semtype ) ;
        
        $(row_div).attr("relation_id",  "ER" + row_data.predication_id.toString()) ;
        
        /* explicit relation has a name */
        $(row_div).attr("relation_label",row_data.predicate) ;
        
        $(row_div).attr("object_qualified_cui",      row_data.object.type+":"+row_data.object.cui) ;
        $(row_div).attr("object_name",    row_data.object.name.replace("'", "&apos;")) ;
        $(row_div).attr("object_semtype", row_data.object.semtype ) ;
    }
}

function evidenceRenderer() {
    return function ( row_data, type, val, meta ) {
        if (type == 'display') {

            /* rowDataDiagnostic("evidenceRenderer",row_data) ; */

            var evidence_count = parseInt(row_data.evidence_count) ;
            if ( ! isNaN(evidence_count) ) {
            
                var predicate = 
                        row_data.subject.name.replace("'", "&apos;")+
                        " "+row_data.predicate+" "+
                        row_data.object.name.replace("'", "&apos;") ;

                var id = 'DT_evidence_'+row_data.DT_RowData.pkey ;
                
                var evidence_div =  '<div id="'+id+'" class="btn btn-default cmbutton"' ;
                evidence_div += " onclick='clickEvidence(" ;
                evidence_div += row_data.predication_id  ;
                evidence_div += ',"' + predicate + '"' ;
                evidence_div += ");'>" + evidence_count.toString() + '</div>' ;
                
                /* alert("evidence_div: "+evidence_div ) ; */
                
                return evidence_div ;
                
            } else {
                return "" ;
            }
        } else {
          /* 'filter', 'sort', 'type' and 
             undefined are meaningless in 
             this context: just return blank */
          return "";
        }
    } ;
}

function clickPMID( pmid ) {

    $( '#pmid_result_table_title' ).empty().append("Relations for PMID: "+pmid) ;
    
    $( '#pmid_abstract_iframe' ).attr( "src", PUBMED_ROOT_URL + pmid ) ;
    
    pmid_table_args.query = pmid ;
    pmid_table.ajax.reload() ;
    
    /* Goto the PMID abstract tab? */
    $("#citation_tabs").tabs("option", "active", 1 ); 
}

function citationRenderer() {
    return function ( row_data, type, val, meta ) {
        if (type == 'display') {

            /* rowDataDiagnostic("citationRenderer",row_data) ; */

            var id = 'DT_pmid_'+row_data.DT_RowData.pkey ;
            
            var pmid_div =  '<p><div id="'+id+'" class="sentence"' ;
            /* pmid_div += ' title="PMID: '+row_data.pmid+'"'; */
            pmid_div += ' onclick="clickPMID(' ;
            pmid_div += row_data.pmid  ;
            pmid_div += ');"' ;
            pmid_div += '>' + row_data.sentence + '</div></p>' ;
            
            /* alert("pmid_div: "+pmid_div ) ; */
            
            return pmid_div ;
            
        } else {
          /* 'filter', sort', 'type' and undefined, just return the sentence */
          return row_data.sentence;
        }
    } ;
}

function score(raw_score) {
    var normalizedScore = 1000.0 * parseFloat(raw_score) ;
    normalizedScore = normalizedScore.toFixed(5) ;
    return normalizedScore ;
}

function clickCooccurrence( tuple_id, subject_concept_id, subject_name, related_name ) {

    /* alert( "clickCooccurrence( tuple: "+tuple_id + ", subject id: " + 
              subject_concept_id+ ", name: " + subject_name+ 
              ", related concept: " + related_name +")" ) ; */

    $( '#cooccurrence_result_table_title' ).empty().append("Linking Concept Co-Occurrence for '"+subject_name+"' and '"+related_name+"'") ;
    
    /* Use Concept A to Concept C tuple implicit relationship tuple_id as query identifier */
    cooccurrence_table_args.query = tuple_id.toString()+","+subject_concept_id.toString() ; 
    cooccurrence_table.ajax.reload() ;
    
    /* Goto the Co-occurrence data results tab? */
    $("#semantic_filter").hide() ;
    $("#concept_tables a[href='#cooccurrence_result_div']").tab("show") ;
    
}

function implicitScoreRenderer() {
    return function ( row_data, type, val, meta ) {
    
        /* alert("implicitScoreRenderer(row_data:"+JSON.stringify(row_data)+")") ; */
        
        var relation_score = score( row_data.score ) ;
        if (type == 'display') {

            var id = 'DT_implicit_score_'+row_data.DT_RowData.pkey ;
            
            var score_div =  '<div id="'+id+'" class="btn clickable"' ;
            score_div += " onclick='clickCooccurrence(" ;
            score_div += '"'  + row_data.tuple_id.toString() + '"' ;
            score_div += ',"' + row_data.subject.concept_id.toString() + '"' ;
            score_div += ',"' + row_data.subject.name.replace("'", "&apos;") + '"' ;
            score_div += ',"' + row_data.related.name.replace("'", "&apos;") + '"' ;
            score_div += ");'>" + relation_score + '</div>' ;
            
            /* alert("score_div: "+score_div ) ; */
            
            return score_div ;
            
        } else {
          /* 'filter', sort', 'type' and undefined, just return the sentence */
          return relation_score;
        } 
    } ;
}

function cooccurrenceScoreRenderer() {
    return function ( row_data, type, val, meta ) {
        var normalizedScore = parseFloat(row_data.b_concept.contribution) ;
        return normalizedScore.toFixed(5) ;
    } ;
}

function cooccurrencePMIDLinkRenderer(c1,c2) {
    return function ( row_data, type, val, meta ) {
        if (type == 'display') {

            /* rowDataDiagnostic("cooccurrencePMIDLinkRenderer",row_data) ; */

            var id = 'DT_pmid_'+row_data.DT_RowData.pkey ;
            
            /* Stub implementation */
            var cooccurrence_pmid_query = row_data[c1+'_concept'].name+"+AND+"+row_data[c2+'_concept'].name ;
            
            var pmid_div =  '<div id="'+id+'" class="btn clickable"' ;
            pmid_div += " onclick='clickPMID(" ;
            pmid_div += '"' + cooccurrence_pmid_query.replace("'", "&apos;") + '"';
            pmid_div += ");'>PubMed</div>" ;
            
            /* alert("pmid_div: "+pmid_div ) ; */
            
            return pmid_div ;
            
        } else {
          /* 'filter', sort', 'type' and undefined, just return the sentence */
          return "Link";
        }
    } ;}

function cooccurrrenceConceptRenderer() {
    return function ( row_data, type, val, meta ) {
        /* alert("cooccurrrenceConceptRenderer(row_data:"+JSON.stringify(row_data)+")") ; */
        if (type == 'display') {

            var el_class = "subject" ;
            var concept_div_id = 'DT_COR_relation_'+row_data.DT_RowData.pkey.replace(".", "_") ; 
            var concept_div =  '<div class="btn clickable '+el_class+'" id="' + concept_div_id + '"' ;

            concept_div += " onclick='clickConcept(" ;
            concept_div += '"cooccurrence"'  ;
            concept_div += ',"' + concept_div_id + '"'  ;
            concept_div += ',"' + implicit_table_args.query + '"'  ;
            concept_div += ',' + row_data.b_concept.concept_id  ;
            concept_div += ',"' + row_data.b_concept.type + '"'  ;
            concept_div += ',"' + row_data.b_concept.cui  + '"'  ;
 
            concept_div += ',"' + row_data.b_concept.name.replace("'", "&apos;") + '"'  ;
            
            concept_div += ',"' + row_data.a_concept.type + ":" + row_data.a_concept.cui + '"'  ; 
            concept_div += ',"' + row_data.a_concept.name.replace("'", "&apos;") + '"'  ;
            concept_div += ',"' + getSemtype(row_data.a_concept) + '"'  ; 
            
            /* Cooccurrence Relation has a contribution score */
            concept_div += ',"CR' + row_data.tuple_id.toString()+"."+row_data.b_concept.concept_id + '"' ;
            concept_div += ',"' + row_data.b_concept.contribution.toString() + '"' ; 
            
            concept_div += ',"' + row_data.b_concept.type + ":" + row_data.b_concept.cui + '"'  ; 
            concept_div += ',"' + row_data.b_concept.name.replace("'", "&apos;") + '"'  ;
            concept_div += ',"' + getSemtype(row_data.b_concept) + '"'  ; 
            
            /* Propagate C concept details here, as 'extra' argument to clickConcept() */
            concept_div += ',"' + row_data.c_concept.type + ":" + row_data.c_concept.cui + '"'  ; 
            concept_div += ',"' + row_data.c_concept.name.replace("'", "&apos;") + '"'  ;
            concept_div += ',"' + getSemtype(row_data.c_concept) + '"'  ; 
            
            concept_div += ");'" ;
            concept_div += '>' + row_data.b_concept.name + '</div>' ;
            
            /* alert("concept_div: "+concept_div ) ; */
            
            return concept_div ;
            
        } else {
            /* 'filter', 'sort', 'type' and undefined all just use the name string */
            return row_data.b_concept.name ;
        }
    } ;
}

function drawTable(mode) {
    /* Note that the number of column 'thead' and 'tfoot' should 
       be strictly equal to the number of 'data' columns specified
       for the 'columns' variable set in the ready() method which follows */
    var output =  '<h4 class="text-center data_table_subtitle"><div id="'+mode+'_result_table_title"></div></h4>\n';
    output +=  "<table id='"+mode+"_result_table' class='compact display table-condensed desktop-table'>" ;
    var rowspec ;
    if( mode == 'concept' ) {
        rowspec  = "<th>Concept</th>" ;
    } else if( mode == 'explicit' ) {
        rowspec  = "<th>Subject</th>"+
                   "<th>Relation</th>"+
                   "<th>Object</th>"+
                   "<th>Evidence</th>" ;
    } else if( mode=='implicit' ) {
        rowspec  = "<th>Related Concept</th>"+
                   "<th>Score</th>" ;
    } else if( mode=='cooccurrence' ) {
        rowspec  = "<th>Linking (B) Concept</th>"+
                   '<th class="sorting_disabled">Contribution</th>'+
                   "<th>A-B</th>"+
                   "<th>B-C</th>" ;
    }  else if( mode=='evidence' ) {
        rowspec  = "<th>Sentence</th>"+
                   "<th>PMID</th>" ;
    } else if(mode=='pmid') {
        rowspec = "<th>Subject</th>"+
                   "<th>Relation</th>"+
                   "<th>Object</th>"+
                   "<th>Other Citations</th>" ;
    } else if(mode=='concept') {
        rowspec = "<th>Concept</th>" ;
    } else {
        return 'ERROR: Unknown dashboard query mode: '+ mode ;
    }
    output += "<thead><tr>"+rowspec+"</tr></thead>" ;
    output += "</table>" ;
    return output ;
}


var initConceptTables = function() {
    alert("Stub initConceptTables") ;
} ;

$(document).ready( function () {

    /* Concept Search */
    $("form#concept_search").submit( function( event ){

        // take input value and turn into url for colorbox to display
        var concept_search_string   = $("form#concept_search input#concept_search_string").val();
        
        concept_search_string = concept_search_string.trim() ;
        if(concept_search_string.length===0) { 
            // alert("Please type a non-blank string into the search box!");

        } else {
            
            /* Encode the search string and URI */
            /* Assuming that search strings won't have plus signs, of course! */
            concept_search_string = concept_search_string.replace(" ","+") ; 
            var query_url = encodeURI( "/query/search/concept?query=" + concept_search_string ) ;
    
            /* alert("Concept Search URL: "+query_url) ; */
            
            // initialize colorbox
            $.colorbox({
                opacity:0.5,
                width:"75%",
                height:"90%",
                href: query_url,
                onComplete:function(){
                    initConceptTables() ;
                },
            });
        }
        // prevent usage of 'action' parameter of form (i.e. stop a redirect from occurring)
        event.preventDefault();

    });

    $('#current_concept').append("<small>Viewing Concepts Related to:</small> "+rootQueryContext.concept_name) ;

    /* SemMedDb Explicit Search Results Table*/
    $('#explicit_result_div').append( drawTable("explicit") );

    var explicit_columns = [] ;
    var explicit_orders = [];
    
    explicit_columns.push( { "data": null , "render": explicitConceptRenderer("main","subject"), "orderable": false } ) ; 
    explicit_columns.push( { "data": "predicate", "orderable": false } ) ; 
    explicit_columns.push( { "data": null , "render": explicitConceptRenderer("main","object"), "orderable": false } ) ;
    explicit_columns.push( { "data": null , "render": evidenceRenderer(), "orderable": false, "searchable": false } ) ; 

    explicit_table = $('#explicit_result_table')
        .on( 'processing.dt', function ( e, settings, processing ) {
            $('#explicit_processing').css( 'display', processing ? 'inline' : 'none' );
        } ).DataTable(  {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": '/query/data',
            "type": 'POST',
            "data": explicit_data_query, 
        },
        "columns"    : explicit_columns,
        "order"      : explicit_orders,
        "scrollY": 180,
        "rowCallback": function( row_div, row_data, index ) {
            if(row_data.tophit == 1) {
                $(row_div).addClass('tophit');
            }
            dragRelationEvent( "explicit", row_div, row_data ) ;
        },
        "dom":"frtip",
        "oLanguage": {
            "sSearch": "Filter By Text:"
        },
    } );

    explicit_table.$('input, select').serialize();

    // repeat helper
    String.prototype.repeat = function( num )
    {
        return new Array( num + 1 ).join( this );
    } ;
    
    var semantic_filter_menu = (function(){
        var temp = "";
        temp += "<form>";
        temp += "<fieldset style='vertical-align:middle'>";
        temp += "<p>";
        temp += "&nbsp;".repeat(40);
        temp += "<select id='layout-menu'>";
        temp += "<option value='-ncf' selected>";
        temp += "No Type Filter";
        temp += "</option>";
        temp += "<option value='dsyn'>";
        temp += "Diseases";
        temp += "</option>";
        temp += "<option value='gngm'>";
        temp += "Genes " ;
        temp += "</option>";
        temp += "<option value='phsu'>";
        temp += "Drugs";
        temp += "</option>";
        temp += "<option value='-adv'>";
        temp += "Other..";
        temp += "</option>";
        temp += "</select>";
        temp += "</fieldset>";
        temp += "</form>";
        return temp;
    })();
    
    $("#semantic_filter").css('position','left').append( semantic_filter_menu );
    $("#semantic_filter select#layout-menu").selectmenu({
            select: function( event, data ) {
                if( data.item.value === "-adv" ){

                    // bring up color box - dom reloads upon change
                    $.colorbox( {
                        href:"/query/search/filter",
                        opacity:0.3,
                        width: "25%",
                        height:"90%",

                        onOpen:function(){
                            current_type_filter = explicit_table_args.type_filter ;
                        },
                    } );

                } else if( data.item.value === "-ncf" ){
                
                    /* No Categorical Filtering */
                    explicit_table_args.type_filter = "" ;
                    explicit_table.ajax.reload() ;
                               
                } else {

                    explicit_table_args.type_filter = data.item.value ;
                    explicit_table.ajax.reload() ;

                }
            }
        }).css("width","110px");

    /* Implicitome Implicit Search Results Table*/
    $('#implicit_result_div').append( drawTable("implicit") ) ;
    
    var implicit_columns = [] ;
    var implicit_orders  = [] ;
    implicit_columns.push( { "data": null , "render": implicitConceptRenderer(), "orderable": false } ) ; 
    implicit_columns.push( { "data": null , "render": implicitScoreRenderer()} ) ; 

    /* order results initially from high to low score */
    implicit_orders.push([1,'desc']) ;  
    
    implicit_table = $('#implicit_result_table')
        .on( 'processing.dt', function ( e, settings, processing ) {
            $('#implicit_processing').css( 'display', processing ? 'inline' : 'none' );
        } ).DataTable(  {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": '/query/data',
            "type": 'POST',
            "data": implicit_data_query,
        },
        "columns"    : implicit_columns,
        "order"      : implicit_orders,
        "scrollY": 180,
        "rowCallback": function( row_div, row_data, index ) {
            dragRelationEvent( "implicit", row_div, row_data ) ;
        },
        "dom":"frtip",
        "oLanguage": {
            "sSearch": "Filter By Text:"
        },
    } ); 

    implicit_table.$('input, select').serialize();

    /* Implicitome Co-occurrence Search Results Table*/
    $('#cooccurrence_result_div').append( drawTable("cooccurrence") ) ;
    
    var cooccurrence_columns = [] ;
    var cooccurrence_orders  = [] ;
    cooccurrence_columns.push( { "data": null , "render": cooccurrrenceConceptRenderer(),        "orderable": false } ) ; 
    cooccurrence_columns.push( { "data": null , "render": cooccurrenceScoreRenderer(),           "orderable": false } ) ; 
    cooccurrence_columns.push( { "data": null , "render": cooccurrencePMIDLinkRenderer("a","b"), "orderable": false } ) ; 
    cooccurrence_columns.push( { "data": null , "render": cooccurrencePMIDLinkRenderer("b","c"), "orderable": false } ) ; 

    /* order results initially from high to low score */
    cooccurrence_orders.push([1,'desc']) ;  
    
    cooccurrence_table = $('#cooccurrence_result_table')
        .on( 'processing.dt', function ( e, settings, processing ) {
            $('#cooccurrence_processing').css( 'display', processing ? 'inline' : 'none' );
        } ).DataTable(  {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": '/query/data',
            "type": 'POST',
            "data": cooccurrence_data_query,
        },
        "columns"    : cooccurrence_columns,
        "order"      : cooccurrence_orders,
        "scrollY": 180,
        "searching": false,  /* disable/remove the text Search box? */
        "paging" : false,    /* don't need paging for only 5 elements */
    } ); 

    cooccurrence_table.$('input, select').serialize();

    /* SemMedDb PMID citations "Evidence" Search Results Table*/
    $('#evidence_result_div').append( drawTable("evidence") ) ;
    
    var evidence_columns = [ 
        { "data": null , "render": citationRenderer() },
        { "data": "pmid"  },
    ] ;
    var evidence_orders  = [ [0,'asc'] ] ; /* order results ascending alphabetically  */
    
    evidence_table = $('#evidence_result_table')
        .on( 'processing.dt', function ( e, settings, processing ) {
            $('#evidence_processing').css( 'display', processing ? 'inline' : 'none' );
        } ).DataTable(  {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": '/query/data',
            "type": 'POST',
            "data": evidence_data_query,
        },
        "columns"    : evidence_columns,
        "order"      : evidence_orders,
        "scrollY": 180,
        "oLanguage": {
            "sSearch": "Filter By Text:"
        },
    } );

    evidence_table.$('input, select').serialize();
    
    $( "#citation_tabs" ).tabs();

    /* SemMedDb PMID citations "Evidence" Search Results Table*/
    $('#pmid_result_div').append( drawTable("pmid") ) ;
    
    var pmid_columns = [] ;
    pmid_columns.push( { "data": null , "render": explicitConceptRenderer("pmid","subject"), "orderable": false } ) ; 
    pmid_columns.push( { "data": "predicate", "orderable": false } ) ; 
    pmid_columns.push( { "data": null , "render": explicitConceptRenderer("pmid","object"), "orderable": false } ) ; 
    pmid_columns.push( { "data": null , "render": evidenceRenderer(), "orderable": false, "searchable": false } ) ; 
    
    $('#pmid_result_div').append( drawTable("pmid") ) ;
    
    pmid_table = $('#pmid_result_table')
        .on( 'processing.dt', function ( e, settings, processing ) {
            $('#pmid_relations_processing').css( 'display', processing ? 'inline' : 'none' );
        } ).DataTable(  {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": '/query/data',
            "type": 'POST',
            "data": pmid_data_query,
        },
        "columns"    : pmid_columns,
        "scrollY": 180,
        "rowCallback": function( row_div, row_data, index ) {
            dragRelationEvent( "pmid", row_div, row_data ) ;
        },
        "oLanguage": {
            "sSearch": "Filter By Text:"
        },
    } );

    pmid_table.$('input, select').serialize();
    
    $( "#pmid_search_submit" ).click(function( event ) {
    
        var pmid = $( "#pmid_search_string" ).val() ;
        
        if(pmid.trim().length>0) {
            clickPMID( pmid ) ;            
        }
    });

    /* Goto the Explicit results tab? */
    $("#discovery_data_tabs").tabs("option", "active", 0 ); 

} );

function redrawMap(concept_map) {
    
    if ( $("#cmlayout").find( ":selected" ).val( ) !== "preset" )  {
        var layout = concept_map.makeLayout({ name: currentCMLayout });
        layout.run();
        concept_map.resize();
    } 
}

function addNodeToMap( concept_id,  concept_name, semtype ) {

    var concept_map = $('#concept_map').cytoscape('get');

    /* Sanity check: don't add a node more than once? */
    var existingNodes =
            concept_map.nodes( '[id="'+concept_id+'"]' );
    if(existingNodes.size()>0) {
        return ;
    }

    var semtype_class = " " ;
    if( !( semtype===null || semtype.length===0 )) {
        
        semtype_class = semtype_class_catalog[semtype] ;
        if( semtype_class === null ) {
            semtype_class = " " ;
        }
    }

    /* Some concept names have HTML escaped 
       apostrophe characters - put in literals */
    concept_name = concept_name.replace("&apos;","'") ;
    
    var new_node = [
        {
            group: "nodes", 
            data: { 
                id   : concept_id,
                name : concept_name,
             },
            position : { 
                    "x": 100, 
                    "y": 100 
             },
            classes: semtype_class,
        },
    ] ;

    concept_map.add(new_node) ;

    redrawMap(concept_map) ;
}

function addRelationToMap( 
            subject_qualified_cui,  subject_name, subject_semtype,
            relation_id, relation_label,
            object_qualified_cui,   object_name, object_semtype
) {
    console.log(relation_id);

    var concept_map = $('#concept_map').cytoscape('get');
    
    /* Sanity check: don't add an edge more than once? */
    var existingEdges = 
            concept_map.edges(
                '[id="'+relation_id+'"]'+
                '[source="'+subject_qualified_cui+'"]'+
                '[target="'+object_qualified_cui+'"]'
            );
    if(existingEdges.size()>0) {
        return ;
    }

    var subject_semtype_class = semtype_class_catalog[subject_semtype] ;
    if( subject_semtype_class === null ) {
        subject_semtype_class = ' ' ;
    }

    var object_semtype_class = semtype_class_catalog[object_semtype] ;
    if( object_semtype_class === null ) {
        object_semtype_class = ' ' ;
    }

    /* Some concept names have HTML escaped 
       apostrophe characters - put in literals */
    subject_name = subject_name.replace("&apos;","'") ;
    object_name  = object_name.replace("&apos;","'") ;

    var new_relation = [
        {
            group: "nodes", 
            data: { 
                "id"   : subject_qualified_cui,
                "name" : subject_name,
             },
            position : { 
                    "x": 50, 
                    "y": 50 
             },
             classes : subject_semtype_class
        },
        { 
            group: "nodes", 
            data: { 
                "id"   : object_qualified_cui,
                "name" : object_name,
             },
            position : { 
                    "x": 200, 
                    "y": 200 
             },
             classes : object_semtype_class
       },
        {
            group: "edges", 
            data: { 
                "id"    : relation_id,
                 
                /* the edge 'name' may be an explicit 
                   relation name, an implicit score
                   or a concurrence contribution score */
                "name"  : relation_label,
                
                "source": subject_qualified_cui,
                "source_name": subject_name,
                "target": object_qualified_cui,
                "target_name": object_name, 
            }
        }
    ] ;
    
    concept_map.add(new_relation) ;
    
    redrawMap(concept_map) ;

}

/* Overriding stub function variable defined in concept_map.js */
reloadData = function( cui, concept_name ) {

    /* alert("reloadData(calling implicit table with cui: "+cui+" , name: "+concept_name+")") ; */
    
    $('#current_concept').empty().append("<small>Viewing Concepts Related to:</small> " +concept_name) ;

    /* refresh the type filter... */
    explicit_table_args.type_filter = current_type_filter ; 
    explicit_table_args.query = cui ;
    explicit_table.ajax.reload() ;
    
    implicit_table_args.query = cui ;
    implicit_table.ajax.reload() ;

    /* empty the co-occurrence table */
    cooccurrence_table_args.query = "0,0" ; 
    cooccurrence_table.ajax.reload() ;

} ;

$("#contextTable").hide();
var tableActions = {

    add: function( self ){
    
        $(".subject[id='"+$("#contextTable").data("concept_div_id")+"']").qtip('destroy');

        var context                 = $("#contextTable").data("context");
        var subject_qualified_cui      = $("#contextTable").data("subject_qualified_cui");
        var subject_concept_name    = $("#contextTable").data("subject_concept_name");
        var subject_concept_semtype = $("#contextTable").data("subject_concept_semtype");
        var relation_id             = $("#contextTable").data("relation_id");
        var relation_label          = $("#contextTable").data("relation_label");
        var related_qualified_cui      = $("#contextTable").data("related_qualified_cui");
        var related_concept_name    = $("#contextTable").data("related_concept_name");
        var related_concept_semtype = $("#contextTable").data("related_concept_semtype");
        
        /* extra information with co-occurrence relations */
        var c_qualified_cui      = $("#contextTable").data("c_qualified_cui");
        var c_concept_name    = $("#contextTable").data("c_concept_name");
        var c_concept_semtype = $("#contextTable").data("c_concept_semtype");


        if(context === "cooccurrence") {
        
            /* Also add cooccurrence B - C relations to the map? */
        
            relation_label = "Mentioned with" ;
            addRelationToMap(
                related_qualified_cui, related_concept_name, related_concept_semtype,
                relation_id+"_BC",  relation_label,
                c_qualified_cui,       c_concept_name,       c_concept_semtype
            );
            
            /* A-B relation_id needs to be unique? */
            relation_id += "_AB" ;
        }
        
        /* Primary explicit relation, implicit relation or A-B cooccurrence concept */
        // TODO - use this for issue #110 extension
        addRelationToMap(
            subject_qualified_cui, subject_concept_name, subject_concept_semtype,
            relation_id,        relation_label,
            related_qualified_cui, related_concept_name, related_concept_semtype
        );
    },
    
    explore: function( self ){
        $(".subject[id="+$("#contextTable").data("concept_div_id")+"]").qtip('destroy');

        var selected_concept_cui  = $("#contextTable").data("selected_concept_cui");
        var selected_concept_name = $("#contextTable").data("selected_concept_name");

        reloadData( selected_concept_cui, selected_concept_name );
        
        /* Display the Explicit data results tab? */
        $("#discovery_data_tabs").tabs("option", "active", 0 );
    },
    
    getConceptInfo: function( rel_data ){

        /* TODO There has to be a better way to 
           structure this... preferably using objects */
           
        var context                 = rel_data[0];
        
        var concept_div_id          = rel_data[1];
        var current_concept_cui     = rel_data[2];
        
        var selected_concept_id     = rel_data[3];
        var selected_concept_type   = rel_data[4];
        var selected_concept_cui    = rel_data[5];
        var selected_concept_name   = rel_data[6];
        
        var subject_qualified_cui      = rel_data[7];
        var subject_concept_name    = rel_data[8];
        var subject_concept_semtype = rel_data[9];
        
        var relation_id             = rel_data[10];
        var relation_label          = rel_data[11];
        
        var related_qualified_cui      = rel_data[12];
        var related_concept_name    = rel_data[13];
        var related_concept_semtype = rel_data[14];
        
        var c_qualified_cui            = rel_data[15];
        var c_concept_name          = rel_data[16];
        var c_concept_semtype       = rel_data[17];

        var concept_table_location = $(".subject[id="+concept_div_id+"]");

        // TODO: This is a pretty gnarly method... most of the legwork should be done server-side.

        $.when(

            // $.when takes promise chains, so the following lambda needs to return promises in all cases.
            ( function(){
                if( selected_concept_type === "ENTREZ" ){
                    // we first get the ID of our target concept. Then we use the ID to find the concept information itself.
                    return $.getJSON( "http://mygene.info/v2/query?q=symbol%3A"+selected_concept_name+"&species=human" );
                } else if ( selected_concept_type === "META" ){
                    return $.when(null);
                }
            } )()

        ).pipe( function( data ){

            if( selected_concept_type === "ENTREZ"){

                if( data.hits.length === 0 ) {
                    /*
                       We default to just wanting to display buttons. Thus we fulfill the promise, but with nothing.
                       PROBLEM - the data package is going to contain the "valid" results of the 
                       above $.getJSON query; but then the logic below will think it should put a table up...
                    */
                    return $.when(null);

                } else {

                    // otherwise we have it
                    // Look for annotation/concept information
                    var hit = data.hits[0];
                    var table;
                    return $.ajax({
                            method:"GET",
                            url: "http://mygene.info/v2/gene/"+hit._id,
                            dataType: "jsonp",
                            data:( function(){
                                var fieldFilter  = ["symbol","map_location","entrezgene","HGNC","alias","type_of_gene","summary","unigene","uniprot"] ; // subtract uniprot
                                return { fields: fieldFilter.join(',') };
                            })()
                        }) ;
                }

            } else if ( selected_concept_type === "META" ) {

                var patt = /\d+/ ;
                predication_id = patt.exec(relation_id);
                var conceptDetailsURL = "/query/"+context+"/concept/details/"+predication_id+"/"+selected_concept_id ;
                return  $.getJSON( conceptDetailsURL );

            }

        }).then( function( data ){

            // Strategy is to modify the data while we have it in a well-formed state, using jQuery to modify a DOM element instead of attempting to pass anything out of this chain
            // That way we can move most of the behavior into the ajax call and not deal with the difficulties of porting it out from a deferred
            $("#contextTable table").empty();

            // if no data we add nothing to the table and just leave the buttons to work.
            if( data && !data.Result ){
                $.each( data, function( i, nval ){

                    var key;
                    var val;
                    if( selected_concept_type === "META" ){
                        var field = nval;
                        key = field[0];
                        val = field[1] ? field[1] : false;
                    } else if ( selected_concept_type === "ENTREZ"){
                        key = i;
                        val = nval;
                        if( key == "_id") { val = false; }
                        if( key == "entrezgene" ) { key = "Entrez_ID" ; }
                    }

                    // If we don't have a val, just move on.
                    if ( !val ) {
                        // "return true" is the $.each equivalent of "continue" for real (de-abstracted) loops
                        return true;
                    }

                    // perform capitalization/"titlizing'
                    var keyTitle = [];
                    var acronym = [ "id", "cui", "sab", "omim" ];
                    var conjunct = ["of", "and"];
                    var word;
                    $.each( key.split("_"), function(i,v) {
                        if( acronym.indexOf( v ) > -1  ) {
                            word = v.toUpperCase();
                        } else if( conjunct.indexOf( v ) > -1 ){
                            word = v.toLowerCase();
                        } else {
                            word = v.capitalize();
                        }
                        keyTitle.push( word );
                    });
                    keyTitle = keyTitle.join(" ");

                    // Handle semtype mapping special case
                    if( key === "semtype" ) {

                        /* override previous work */
                        keyTitle = "Semantic Type";
                        
                        /* get the full semantic type name */
                        val = semcode2name[val] ;
                    }
                    
                    if( key === "tui" && val.length>0 ) {
                
                        /* override previous work */
                        keyTitle = "Semantic Type";

                        val = val[0] ;
                        val = subtype2semcode[ val ] ;

                        /* Convert to the full semantic type name */
                        val = semcode2name[val] ;
                    }

                    if( key === "uniprot" ){

                        var uniprot = [];
                        $.each( val, function( k, v ) {
                            uniprot.push( k+v );
                        });
                        /*
                           uniprot produces a string that was embedded with items, so has to be 
                           turned into a string first and split before it can be expanded again
                        */
                        val = uniprot.join(", ").split(",").join(", "); 
                    }

                    var keyString = keyTitle; // keyTitle.join(" ");
                    var valString = val;

                    $("#contextTable table").append(
                        "<tr>"+
                            "<th>"+
                            keyString+
                            "</th>"+
                            "<td>"+
                            valString+
                            "</td>"+
                        "</tr>"
                    ) ;

                } );

                // style the new elements
                $("#contextTable table th")
                    .css("white-space", "nowrap");
                $("#contextTable table td")
                    .css("word-wrap", "break-all");
                $("#contextTable table *")
                    .css("border", "3px solid transparent");

            }

            /*
            $(".qtip").css("max-width", "");
            */

            concept_table_location.qtip({
                content:{
                    text: $("#contextTable").html(),
                    button:'true'
                },
                show:{
                    event:"click",
                    ready:"true",
                    solo:"true"
                },
                hide:{
                    target:$('#contexTable button'),
                    event:'click unfocus scroll'
                },
                style: { 
                    classes: 'qtip-green'
                }
            });

        });
    }
} ;

// Helper function
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

function clickConcept(

            context,             
            concept_div_id, current_concept_cui,
            selected_concept_id, selected_concept_type, selected_concept_cui, selected_concept_name, 
            subject_qualified_cui,  subject_concept_name, subject_concept_semtype,
            relation_id,         relation_label,
            related_qualified_cui,  related_concept_name, related_concept_semtype,
            
            /* Extra arguments for co-occurrence context only */
            c_qualified_cui,  c_concept_name, c_concept_semtype
        ) {

    /* 
    
    1) check if id clicked corresponds to the
       current query cui for the table.
       
       Note: query should be visible in the global scope?

    2) The id clicked is NOT the original query concept,
       then assume that the user wants to set the query
       concept to the selected new concept (i.e. could be a gene).
       In that case:

          a) Reset the corresponding query variable
             to the new concept cui

          b) Update the explicit and implicit
             data tables with the new concept

          c) Add this concept as a new node in the concept map
             adding the relation as a new edge
    */
    if( selected_concept_cui == current_concept_cui ) {
        // hide buttons
        $("#contextTable div[name='buttons']").hide();
    } else {
        // show buttons
        $("#contextTable div[name='buttons']").show();
    }

    /*
        We use jquery data caching system to pass property values to children of the #contextTable div in the DOM
        Specifically we want buttons to work with these data values through the passing of "this".
        using #contextTable as our data repo ensures ease of clearability and use.
    */

    /* Clear data before addition of new data */
    $("#contextTable").removeData();
    
    $("#contextTable").data("context", context);
    $("#contextTable").data("concept_div_id", concept_div_id);
    $("#contextTable").data("selected_concept_id",   selected_concept_id);
    $("#contextTable").data("selected_concept_type", selected_concept_type);
    $("#contextTable").data("selected_concept_cui", selected_concept_cui);
    $("#contextTable").data("selected_concept_name", selected_concept_name);
    $("#contextTable").data("subject_qualified_cui", subject_qualified_cui);
    $("#contextTable").data("subject_concept_name", subject_concept_name);
    $("#contextTable").data("subject_concept_semtype", subject_concept_semtype);
    $("#contextTable").data("relation_id", relation_id);
    $("#contextTable").data("relation_label", relation_label);
    $("#contextTable").data("related_qualified_cui", related_qualified_cui);
    $("#contextTable").data("related_concept_name", related_concept_name);
    $("#contextTable").data("related_concept_semtype", related_concept_semtype);
    
    $("#contextTable").data("c_qualified_cui", c_qualified_cui);
    $("#contextTable").data("c_concept_name", c_concept_name);
    $("#contextTable").data("c_concept_semtype", c_concept_semtype);

    $(".subject").qtip("destroy");

    tableActions.getConceptInfo( arguments );

    //Goto the Explicit data results tab?
    //$("#discovery_data_tabs").tabs("option", "active", 0 );

}

var clickEvidence = function( predication_id, predication ) {

    $( '#evidence_result_table_title' ).empty().append(predication) ;
    evidence_table_args.query = predication_id ;
    evidence_table.ajax.reload() ;

    $('#evidence_tables a[href="#evidence_result_div"]').tab('show') ;

} ;

var PUBMED_ROOT_URL = "http://www.ncbi.nlm.nih.gov/pubmed/?term=" ;
function clickPMID( pmid ) {

    $( '#pmid_result_table_title' ).empty().append("Relations for PMID: "+pmid) ;
    
    $( '#pmid_search_string' ).attr( "value", pmid ) ;
    
    $( '#pmid_abstract_iframe' ).attr( "src", PUBMED_ROOT_URL + pmid ) ;
    
    pmid_table_args.query = pmid ;
    pmid_table.ajax.reload() ;

    $('#evidence_tables a[href="#pmid_abstract_div"]').tab('show') ;

}

$(function() {

    $( '#concept_map' )
        .droppable({
            accept: function(d) { return true; },
            drop: function( event, ui ) {
                var draggable = ui.draggable;
                var subject_qualified_cui = draggable.attr('subject_qualified_cui') ;
                var subject_name    = draggable.attr('subject_name') ;
                var subject_semtype = draggable.attr('subject_semtype') ;
                var relation_id     = draggable.attr('relation_id') ;
                var relation_label  = draggable.attr('relation_label') ;
                var object_qualified_cui = draggable.attr('object_qualified_cui') ;
                var object_name     = draggable.attr('object_name') ;
                var object_semtype  = draggable.attr('object_semtype') ;
                
                /* alert( 'The Relation(' + 
                        subject_id + ',' + 
                        subject_name + ',' + 
                        relation_id + ',' + 
                        relation_label + ',' + 
                        object_id + ',' + 
                        object_name + ',' + 
                        ') dropped onto me!' 
                );
                */
                
                addRelationToMap( 
                    subject_qualified_cui,  subject_name, subject_semtype,
                    relation_id, relation_label, 
                    object_qualified_cui,   object_name, object_semtype
                ) ;

            }
        });
});

function errorHandler(evt) {
    switch(evt.target.error.code) {
      case evt.target.error.NOT_FOUND_ERR:
        alert('File Not Found!');
        break;
      case evt.target.error.NOT_READABLE_ERR:
        alert('File is not readable');
        break;
      case evt.target.error.ABORT_ERR:
        break; // noop
      default:
        alert('An error occurred reading this file.');
    }
}

function mapFileloaded(evt) {
    /* alert("Map File Loaded Successfully"); */
    var gbk_file_data = evt.target.result;
    /* alert(gbk_file_data) ; */
    $('#gbk_file_data').attr("value",gbk_file_data) ;
    $('#loadMapForm').submit() ;
}

function handleMapFileSelect(evt) {

  var inputfile = evt.target.files[0];
 
  var reader = new FileReader();
  reader.onerror = errorHandler;
  reader.onload = mapFileloaded;
  reader.readAsText(inputfile, "UTF-8");
}


$(function() {

    $( "#discovery_data_tabs" ).tabs( "option", "active", 0 );
    $( "#citation_tabs" ).tabs( "option", "active", 0 );
    
    document.getElementById('load_map_file_name').addEventListener('change', handleMapFileSelect, false);
});


//////////////////////////
/* PAGE LENGTH CONTROLS */
//////////////////////////

$(function() {

    var targetTableId = $("#concept_tables > .tab-content > .active").attr("id");

    var pgln;
    function setPgLn( newPgLn ) {
        pgln = newPgLn;
    }
    if ( targetTableId === "explicit_result_div" ) {
        setPgLn( explicit_table.page.len().toString() );
    }
    if ( targetTableId === "implicit_result_div" ) {
        setPgLn( implicit_table.page.len().toString() );
    }
    if ( targetTableId === "cooccurrence_result_div" ) {
        setPgLn( cooccurrence_table.page.len().toString() );

    }
    // an alternate strategy would involve cancelling event propogation. Here we're going to do a manual edit.
    $("#concept_tables > .nav-tabs > .pgln > .btn-group > .active").removeClass("active");
    $("#concept_tables > .nav-tabs > .pgln > .btn-group > #"+pgln ).addClass("active");

}) ;

// detect if ".active" changes on table.
    // we can't monitor the table event directly, but clicking on tabs is reasonably associated with the change of the table.
$("#concept_tables > .nav-tabs > li > a").click( function( evt ) {

    var targetTableId = evt.target.href.split("#")[1];
    
    // we must check if we need to update the controls to reflect the state of the
    // we must do this without triggering the "change" event below.
        // detect diff.
        // change controls to reflect a diff

    // get page length value
    var pgln;
    function setPgLn( newPgLn ) {
        pgln = newPgLn;
    }
    if ( targetTableId === "explicit_result_div" ) {
        $("#layout-menu-button").show() ;
        setPgLn( explicit_table.page.len().toString() );
    }
    if ( targetTableId === "implicit_result_div" ) {
        $("#layout-menu-button").hide() ;
        setPgLn( implicit_table.page.len().toString() );
    }
    if ( targetTableId === "cooccurrence_result_div" ) {
        $("#layout-menu-button").hide() ;
        setPgLn( cooccurrence_table.page.len().toString() );
    }

    // current button is
    var activePageLenButtonId = $("#concept_tables > .nav-tabs > .pgln > .btn-group > .active").attr("id");

    // change controls
        // set controls to pgln
            // ok now we get really close to the button change producing a loop.
            // we avoid submitting the input form by changing the class without clicking the button.
                // if the class is separate from the button trigger, we're good.

    if( pgln != activePageLenButtonId ){
        // an alternate strategy would involve cancelling event propogation. Here we're going to do a manual edit.
        $("#concept_tables > .nav-tabs > .pgln > .btn-group > .active").removeClass("active");
        $("#concept_tables > .nav-tabs > .pgln > .btn-group > #"+pgln ).addClass("active");
    }

});

// detect if "show page" radio button changes
$(".pgln input[name='options']").change( function( evt ) {

    // take the updated value and make it our own.
    var pgln  = evt.target.value;

    // select children of #concept_tables and see which one is active
    var targetTableId = $("#concept_tables > .tab-content > .active").attr("id");

    // TODO: I think this is the current point of failure.
    if ( targetTableId === "explicit_result_div" ) {
        explicit_table.page.len( pgln ) ;
        explicit_table.draw() ;
    }
    if ( targetTableId === "implicit_result_div" ) {
        implicit_table.page.len( pgln );
        implicit_table.draw() ;
    }
    if ( targetTableId === "cooccurrence_result_div" ) {
        cooccurrence_table.page.len( pgln ) ;
        cooccurrence_table.draw() ;
    }
});

/* 
  RefreshPage: Prompts a save if elements exist, 
  just refreshes the page if this isn't the case.
 */

var refreshPage = function( refreshingButton ){
    // check if graph has items
    // if items, then savePrompt
        // TODO: several functions use $("#concept_map").cytoscape("get")... looks like a refactoring opportunity.
    if( $('#concept_map').cytoscape('get').nodes().size() > 0 ){
        savePrompt( refreshingButton );
    } else {
        if( refreshingButton.id === "newMap" ) {
            window.location.href="/";
        }
        if( refreshingButton.id === "demoMap" ) {
            window.location.href="/query/demo";
        }
    }
} ;

// TODO: initialize the hidden aspects of the templates in one go... or will it work as
$( "#savePrompt" ).hide() ;
$( "#savePromptPrompt" ).hide() ;

function savePrompt( triggerEl ){

    $( "#savePrompt" ).qtip({
        content:{
            text:$("#savePrompt").html()
        },
        show:{
            ready:true,
            modal:true
        },
        hide:'unfocus',
        position:{
            target: $("#newMap"),
            corner: {
               target: 'bottomRight',
               tooltip: 'leftTop'
            }
        },
        style: {
            classes: 'qtip-green'
        }
    });

}

/* Initialize SavePromptLayout
 * Used to interrupt any action that will cause the graph to be lost (mainly redirections).
 */
var savePromptActions = {
    // save
    save:function( self ){

        /* 
           hides then removes qtip (there should 
           be a qtip if these actions are displayed)
        */
        $( "#savePrompt" ).qtip("destroy") ;
        showSaveMapDialog();

    },
    // continue is reserved; abbreviate as "cont"
    cont:function( self ){

        /* hides then removes qtip (there should be a 
           qtip if these actions are displayed) 
         */
        $("#savePrompt").qtip("destroy");
        window.location.href = "/";
    },
    // cancel
    cancel:function( self ){
        /* 
           Hides then removes qtip (there should 
           be a qtip if these actions are displayed)
         */
        $("#savePrompt").qtip("destroy");

    }
};


var showSaveMapDialog = function() {
    $("#input > input").val( rootQueryContext.concept_name ) ;
    $("#savePromptPrompt").dialog({
        height: "145",
        modal: true,
    }) ;
} ;

var submitSaveMapName = function() {
    // edit filename input value
    $("#saveMapForm > input.cmtext").val(
        $("#input > input").val()
    ) ;

    // submit form
    $("#saveMapForm").submit() ;

    // close dialog
    $("#savePromptPrompt").dialog( "close" ) ;
} ;

var saveConceptMap = function( form ) {

    var filename = form.filename.value.trim() ;

    /* alert("saveConceptMap( rootQueryContext: "+JSON.stringify(rootQueryContext)+"\nfile name:"+filename+")") ; */

    /* Get the concept map JSON */
    var concept_map = $('#concept_map').cytoscape('get');
    var concept_map_json = concept_map.json() ;

    /* Coerce layout to current layout */
    concept_map_json.layout.name = currentCMLayout ;

    /* ... then capture the result */
    form.concept_map.value = JSON.stringify(concept_map_json) ;

    if( filename.length > 0) {
        if( filename === "Unknown" &&
            rootQueryContext.concept_name.length > 0 ) {
            form.filename.value = rootQueryContext.concept_name ;
        } else {
            form.filename.value = filename ;
        }
    } else {
        form.filename.value = rootQueryContext.concept_name ;
    }

    form.concept_name.value = rootQueryContext.concept_name ;
    form.query.value        = rootQueryContext.query() ;
    form.rootID.value       = rootQueryContext.rootID ;

    /*
    alert("saveConceptMap("+
            "\nform.concept_name: "+ form.concept_name.value+
            "\nform.query: "+ form.query.value+
            "\nform.rootID: "+ form.rootID.value+
          ")") ;
    */
    return true ;

};

