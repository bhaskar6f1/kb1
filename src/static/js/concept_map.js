/*
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/* stub function - is overridden in gbk_discovery.js */
var reloadData = function(data) {
    alert("Reloading data?") ;
}

/* stub function - is overridden in gbk_discovery.js */
var clickEvidence = function( predication_id, predication ) {
    alert("Evidence for predication " + predication ) ;
}

var darkNodeStyle = {
            'content': 'data(name)',
            'text-valign': 'center',
            'color': 'white',
            'min-zoomed-font-size' : '5px',
            'text-outline-width': '1px',
            'text-outline-color': 'green',

            /* default node color is grey,
               but overridden by semantic
               class styles (below) */
            'background-color': '#A8A8A8'
        } ;

var darkEdgeStyle =  {
            'content': 'data(name)',
            'color': 'white',
            'text-outline-width': 1,
            'text-outline-color': 'green',
            'font-size' : '10px',
            'min-zoomed-font-size': '5px',
            'width': 2,
            'target-arrow-shape': 'triangle',
            'line-color': 'yellow',
            'target-arrow-color': 'yellow',
            'source-arrow-color': 'yellow'
         } ;

var darkSelectedStyle = {
            'background-color': 'white',
            'line-color': 'cyan',
            'target-arrow-color': 'cyan',
            'source-arrow-color': 'cyan'
        };

var darkFadedStyle = {
            'opacity': 0.75,
            'text-opacity': 1
        };

var darkGeneNodeStyle = {
            'background-color':'#0000A8'
        };

var darkChemNodeStyle = {
            'background-color':'#00A800'
        };

var darkPhysNodeStyle = {
            'background-color':'#00A8A8'
        };

var darkDisorderNodeStyle = {
            'background-color':'#A80000'
        } ;

var darkOtherNodeStyle = {
            'background-color':'#A8A8A8'
        } ;

var lightNodeStyle = {
            'content': 'data(name)',
            'text-valign': 'center',
            'color': 'black',
            'min-zoomed-font-size' : '5px',
            'text-outline-width': '1px',
            'text-outline-color': 'white',

            /* default node color is grey,
               but overridden by semantic
               class styles (below) */
            'background-color': '#A8A8A8'
        } ;

var lightEdgeStyle =  {
            'content': 'data(name)',
            'color': 'black',
            'text-outline-width': 1,
            'text-outline-color': 'white',
            'font-size' : '10px',
            'min-zoomed-font-size': '5px',
            'width': 2,
            'target-arrow-shape': 'triangle',
            'line-color': 'brown',
            'target-arrow-color': 'brown',
            'source-arrow-color': 'brown'
        } ;

var lightSelectedStyle = {
            'background-color': '#484848',
            'line-color': 'cyan',
            'target-arrow-color': 'cyan',
            'source-arrow-color': 'cyan'
        } ;

var lightFadedStyle = {
            'opacity': 0.75,
            'text-opacity': 1
        } ;

var lightGeneNodeStyle = {
            'background-color':'#4080FF'
        };

var lightChemNodeStyle = {
            'background-color':'#40FF40'
        };

var lightPhysNodeStyle = {
            'background-color':'#40FFFF'
        };

var lightDisorderNodeStyle = {
            'background-color':'#FF4040'
        } ;

var lightOtherNodeStyle = {
            'background-color':'#404040'
        } ;

$(function(){

    var concept_map = cytoscape({

        container: $('#concept_map')[0],

         style:
            cytoscape.stylesheet()
                .selector('node').css( darkNodeStyle )
                .selector('edge').css( darkEdgeStyle )
                .selector(':selected').css( darkSelectedStyle )
                .selector('.faded').css( darkFadedStyle )
                .selector('.gene_node').css( darkGeneNodeStyle )
                .selector('.chem_node').css( darkChemNodeStyle )
                .selector('.phys_node').css( darkPhysNodeStyle )
                .selector('.disorder_node').css( darkDisorderNodeStyle )
                .selector('.other_node').css( darkOtherNodeStyle ),

        elements: graphData,

        layout: {
            name: currentCMLayout,
            padding: 10
        },

        ready: function(){
        },

     }) ;

    /* on graph initial layout done (could be async depending on layout...) */
    concept_map.elements().unselectify();

    // CONCEPT_MAP EVENTS //

    /* Browser Menu Suppression:
     * Prevent Trigger of Browser Menu on Right-Click of Nodes
     */
    concept_map.on('cxttap','node', function(e){
        var node = e.cyTarget;
        stopEvent( e );
    });

    /*BUG: new nodes added to the white background does not have the features designed for the white background
            until the checkbox has been checked && unchecked
            - The problem is that the elements take on their initial styling as opposed to the styling specified in the
            function as the checkbox is not active when the page is loaded. (the "checked" look is more of a stylistic feature
            than a functional one).
            - The solution will lie in initializing the styling according to the checkbox and not the styling being activated by
            the checking and unchecking of the box*/
    /*BUG: ':selected' is buggy - does not change color with the background. Does not change back color to
            grey when unselected - FIXED*/
    changeElesClr = function() {

        if ( $("#cmtheme").find(":selected").val() === "dark" ){

            concept_map.style()
                .resetToDefault() // start a fresh default stylesheet
                    .selector('node').css( darkNodeStyle )
                    .selector('edge').css( darkEdgeStyle )
                    .selector(':selected').css( darkSelectedStyle )
                    .selector('.faded').css( darkFadedStyle )
                    .selector('.gene_node').css( darkGeneNodeStyle )
                    .selector('.chem_node').css( darkChemNodeStyle )
                    .selector('.phys_node').css( darkPhysNodeStyle )
                    .selector('.disorder_node').css( darkDisorderNodeStyle )
                    .selector('.other_node').css( darkOtherNodeStyle )
                .update();
        }
        else {
            concept_map.style()
                .resetToDefault() // start a fresh default stylesheet
                    .selector('node').css( lightNodeStyle )
                    .selector('edge').css( lightEdgeStyle )
                    .selector(':selected').css( lightSelectedStyle )
                    .selector('.faded').css( lightFadedStyle )
                    .selector('.gene_node').css( lightGeneNodeStyle )
                    .selector('.chem_node').css( lightChemNodeStyle )
                    .selector('.phys_node').css( lightPhysNodeStyle )
                    .selector('.disorder_node').css( lightDisorderNodeStyle )
                    .selector('.other_node').css( lightOtherNodeStyle )
                .update();
        }

    } ;

    /* Context Menus:
     * Give set of actions that can be done to individual nodes or edges.
     * Template of actions is defined in discovery.jade as DOM elements.
     * Loosely coupled to "actions" JS object on global scope, below.
     */

    $("#contextMenu").menu().hide();

    function getTargetNode(target_id) {
        /* I access this indirectly with a "just-in-time" accessor
           function to ensure up-to-date visibility of the concept map node */
        return concept_map.$( "node[id='"+target_id+"']" ) ;
    }

    /* Neighbourhood:
     * Highlight neighbourhood nodes.
     */
    concept_map.on('tap', 'node', function(e){

        var ele = e.cyTarget;
        var target_id = ele.id() ;

        var neighborhood = ele.neighborhood().add(ele);

        concept_map.elements().addClass('faded');
        neighborhood.removeClass('faded');

        var concept_id = ele.data("id").split(":")[1] ;
        var clickedConceptName = ele.data("name") ;

        $("#conceptMapNodePrompt").data("selected_concept_cui", concept_id ) ;
        $("#conceptMapNodePrompt").data("selected_concept_name", clickedConceptName ) ;
        $("#conceptMapNodePrompt > #buttons > #show").text("Show Relations");
        $("#clickedElementName ").empty().append( clickedConceptName ) ;

        var node_element = concept_map.$( "node[id='"+target_id+"']" ) ;

        node_element.qtip({
            content : {
                text : $("#conceptMapNodePrompt").html()
            },
            show:{
                ready:true,
            },
            position: {
                corner: {
                     target:  'topLeft',
                     tooltip: 'topLeft'
                }
            },
            style: {
                classes: 'qtip-green'
            }
        }) ;

        // cytoscape-qtip wants
        var qtipApi = node_element.qtip('api');

        mapActions = {

            explore: function() {

                var selected_concept_cui  = $("#conceptMapNodePrompt").data("selected_concept_cui");
                var selected_concept_name = $("#conceptMapNodePrompt").data("selected_concept_name");

                /* alert("explore: cui = "+selected_concept_cui+", name = "+selected_concept_name) ; */

                reloadData( selected_concept_cui, selected_concept_name );

                /* Display the Explicit data results tab? */
                $("#discovery_data_tabs").tabs("option", "active", 0 );

                qtipApi.destroy();
            },

            del: function() {
                deleteNode( target_id );
                qtipApi.destroy;
            },

            cancel: function() {
                qtipApi.destroy();
            },

        } ;

    });

    concept_map.on('tap','edge', function(e) {
        var ele = e.cyTarget;
        var target_id = ele.id();

        var relation      = ele.data("source_name")+" "+ele.data("name")+" "+ele.data("target_name") ;
        var relation_type = ele.id().substr(0,1) ;
        var relation_id   = ele.id().substr(2) ;

        // TODO: Edit names to be more generic between whatever is polymorphic between nodes and edges
        $("#conceptMapNodePrompt").data("selected_concept_cui", target_id ) ;
        $("#conceptMapNodePrompt").data("selected_concept_name", relation ) ;
        $("#conceptMapNodePrompt > #buttons > #show").text("Show Evidence");
        $("#clickedElementName ").empty().append( relation ) ;

        var edge_element = concept_map.$( "edge[id='"+target_id+"']" ) ;

        console.log("edge-data", {
            "relation": relation,
            "relation_type": relation_type,
            "relation_id": relation_id,
        })

        edge_element.qtip({
            content : {
                text : $("#conceptMapNodePrompt").html()
            },
            show:{
                ready:true,
            },
            position: {
                // Adjusting from top.
                adjust:
                    (function() {
                        var edgeRenderPosition = e.cyRenderedPosition;
                        var viewportPosition = $("#concept_map")[0].getBoundingClientRect();
                        return {
                            x: viewportPosition.left + edgeRenderPosition.x,
                            y: viewportPosition.top + edgeRenderPosition.y
                        }
                    })()
            },
            style: {
                classes: 'qtip-green'
            }
        }) ;

        var qtipApi = edge_element.qtip("api") ;

        mapActions = {

            explore: function() {

                source_name = ele.source().data("name") ;
                target_name = ele.target().data("name") ;

                if ( relation_type == "E" ) {
                    clickEvidence( relation_id, relation ) ;
                }
                if ( relation_type == "C" ) {
                    clickPMID( source_name + " AND " + target_name )
                }
                if ( relation_type == "I" ) {
                    //console.log(ele.source().data(), $("#contextTable").data("subject_concept_id"), ele.target().data());
                    var relsig_part = relation_id.split(".") ;
                    console.log(relation_id, relsig_part) ;
                    clickCooccurrence( relsig_part[0], relsig_part[1], source_name, target_name ) ;
                }

                /* Display the Explicit data results tab? */
                $("#discovery_data_tabs").tabs("option", "active", 0 );

                qtipApi.destroy();
            },

            del: function() {
                deleteEdge( target_id );
                qtipApi.destroy;
            },

            cancel: function() {
                qtipApi.destroy();
            },

        };

    });

    concept_map.on('tap', function(e){

        if( e.cyTarget === concept_map ){
            concept_map.elements().removeClass('faded');
        }

    });

    $( "#confirm_concept_deletion" ).
        append(
            '"'+'<span id="concept_to_be_deleted"></span>'+
            '" will be deleted from the concept map. Are you sure?'
        ).hide() ;

    // Workaround to get Cytoscape to understand its bounds in bootstrap?
    // http://stackoverflow.com/questions/25916735/using-cytoscape-rendering-inside-twitter-boostrap-tabs
    // concept_map.resize();

});

// NODE ACTIONS //

/* deleteNode:
 * Create alert confirming the deletion of node with target ID
 */
function deleteNode( target_id ){

    // import a ref to the cytoscape object so that we can use cytoscape methods
    var concept_map = $("#concept_map").cytoscape("get");
    var node = concept_map.getElementById( target_id );

    var qtipApi = node.qtip("api");
    qtipApi.destroy();

    $( "#concept_to_be_deleted" ).empty().append( node.data("name") ) ;
    $( "#confirm_concept_deletion" ).show().dialog({
        resizable: false,
        height:200,
        width :300,
        modal: true,
        buttons: {
            "OK": function() {
                node.remove() ;
                /* rootID should be globally defined somewhere, e.g. discovery.jade*/
                concept_map.nodes().orphans('node['+rootID+']').remove() ;
                $( this ).dialog( "close" );
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        }
    });

};

/* deleteEdge:
 * Create alert confirming the deletion of edge with target ID
 * TODO: Do we remove source and/or target?
 */
function deleteEdge( target_id ){

    // import a ref to the cytoscape object so that we can use cytoscape methods
    var concept_map = $("#concept_map").cytoscape("get");
    var edge = concept_map.getElementById( target_id );

    var qtipApi = edge.qtip("api");
    qtipApi.destroy();

    $( "#concept_to_be_deleted" ).empty().append( edge.data("name") ) ;
    $( "#confirm_concept_deletion" ).show().dialog({
        resizable: false,
        height:200,
        width :300,
        modal: true,
        buttons: {
            "OK": function() {
                edge.remove() ;
                $( this ).dialog( "close" );
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        }
    });

};

/* nextLevelData:
 * Gives data related to the tapped node that exists on the next level of analysis
 * Affected by the previous node's identification data. This directs the query.
 */
function nextLevelConcepts( searchParams ) {

    // Argument Object takes sessionControls' values as default.
    // TODO: If no ID given, then return 0. Is this the wisest way to handle error cases?
    // TODO: evaluate well-formedness at this level, or later?
    searchParams = searchParams || {};
    searchParams.id = searchParams.id || 0;
    searchParams.expandCount = searchParams.expandCount || 0;
    searchParams.weightCutoff = searchParams.weightCutoff || 0;
    searchParams.callback = searchParams.callback || function () {};

    // console.log( "Being search for NextLevelConcepts with following parameters:" + JSON.stringify( searchParams ) );

    // Pass the above to the Django controller view
    var rc = $.ajax({
        dataType: "json",
        url: sessionControls.dataSource,
        data: {
          // TODO: Insert request based on the argument information
        },
        success: function ( returned_concepts ) {

          // console.log(" Query success - call returned");

          // process the returned data to map to cytoscape conventions
              // take concepts and assign to node groups
              // take relations and assign to edge groups
              // Concepts are linked by ID in edges.
              // Concept IDs are standard to unique instances of that concept. // TODO: Breakable? What if we need to update naming conventions?
          // returned_concepts are in an array
          return returned_concepts;

        }

    });

    // return array
    return rc;

};

// ACTION MENU //

/* actions:
 * This is the object that the 'onclicks' refer to to provide generic capability to the context menu in reference to the nodes.
 * We reference these functions down the hierarchy and implement them directly into the DOM. This will be prone to change when we update the library used for the context menu.
    // In particular I would prefer if the entire object were specced as a widget in the javascript, so we can
        // (a) avoid coupling the view with functionality too much, and
        // (b) provide generic functionality entirely hidden inside of the javascript file
    // we would need to find a jQueryUI widget to use since that's what qTip2 likes.
    // Although this fiddle says otherwise: http://jsfiddle.net/waspinator/zMsqK/9/
    // TODO: try out the context.js library. I don't think I've employed it fully enough to disregard it entirely.
 * It is tied to #contextMenu in a somewhat coupled way.
 */
var menuActions = {
    // delete nodes
    del: {
        self: function( self ){
            var target_id = $("#contextMenu").data("target_id");
            if( target_id[0] ==='E' || target_id[0] ==='I') {
                // alert("Delete edge: "+target_id) ;
                deleteEdge( target_id );
            } else {
                // alert("Delete node: "+target_id) ;
                deleteNode( target_id );
            }

        },
        child:  function( self ){ var target_id = $("#contextMenu").data("target_id"); },
        parent: function( self ){ var target_id = $("#contextMenu").data("target_id"); },
    },
    // expand nodes
    exp:function( self ){ var target_id = $("#contextMenu").data("target_id"); nextLevelConcepts({ id: target_id }) }
} ;

// HELPER FUNCTIONS //

/* stopEvent:
 * Ensure that an event doesn't occur.
 * Used to prevent the context menu appearing upon right-click.
 */
function stopEvent( event ){
    if(event.preventDefault != undefined)
        event.preventDefault();

    if(event.stopPropagation != undefined)
        event.stopPropagation();
}

changeMapBkgd = function() {

    if( $("#cmtheme").find(":selected").val() === "dark" ) {
        $( "#concept_map" ).css("background-color","#333") ;
        $( ".cmtoolbar" ).css("background-color","#002000") ;
        $( ".cmlabel" ).css("color","white") ;
        $( "#concept_map").css("border","2px solid white") ;
        $( ".cmtoolbar" ).css("border","2px solid white") ;

    } else {
        $( "#concept_map").css("background-color","white") ;
        $( ".cmtoolbar" ).css("background-color","white") ;
        $( ".cmlabel" ).css("color","black") ;
        $( "#concept_map").css("border","2px solid #333") ;
        $( ".cmtoolbar" ).css("border","2px solid #002000") ;
    }
} ;





