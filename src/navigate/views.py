"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import sys
import json

from django.template.response import TemplateResponse

from query import QueryFactory
from query import datasource

DEBUG = True

def home(request):
    return TemplateResponse(request, 'navigate/home.jade')

def browse(request):
    
    query = request.POST.get('query')
    
    if DEBUG:
        print >>sys.stderr,"Browsing from concept '"+query
    
    # query_engine = QueryFactory(datasource.SEMMEDDB)
    
    # Stub initial implementation - just returns some PMID's
    results = ['19851774','19855969','15799417','19149034'] # query_engine.retrieveCitationsByConcept(query)
    
    results = json.dumps(results) if results!= None and len(results) > 0 else 0
    
    ctx = {
           'query' : unicode(query),
           'results' : results,
           }
    
    if DEBUG:
        print >>sys.stderr,"Search results:\n",ctx['results']

    return TemplateResponse(request, 'navigate/display.jade',ctx)